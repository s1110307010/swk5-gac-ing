﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class CacheSizeList {

        public CacheSizeList() {
            CacheSizes = new List<CacheSize>();
        }

        public List<CacheSize> CacheSizes { get; set; }

        public int Count() {
            return CacheSizes.Count();
        }
    }
}
