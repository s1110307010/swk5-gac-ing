﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class RoleList {

        public RoleList() {
            Roles = new List<Role>();
        }

        public List<Role> Roles { get; set; }

        public int Count() {
            return Roles.Count();
        }
    }
}
