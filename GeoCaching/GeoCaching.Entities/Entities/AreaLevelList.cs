﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class AreaLevelList {

        public AreaLevelList() {
            AreaLevels = new List<int>();
        }

        public List<int> AreaLevels { get; set; }

        public int Count() {
            return AreaLevels.Count();
        }
    }
}
