﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public class User : EntityBase {
		#region Name
		/// <summary>
		/// Gets or sets the value for Name
		/// </summary>
		public string Name { get; set; }
		#endregion

		#region Password
		/// <summary>
		/// Gets or sets the value for Password
		/// </summary>
		public string Password { get; set; }
		#endregion

		#region Email
		/// <summary>
		/// Gets or sets the value for Email
		/// </summary>
		public string Email { get; set; }
		#endregion

		#region ResidenceCoordinates
		/// <summary>
		/// Gets or sets the value for ResidenceCoordinates
		/// </summary>
		public Coordinate ResidenceCoordinates { get; set; }
		#endregion
	}
}