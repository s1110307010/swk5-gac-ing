﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public class Role : EntityBase {
		#region Name
		/// <summary>
		/// Gets or sets the value for Name
		/// </summary>
		public string Name { get; set; }
		#endregion
	}
}