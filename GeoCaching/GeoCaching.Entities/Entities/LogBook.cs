﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public class LogBook : EntityBase {
		#region CacheId
		/// <summary>
		/// Gets or sets the value for CacheId
		/// </summary>
		public int CacheId { get; set; }
		#endregion

		#region UserId
		/// <summary>
		/// Gets or sets the value for UserId
		/// </summary>
		public int UserId { get; set; }
		#endregion

		#region SearchState
		/// <summary>
		/// Gets or sets the value for SearchState
		/// </summary>
		public SearchState SearchState { get; set; }
		#endregion

		#region Comment
		/// <summary>
		/// Gets or sets the value for Comment
		/// </summary>
		public string Comment { get; set; }
		#endregion

		#region CreationDate
		/// <summary>
		/// Gets or sets the value for CreationDate
		/// </summary>
		public DateTime CreationDate { get; set; }
		#endregion
	}
}