﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public class Picture : EntityBase {
		#region CacheId
		/// <summary>
		/// Gets or sets the value for CacheId
		/// </summary>
		public int CacheId { get; set; }
		#endregion
		
		#region ImageData
		/// <summary>
		/// Gets or sets the value for ImageData
		/// </summary>
		public byte[] ImageData { get; set; }
		#endregion
	}
}