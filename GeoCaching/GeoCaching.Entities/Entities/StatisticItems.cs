﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class StatisticUser {
		/// <summary>
		/// Gets or sets the value for User
		/// </summary>
		public User UserItem { get; set; }

		/// <summary>
		/// Gets or sets the value for Count
		/// </summary>
		public int Count { get; set; }
	}

	public class StatisticGeoCache {
		/// <summary>
		/// Gets or sets the value for User
		/// </summary>
		public Cache CacheItem { get; set; }

		/// <summary>
		/// Gets or sets the value for Count
		/// </summary>
		public int Count { get; set; }
	}
}
