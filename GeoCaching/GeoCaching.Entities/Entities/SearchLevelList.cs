﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class SearchLevelList {

        public SearchLevelList() {
            SearchLevels = new List<int>();
        }

        public List<int> SearchLevels { get; set; }

        public int Count() {
            return SearchLevels.Count();
        }
    }
}
