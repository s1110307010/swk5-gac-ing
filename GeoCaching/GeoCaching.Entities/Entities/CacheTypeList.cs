﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class CacheTypeList {

        public CacheTypeList() {
            CacheTypes = new List<CacheType>();
        }

        public List<CacheType> CacheTypes { get; set; }

        public int Count() {
            return CacheTypes.Count();
        }
    }
}
