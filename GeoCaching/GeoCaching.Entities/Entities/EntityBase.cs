﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public abstract class EntityBase : IEntity {
		#region IEntity Members
		public int Id { get; set; }
		#endregion
	}
}
