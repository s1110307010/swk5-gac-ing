﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class CacheList {

        public CacheList() {
            Caches = new List<Cache>();
        }

        public List<Cache> Caches { get; set; }

        public int Count() {
            return Caches.Count();
        }
    }
}
