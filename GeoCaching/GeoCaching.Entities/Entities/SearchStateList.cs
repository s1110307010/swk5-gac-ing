﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class SearchStateList {

        public SearchStateList() {
            SearchStates = new List<SearchState>();
        }

        public List<SearchState> SearchStates { get; set; }

        public int Count() {
            return SearchStates.Count();
        }
    }
}
