﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class Cache : EntityBase {
		#region Constructor
		public Cache() {
            this.SearchLevel = 1;
            this.AreaLevel = 1;
            this.CreationDate = DateTime.Today;
        }
		#endregion

		#region Name
		/// <summary>
        /// Gets or sets the value for Name
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Coordinates
        /// <summary>
        /// Gets or sets the value for Coordinates
        /// </summary>
        public Coordinate Coordinates { get; set; }
        #endregion

        #region Size
        /// <summary>
        /// Gets or sets the value for Size
        /// </summary>
        public CacheSize Size { get; set; }
        #endregion

        #region SearchLevel
        /// <summary>
        /// Gets or sets the value for SearchLevel
        /// </summary>
        public int SearchLevel { get; set; }
        #endregion

        #region AreaLevel
        /// <summary>
        /// Gets or sets the value for AreaLevel
        /// </summary>
        public int AreaLevel { get; set; }
        #endregion

        #region CreationDate
        /// <summary>
        /// Gets or sets the value for CreationDate
        /// </summary>
		public DateTime CreationDate { get; set; }
        #endregion

        #region Puzzle
        /// <summary>
        /// Gets or sets the value for Puzzle
        /// </summary>
        public string Puzzle { get; set; }
        #endregion

        #region OwnerId
        /// <summary>
        /// Gets or sets the value for OwnerId
        /// </summary>
        public int OwnerId { get; set; }
        #endregion

        #region State
        /// <summary>
        /// Gets or sets the value for State
        /// </summary>
        public CacheState State { get; set; }
        #endregion

        #region Type
        /// <summary>
        /// Gets or sets the value for Type
        /// </summary>
        public CacheType Type { get; set; }
        #endregion
    }
}