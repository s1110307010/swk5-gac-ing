﻿namespace GeoCaching.Entities {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class RatingList {

        public RatingList() {
            Ratings = new List<int>();
        }

        public List<int> Ratings { get; set; }

        public int Count() {
            return Ratings.Count();
        }
    }
}
