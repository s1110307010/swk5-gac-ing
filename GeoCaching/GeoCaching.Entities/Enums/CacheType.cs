﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public enum CacheType {
		DriveIn = 0,
        MathPhysics = 1,
        Moving = 2,
        Multi = 3,
        Other = 4,
		Quiz = 5,
        Traditional = 6,
		Virtual = 7,
        Webcam = 8,
	}
}
