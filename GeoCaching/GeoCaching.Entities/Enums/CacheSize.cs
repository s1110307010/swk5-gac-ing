﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
	public enum CacheSize {
		Mikro = 0,
		Small = 1,
		Normal = 2,
		Big = 3,
		None = 4,
		Other = 5
	}
}
