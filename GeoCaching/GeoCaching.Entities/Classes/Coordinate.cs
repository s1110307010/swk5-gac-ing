﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    [Serializable]
    public class Coordinate {
		/// <summary>
		/// Gets or sets the value for Longitude
		/// </summary>
		public double Longitude { get; set; }

		/// <summary>
		/// Gets or sets the value for Latitude
		/// </summary>
		public double Latitude { get; set; }

		/// <summary>
		/// Gets or sets the value for SpatialReferenceId
		/// </summary>
		public int SpatialReferenceId { get; set; }
	}
}
