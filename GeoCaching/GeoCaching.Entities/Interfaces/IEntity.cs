﻿namespace GeoCaching.Entities {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IEntity {
		/// <summary>
		/// Gets or sets the value for Id
		/// </summary>
		int Id { get; set; }
	}
}