﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class DALUtilsTest {
        [TestMethod]
		[ExpectedException(typeof(ArgumentException))]
        public void CreateParameterTest1() {
			DALUtils.CreateParameter(null, null, null);
        }

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateParameterTest2() {
			DALUtils.CreateParameter("@Id", null, null);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateParameterTest3() {
			DALUtils.CreateParameter("@Id", 0, null);
		}
    }
}