﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class RoleDaoTest {
        #region private HelperClasses
        private void CreateTestData() {
            UnitTestUtil.CreateTestRoles(DALFactory.GetRoleDao());
            //UnitTestUtil.CreateTestCaches(DALManager.GetCacheDao());
            //UnitTestUtil.CreateTestRatings(DALManager.GetRatingDao());
        }

        private void DeleteTestData() {
            UnitTestUtil.DeleteTestRoles(DALFactory.GetRoleDao());
            //UnitTestUtil.DeleteTestRatings(DALManager.GetRatingDao());
            //UnitTestUtil.DeleteTestCaches(DALManager.GetCacheDao());
        }
        #endregion

        #region Other Tests
        
        [TestMethod]
        public void RoleDaoTest_GetAllRoles() {
            DeleteTestData();
            CreateTestData();
            IRoleDao roleDao = DALFactory.GetRoleDao();
            IEnumerable<Role> roleList = roleDao.GetAllRoles();
            Assert.IsNotNull(roleList);
            Assert.AreEqual(2, roleList.Count());
        }

        [TestMethod]
        public void RoleDaoTest_GetRolesOfUser() {
            DeleteTestData();
            CreateTestData();
            IRoleDao roleDao = DALFactory.GetRoleDao();
            IEnumerable<Role> roleList = roleDao.GetRolesOfUser(0);
            Assert.IsNull(roleList);
            roleList = roleDao.GetRolesOfUser(1);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(2, roleList.Count());
            roleList = roleDao.GetRolesOfUser(2);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(1, roleList.Count());
            roleList = roleDao.GetRolesOfUser(3);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(1, roleList.Count());
        }

        [TestMethod]
        public void RoleDaoTest_RemoveRoleFromUser() {
            DeleteTestData();
        }

        [TestMethod]
        public void RoleDaoTest_RemoveRoleFromUser_NoUserEntered() {
            IRoleDao roleDao = DALFactory.GetRoleDao();
            Assert.IsNotNull(roleDao);
            roleDao.RemoveRolesFromUser(1, new List<Role>());
        }

        [TestMethod]
        public void RoleDaoTest_RemoveRoleFromUser_NoRoleEntered() {
            IRoleDao roleDao = DALFactory.GetRoleDao();
            Assert.IsNotNull(roleDao);
            roleDao.RemoveRolesFromUser(1, null);
        }

        [TestMethod]
        public void RoleDaoTest_AddRoleToUser() {
            DeleteTestData();
            CreateTestData();
        }

        [TestMethod]
        public void RoleDaoTest_AddRoleToUser_NoUserEntered() {
            IRoleDao roleDao = DALFactory.GetRoleDao();
            Assert.IsNotNull(roleDao);
            roleDao.AssignRolesToUser(1, new List<Role>());
        }

        [TestMethod]
        public void RoleDaoTest_AddRoleToUser_NoRoleEntered() {
            IRoleDao roleDao = DALFactory.GetRoleDao();
            Assert.IsNotNull(roleDao);
            roleDao.AssignRolesToUser(1, null);
        }

        #endregion
    }
}