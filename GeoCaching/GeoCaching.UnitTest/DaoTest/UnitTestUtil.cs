﻿namespace GeoCaching.UnitTest {
    using GeoCaching.DAL;
    using GeoCaching.Entities;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data;
	using System.Diagnostics.CodeAnalysis;

	[ExcludeFromCodeCoverage]
    public static class UnitTestUtil {
        #region Constants
        
        const string DELETE_USER = "DELETE FROM [dbo].[User] WHERE [Id] = @id";
        
        #endregion

        #region Cache Utilities

        public static void CreateTestCaches(ICacheDao cacheDao) {
            Assert.AreEqual(0, cacheDao.CountEntries());
            foreach (Cache cache in DalCreateTestCacheList()) {
                Assert.IsTrue(cacheDao.Create(cache));
            }
            Assert.AreEqual(3, cacheDao.CountEntries());
        }

        public static void DeleteTestCaches(ICacheDao cacheDao) {
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheDao.Delete(cache.Id));
                }
            }
            Assert.AreEqual(0, cacheDao.CountEntries());
        }

        public static IEnumerable<Cache> DalCreateTestCacheList() {
            return new List<Cache> {
                GlobalTestUtils.CreateCache("Cache1", 1, "Puzzle", 1, 4, CacheSize.Normal, CacheState.Available, CacheType.DriveIn, 47.735367, 13.339183, 4326),
                GlobalTestUtils.CreateCache("Cache2", 2, "Puzzle2", 4, 2, CacheSize.Small, CacheState.Available, CacheType.Moving, 40.735367, 23.339183, 4326),
                GlobalTestUtils.CreateCache("Cache3", 2, "Puzzle3", 3, 1, CacheSize.None, CacheState.NotAvailable, CacheType.Quiz, 45.735367, 17.339183, 4326)};
        }


        public static void CreateTestPictures(ICacheDao cacheDao) {
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Picture picture in DalCreateTestPictureList()) {
                    Assert.IsTrue(cacheDao.AddPicture(picture));
                }
            }
            Assert.AreEqual(3, cacheDao.CountEntries());
        }

        public static void DeleteTestPictures(ICacheDao cacheDao) {
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                IEnumerable<Picture> pictureList = null;
                foreach (Cache cache in cacheList) {
                    pictureList = cacheDao.GetPicturesOfCache(cache.Id);
                    if (pictureList != null) {
                        foreach (Picture picture in pictureList) {
                            Assert.IsTrue(cacheDao.RemovePicture(picture.Id));
                        }
                    }
                    Assert.IsNull(cacheDao.GetPicturesOfCache(cache.Id));
                }
            }
        }

        public static IEnumerable<Picture> DalCreateTestPictureList() {
            List<Picture> picList = null;
            IEnumerable<Cache> cacheList = DALFactory.GetCacheDao().GetAll();
            if (cacheList != null) {
                picList = new List<Picture>();
                foreach (Cache cache in cacheList) {
                    picList.Add(GlobalTestUtils.CreatePicture(cache.Id, new Byte[] { 1, 2, 3, 4 }));
                }
            }
            return picList;
        }
        #endregion

        #region Rating Utilities
        public static void CreateTestRatings(IRatingDao rateDao) {
            Assert.AreEqual(0, rateDao.CountEntries());
            foreach (Rating rate in DalCreateTestRatingList()) {
                Assert.IsTrue(rateDao.Create(rate));
            }
            Assert.AreEqual(3, rateDao.CountEntries());
        }

        public static void DeleteTestRatings(IRatingDao ratingDao) {
            IEnumerable<Rating> cacheList = ratingDao.GetAll();
            if (cacheList != null) {
                foreach (Rating rate in cacheList) {
                    Assert.IsTrue(ratingDao.Delete(rate.Id));
                }
            }
            Assert.AreEqual(0, ratingDao.CountEntries());
        }

        public static IEnumerable<Rating> DalCreateTestRatingList() {
            IList<Rating> rateList = new List<Rating>();
            if (rateList != null) {
                IEnumerable<Cache> cacheList = DALFactory.GetCacheDao().GetAll();
                if (cacheList != null) {
                    foreach (Cache cache in cacheList) {
                        rateList.Add(GlobalTestUtils.CreateRate(cache.Id, 1, 3));
                    }            
                }
            }
            return rateList;
        }

        #endregion

        #region LogBook Utilities
        public static void CreateTestLogBooks(ILogBookDao logBookDao) {
            Assert.AreEqual(0, logBookDao.CountEntries());
            foreach (LogBook logBook in DalCreateTestLogBookList()) {
                Assert.IsTrue(logBookDao.Create(logBook));
            }
            Assert.AreEqual(3, logBookDao.CountEntries());
        }

        public static void DeleteTestLogBooks(ILogBookDao logBookDao) {
            IEnumerable<LogBook> cacheList = logBookDao.GetAll();
            if (cacheList != null) {
                foreach (LogBook logBook in cacheList) {
                    Assert.IsTrue(logBookDao.Delete(logBook.Id));
                }
            }
            Assert.AreEqual(0, logBookDao.CountEntries());
        }

        public static IEnumerable<LogBook> DalCreateTestLogBookList() {
            IList<LogBook> logBookList = new List<LogBook>();
            if (logBookList != null) {
                IEnumerable<Cache> cacheList = DALFactory.GetCacheDao().GetAll();
                if (cacheList != null) {
                    foreach (Cache cache in cacheList) {
                        logBookList.Add(GlobalTestUtils.CreateLogBook(cache.Id, 1, SearchState.Found, "Comment" + cache.Id));
                    }
                }
            }
            return logBookList;
        }
        #endregion

        #region User Utilities
        public static void RestoreTestUsers(IUserDao userDao) {
            IEnumerable<User> userList = userDao.GetAll();
            Assert.IsNotNull(userList);
            if (userList != null) {
                foreach (User user in userList) {
                    if (user.Id != 3) {
                        Assert.IsTrue(userDao.Reactivate(user.Id));
                    }
                }
                Assert.AreEqual(2, userDao.CountEntries());
            }
        }

        
        public static void CreateTestUsers(IUserDao userDao) {
            DeleteTestUsers(userDao);
            foreach (User user in DalCreateTestUserList()) {
                Assert.IsTrue(userDao.Create(user));
            }
            Assert.AreEqual(5, userDao.CountEntries());
        }

        public static void DeleteTestUsers(IUserDao userDao) {
            User user = userDao.GetByUserName("Andreas Etzelstorfer2");
            if (user != null) {
                Assert.IsTrue(userDao.Delete(user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            user = userDao.GetByUserName("Julia2");
            if (user != null) {
                Assert.IsTrue(userDao.Delete(user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            user = userDao.GetByUserName("Test User2");
            if (user != null) {
                Assert.IsTrue(userDao.Delete(user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            Assert.AreEqual(2, userDao.CountEntries());
        }
        
        public static IEnumerable<User> DalCreateTestUserList() {
            return new List<User> {
                GlobalTestUtils.CreateUser("Andreas Etzelstorfer2", "password", "a.b2@sch.at", 47.735367, 13.339183, 4326),
                GlobalTestUtils.CreateUser("Julia2", "Wiesinger2", "u.w2b@sch.at", 40.735367, 23.339183, 4326),
                GlobalTestUtils.CreateUser("Test User2", "password2", "t.u2@sch.at", 45.735367, 17.339183, 4326)};
        }
        #endregion

        #region Role Utilities
        public static void CreateTestRoles(IRoleDao roleDao) {
            Assert.IsNotNull(roleDao);
            if (roleDao != null) {
                IEnumerable<Role> roleList = roleDao.GetAllRoles();
                roleDao.AssignRolesToUser(1, roleList);

                roleList = null;
                roleList = new List<Role>() { roleDao.GetRoleById(2) };
                roleDao.AssignRolesToUser(2, roleList);

                roleList = null;
                roleList = new List<Role>() { roleDao.GetRoleByName("Hider") };
                roleDao.AssignRolesToUser(3, roleList);

                Assert.AreEqual(2, roleDao.GetRolesOfUser(1).Count());
                Assert.AreEqual(1, roleDao.GetRolesOfUser(2).Count());
                Assert.AreEqual(1, roleDao.GetRolesOfUser(3).Count());
            }
        }

        public static void DeleteTestRoles(IRoleDao roleDao) {
            int userId = 0;
            IEnumerable<Role> roleList = roleDao.GetRolesOfUser(userId);
            Assert.IsNull(roleList);

            roleDao.RemoveRolesFromUser(userId, roleDao.GetAllRoles());
            roleList = roleDao.GetRolesOfUser(userId);
            Assert.IsNull(roleList);

            userId = 1;
            roleList = roleDao.GetRolesOfUser(userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(2, roleList.Count());

                roleDao.RemoveRolesFromUser(userId, roleDao.GetAllRoles());
                roleList = roleDao.GetRolesOfUser(userId);
                Assert.IsNull(roleList);
            }

            userId = 2;
            roleList = roleDao.GetRolesOfUser(userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(1, roleList.Count());

                roleDao.RemoveRolesFromUser(userId, roleDao.GetAllRoles());
                roleList = roleDao.GetRolesOfUser(userId);
                Assert.IsNull(roleList);
            }

            userId = 3;
            roleList = roleDao.GetRolesOfUser(userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(1, roleList.Count());

                roleDao.RemoveRolesFromUser(userId, roleDao.GetAllRoles());
                roleList = roleDao.GetRolesOfUser(userId);
                Assert.IsNull(roleList);
            }
        }

        #endregion
    }
}