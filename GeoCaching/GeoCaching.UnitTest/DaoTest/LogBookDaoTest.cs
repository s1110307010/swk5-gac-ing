﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class LogBookDaoTest {
        #region private HelperClasses
        private void CreateTestData() {
            UnitTestUtil.CreateTestCaches(DALFactory.GetCacheDao());
            UnitTestUtil.CreateTestLogBooks(DALFactory.GetLogBookDao());
        }

        private void DeleteTestData() {
            UnitTestUtil.DeleteTestLogBooks(DALFactory.GetLogBookDao());
            UnitTestUtil.DeleteTestCaches(DALFactory.GetCacheDao());
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void LogBookDaoTest_GetCountOfEntries_NoEntryFound() {
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            DeleteTestData();
            Assert.AreEqual(0, logBookDao.CountEntries());
            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                IEnumerator<LogBook> logBookIt = logBookList.GetEnumerator();
                Assert.IsNull(logBookIt.Current);
            }
        }


        [TestMethod]
        public void LogBookDaoTest_CreateAndDeleteLogBook() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_GetAll() {
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, logBookDao.CountEntries());
            
            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookDao != null) {
                int countLogBook = 0;
                foreach (LogBook logBook in logBookList) {
                    countLogBook++;
                }
                Assert.AreEqual(3, countLogBook);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookTest_Get() {
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, logBookDao.CountEntries());

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                LogBook logBook2 = null;
                foreach (LogBook logBook in logBookList) {
                    logBook2 = logBookDao.Get(logBook.Id);
                    Assert.AreEqual(logBook.Id, logBook2.Id);
                    Assert.AreEqual(logBook.CacheId, logBook2.CacheId);
                    Assert.AreEqual(logBook.Comment, logBook2.Comment);
                    Assert.AreEqual(logBook.CreationDate, logBook2.CreationDate);
                    Assert.AreEqual(logBook.SearchState, logBook2.SearchState);
                    Assert.AreEqual(logBook.UserId, logBook2.UserId);
                }
            }
            DeleteTestData();
        }



        [TestMethod]
        public void LogBookDaoTest_FilterCache_CheckSearchState() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<SearchState> searchStates = new List<SearchState>();
            searchStates.Add(SearchState.NotFound);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, searchStates);
            Assert.AreEqual(0, cacheList.Count());

            searchStates.Add(SearchState.Found);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, searchStates);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_CacheDao_IsCacheInUse() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheDao.IsCacheInUse(cache.Id));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_GetAllLogBookEntriesOfCache() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            ICacheDao cacheDao = DALFactory.GetCacheDao();
            Assert.IsNotNull(cacheDao);
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            Assert.IsNotNull(cacheList);

            if (cacheDao != null && cacheList != null) {
                foreach (Cache cache in cacheList) {
                    IEnumerable<LogBook> logBookList = logBookDao.GetAllLogBookEntriesOfCache(cache.Id);
                    Assert.IsNotNull(logBookList);
                    Assert.AreEqual(1, logBookList.Count());
                    
                    logBookList = logBookDao.GetAllLogBookEntriesOfCache(0);
                    Assert.IsNull(logBookList);
                }
            }
            DeleteTestData();
        }
    #endregion

        #region statistics
        
        [TestMethod]
        public void LogBookDaoTest_GetCountOfCachesByState() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            ILogBookDao logbookDao = DALFactory.GetLogBookDao();
            DeleteTestData();
            CreateTestData();

            IEnumerable<int> caches = new List<int>(cacheDao.GetAll().Select(t => t.Id));

            Assert.AreEqual(3, logbookDao.GetCountOfCachesByState(1, caches, SearchState.Found));
            Assert.AreEqual(0, logbookDao.GetCountOfCachesByState(2, caches, SearchState.Found));
            Assert.AreEqual(0, logbookDao.GetCountOfCachesByState(3, caches, SearchState.Found));

            Assert.AreEqual(0, logbookDao.GetCountOfCachesByState(1, caches, SearchState.NotFound));
            Assert.AreEqual(0, logbookDao.GetCountOfCachesByState(2, caches, SearchState.NotFound));
            Assert.AreEqual(0, logbookDao.GetCountOfCachesByState(3, caches, SearchState.NotFound));
            
            DeleteTestData();
        }
        #endregion

        #region Test Update

        [TestMethod]
        public void LogBookDaoTest_ChangeComment() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.IsNotNull(logBook);
                    logBook.Comment = "TestComment";
                    logBookDao.Update(logBook);
                }
            }
            logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.AreEqual("TestComment", logBook.Comment);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_ChangeSearchState_Found() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.IsNotNull(logBook);
                    logBook.SearchState = SearchState.Found;
                    logBookDao.Update(logBook);
                }
            }
            logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.AreEqual(SearchState.Found, logBook.SearchState);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_ChangeSearchState_NotFound() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.IsNotNull(logBook);
                    logBook.SearchState = SearchState.NotFound;
                    logBookDao.Update(logBook);
                }
            }
            logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.AreEqual(SearchState.NotFound, logBook.SearchState);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_ChangeUser() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.IsNotNull(logBook);
                    logBook.UserId = 1;
                    logBookDao.Update(logBook);
                }
            }
            logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.AreEqual(1, logBook.UserId);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void LogBookDaoTest_ChangeCache() {
            DeleteTestData();
            CreateTestData();
            ILogBookDao logBookDao = DALFactory.GetLogBookDao();
            Assert.AreEqual(3, logBookDao.CountEntries());
            Assert.IsNotNull(logBookDao);

            ICacheDao cacheDao = DALFactory.GetCacheDao();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());
            IEnumerator<Cache> cacheIt = cacheList.GetEnumerator();
            Assert.IsNotNull(cacheIt);
            Cache cache = null;
            if (cacheIt.MoveNext()) {
                cache = cacheIt.Current;
            }

            IEnumerable<LogBook> logBookList = logBookDao.GetAll();
            if (logBookList != null && cacheList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.IsNotNull(logBook);

                    logBook.CacheId = cache.Id;
                    logBookDao.Update(logBook);
                }
            }
            logBookList = logBookDao.GetAll();
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    Assert.AreEqual(cache.Id, logBook.CacheId);
                }
            }
            DeleteTestData();
        }

        #endregion
    }
}