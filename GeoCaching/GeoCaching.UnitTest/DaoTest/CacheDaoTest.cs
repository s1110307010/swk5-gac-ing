﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class CacheDaoTest {
        #region private HelperClasses
        private void CreateTestData() {
            UnitTestUtil.CreateTestCaches(DALFactory.GetCacheDao());
            UnitTestUtil.CreateTestPictures(DALFactory.GetCacheDao());
        }

        private void DeleteTestData() {
            UnitTestUtil.DeleteTestPictures(DALFactory.GetCacheDao());
            UnitTestUtil.DeleteTestCaches(DALFactory.GetCacheDao());
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void CacheDaoTest_GetCountOfEntries_NoEntryFound() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            Assert.AreEqual(0, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                IEnumerator<Cache> cacheIt = cacheList.GetEnumerator();
                Assert.IsNull(cacheIt.Current);
            }
        }


        [TestMethod]
        public void CacheDaoTest_CreateAndDeleteCache() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_GetAll() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                int countCaches = 0;
                foreach (Cache cache in cacheList) {
                    countCaches++;
                }
                Assert.AreEqual(3, countCaches);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_Get() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Cache cache2 = null;
                foreach (Cache cache in cacheList) {
                    cache2 = cacheDao.Get(cache.Id);
                    Assert.AreEqual(cache.Id, cache2.Id);
                    Assert.AreEqual(cache.CreationDate, cache2.CreationDate);
                    Assert.AreEqual(cache.AreaLevel, cache2.AreaLevel);
                    Assert.AreEqual(cache.Coordinates.Latitude, cache2.Coordinates.Latitude);
                    Assert.AreEqual(cache.Coordinates.Longitude, cache2.Coordinates.Longitude);
                    Assert.AreEqual(cache.Coordinates.SpatialReferenceId, cache2.Coordinates.SpatialReferenceId);
                    Assert.AreEqual(cache.Name, cache2.Name);
                    Assert.AreEqual(cache.OwnerId, cache2.OwnerId);
                    Assert.AreEqual(cache.Puzzle, cache2.Puzzle);
                    Assert.AreEqual(cache.SearchLevel, cache2.SearchLevel);
                    Assert.AreEqual(cache.Size, cache2.Size);
                    Assert.AreEqual(cache.State, cache2.State);
                    Assert.AreEqual(cache.Type, cache2.Type);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_GetCacheByName() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Cache cache2 = null;
                foreach (Cache cache in cacheList) {
                    cache2 = cacheDao.GetCacheByName(cache.Name);
                    Assert.AreEqual(cache.Id, cache2.Id);
                    Assert.AreEqual(cache.CreationDate, cache2.CreationDate);
                    Assert.AreEqual(cache.AreaLevel, cache2.AreaLevel);
                    Assert.AreEqual(cache.Coordinates.Latitude, cache2.Coordinates.Latitude);
                    Assert.AreEqual(cache.Coordinates.Longitude, cache2.Coordinates.Longitude);
                    Assert.AreEqual(cache.Coordinates.SpatialReferenceId, cache2.Coordinates.SpatialReferenceId);
                    Assert.AreEqual(cache.Name, cache2.Name);
                    Assert.AreEqual(cache.OwnerId, cache2.OwnerId);
                    Assert.AreEqual(cache.Puzzle, cache2.Puzzle);
                    Assert.AreEqual(cache.SearchLevel, cache2.SearchLevel);
                    Assert.AreEqual(cache.Size, cache2.Size);
                    Assert.AreEqual(cache.State, cache2.State);
                    Assert.AreEqual(cache.Type, cache2.Type);
                }
            }
            DeleteTestData();
        }
        
        [TestMethod]
        public void CacheDaoTest_IsCacheInUse() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsFalse(cacheDao.IsCacheInUse(cache.Id));
                }
                Assert.IsFalse(cacheDao.IsCacheInUse(0));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_FilterCache_NoEntry() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(2011, 11, 4), null, null, null, null, null, default(int), null, null);
            Assert.AreEqual(0, cacheList.Count());
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_FilterCache_AllEntryFound() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());
            DeleteTestData();
        }


        [TestMethod]
        public void CacheDaoTest_FilterCache_CheckCacheTypes() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<CacheType> cacheTypes = new List<CacheType>();

            cacheTypes.Add(CacheType.DriveIn);
            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());
            
            cacheTypes.Add(CacheType.Multi);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheTypes.Add(CacheType.MathPhysics);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheTypes.Add(CacheType.Moving);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.Add(CacheType.Multi);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.Add(CacheType.Other);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.Add(CacheType.Quiz);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            cacheTypes.Add(CacheType.Traditional);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            cacheTypes.Add(CacheType.Virtual);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            cacheTypes.Add(CacheType.Webcam);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_FilterCache_CheckCacheSizes() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<CacheSize> cacheSizes = new List<CacheSize>();
            cacheSizes.Add(CacheSize.Normal);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.Add(CacheSize.None);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheSizes.Add(CacheSize.Big);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheSizes.Add(CacheSize.Mikro);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheSizes.Add(CacheSize.Other);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheSizes.Add(CacheSize.Small);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }


        [TestMethod]
        public void CacheDaoTest_FilterCache_CheckAreaLevels() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<int> areaLevels = new List<int>();
            areaLevels.Add(1);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            areaLevels.Add(2);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            areaLevels.Add(3);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            areaLevels.Add(4);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            areaLevels.Add(5);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_FilterCache_CheckSearchLevels() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<int> searchLevels = new List<int>();
			searchLevels.Add(1);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            searchLevels.Add(2);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            searchLevels.Add(3);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            searchLevels.Add(4);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            searchLevels.Add(5);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }

        #endregion

        #region Picture Tests
        [TestMethod]
        public void CacheDaoTest_DeleteAllPictures() {
            DeleteTestData();
            CreateTestData();
        }

        [TestMethod]
        public void CacheDaoTest_GetCountOfPictures_EntryFound() {
            int countPictures = 0;
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    IEnumerable<Picture> picList = cacheDao.GetPicturesOfCache(cache.Id);
                    Assert.IsNotNull(picList);
                    countPictures = 0;
                    foreach (Picture picture in picList) {
                        countPictures++;
                    }
                    Assert.AreEqual(1, countPictures);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_AddPictureToCache() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cacheDao.GetPicturesOfCache(cache.Id);
                }
            }
            DeleteTestData();
        }
        #endregion

        #region Cache Statistics Tests
        [TestMethod]
        public void CacheDaoTest_GetAreaLevelPercentage() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.333333333333, cacheDao.GetAreaLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetAreaLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(0, cacheDao.GetAreaLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(33.333333333333, cacheDao.GetAreaLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(0, cacheDao.GetAreaLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 5));
            Assert.AreEqual(33.333333333333, cacheDao.GetAreaLevelPercentage(new Coordinate() { Longitude = 45.735367, Latitude = 17.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetAreaLevelPercentage(new Coordinate() { Longitude = 40.735367, Latitude = 25.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            // 45.735367, 17.339183
        }

        [TestMethod]
        public void CacheDaoTest_GetSearchLevelPercentage() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.333333333333, cacheDao.GetSearchLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(0, cacheDao.GetSearchLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(33.333333333333, cacheDao.GetSearchLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(33.333333333333, cacheDao.GetSearchLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(0, cacheDao.GetSearchLevelPercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 5));
            Assert.AreEqual(33.333333333333, cacheDao.GetSearchLevelPercentage(new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetSearchLevelPercentage(new Coordinate() { Longitude = 40.735367, Latitude = 20.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
        }

        [TestMethod]
        public void CacheDaoTest_GetSearchStateCountOfCache() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            foreach (Cache cache in cacheList) {
                Assert.AreEqual(0, cacheDao.GetSearchStateCountOfCache(cache.Id, SearchState.Found));
            }
        }

        [TestMethod]
        public void CacheDaoTest_GetSizePercentage() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(0, cacheDao.GetSizePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 0));
            Assert.AreEqual(33.333333333333, cacheDao.GetSizePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetSizePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(0, cacheDao.GetSizePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(33.333333333333, cacheDao.GetSizePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(33.333333333333, cacheDao.GetSizePercentage(new Coordinate() { Longitude = 40.735367, Latitude = 23.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetSizePercentage(new Coordinate() { Longitude = 35.735367, Latitude = 28.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
        }

        [TestMethod]
        public void CacheDaoTest_GetTypePercentage() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.333333333333, cacheDao.GetTypePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 0));
            Assert.AreEqual(0, cacheDao.GetTypePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.333333333333, cacheDao.GetTypePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(0, cacheDao.GetTypePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(0, cacheDao.GetTypePercentage(null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(33.333333333333, cacheDao.GetTypePercentage(new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 0));
            Assert.AreEqual(33.333333333333, cacheDao.GetTypePercentage(new Coordinate() { Longitude = 40.735367, Latitude = 20.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 0));
        }
        
        [TestMethod]
        public void CacheDaoTest_GetCountOfHidedCaches() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();

            IEnumerable<int> caches = new List<int>(cacheDao.GetAll().Select(t => t.Id));

            Assert.AreEqual(1, cacheDao.GetCountOfHidedCaches(1, caches));
            Assert.AreEqual(2, cacheDao.GetCountOfHidedCaches(2, caches));
            Assert.AreEqual(0, cacheDao.GetCountOfHidedCaches(3, caches));

            
            DeleteTestData();
        }
        #endregion

        #region Update Caches
        [TestMethod]
        public void CacheDaoTest_ChangeSearchLevel() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.SearchLevel = 3;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(3, cache.SearchLevel);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_ChangeAreaLevel() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.AreaLevel = 3;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(3, cache.AreaLevel);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_ChangePuzzle() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Puzzle = "Puzzle";
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual("Puzzle", cache.Puzzle);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_ChangeName() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Name = "TestName";
                    cacheDao.Update(cache);
                    break;
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {

                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual("TestName", cache.Name);
                    break;
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_ChangeCoordinate() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Coordinate co = new Coordinate();
            co.Latitude = 44.444444;
            co.Longitude = 55.555555;
            co.SpatialReferenceId = 4326;
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Coordinates = co;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(co.Latitude, cache.Coordinates.Latitude);
                    Assert.AreEqual(co.Longitude, cache.Coordinates.Longitude);
                    Assert.AreEqual(co.SpatialReferenceId, cache.Coordinates.SpatialReferenceId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void CacheDaoTest_ChangeOwnerId() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.OwnerId = 2;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheDao.GetAll()) {
                    Assert.AreEqual(2, cache.OwnerId);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void CacheDaoTest_ChangeSize() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Size = CacheSize.Big;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheSize.Big, cache.Size);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void CacheDaoTest_ChangeStateId() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.State = CacheState.Available;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheState.Available, cache.State);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void CacheDaoTest_ChangeTypeId() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Type = CacheType.Moving;
                    cacheDao.Update(cache);
                }
            }
            cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                Assert.AreEqual(3, cacheDao.CountEntries());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheType.Moving, cache.Type);
                }
            }
            DeleteTestData();
        }
        #endregion

        #region Expected SqlException
        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void CachDaoTest_InsertDuplicateCache() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            foreach (Cache cache in UnitTestUtil.DalCreateTestCacheList()) {
                try {
                    Assert.IsTrue(cacheDao.Create(cache));
                } finally {
                    DeleteTestData();
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void CachDaoTest_ChangeOwnerId_UserNotExists() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            foreach (Cache cache in cacheDao.GetAll()) {
                Assert.IsNotNull(cache);
                cache.OwnerId = -1;
                try {
                    cacheDao.Update(cache);
                } finally {
                    DeleteTestData();
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void CachDaoTest_ChangeSearchLevel_SearchLevelToHigh() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            foreach (Cache cache in cacheDao.GetAll()) {
                Assert.IsNotNull(cache);
                cache.SearchLevel = 10;
                try {
                    cacheDao.Update(cache);
                } finally {
                    DeleteTestData();
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void CachDaoTest_ChangeAreaLevel_AreaLevelToHigh() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            foreach (Cache cache in cacheDao.GetAll()) {
                Assert.IsNotNull(cache);
                cache.AreaLevel = 10;
                try {
                    cacheDao.Update(cache);
                } finally {
                    DeleteTestData();
                }
            }
        }
        #endregion
    }
}