﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class RateDaoTest {
        #region private HelperClasses
        private void CreateTestData() {
            UnitTestUtil.CreateTestCaches(DALFactory.GetCacheDao());
            UnitTestUtil.CreateTestRatings(DALFactory.GetRatingDao());
        }

        private void DeleteTestData() {
            UnitTestUtil.DeleteTestRatings(DALFactory.GetRatingDao());
            UnitTestUtil.DeleteTestCaches(DALFactory.GetCacheDao());
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void RatingDaoTest_GetCountOfEntries_NoEntryFound() {
            IRatingDao rateDao = DALFactory.GetRatingDao();
            DeleteTestData();
            Assert.AreEqual(0, rateDao.CountEntries());
            IEnumerable<Rating> rateList = rateDao.GetAll();
            if (rateList != null) {
                IEnumerator<Rating> rateIt = rateList.GetEnumerator();
                Assert.IsNull(rateIt.Current);
            }
        }


        [TestMethod]
        public void RatingDaoTest_CreateAndDeleteRating() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }


        [TestMethod]
        public void RatingDaoTest_GetAll() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());

            IEnumerable<Rating> ratingList = ratingDao.GetAll();
            if (ratingDao != null) {
                int countRating = 0;
                foreach (Rating rating in ratingList) {
                    countRating++;
                }
                Assert.AreEqual(3, countRating);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void RatingDaoTest_Get() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());

            IEnumerable<Rating> ratingList = ratingDao.GetAll();
            if (ratingDao != null) {
                Rating rating2 = null;
                foreach (Rating rating in ratingList) {
                    rating2 = ratingDao.Get(rating.Id);
                    Assert.AreEqual(rating.Id, rating2.Id);
                    Assert.AreEqual(rating.CacheId, rating2.CacheId);
                    Assert.AreEqual(rating.CreationDate, rating2.CreationDate);
                    Assert.AreEqual(rating.Rate, rating2.Rate);
                    Assert.AreEqual(rating.UserId, rating2.UserId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void RatingDaoTest_CacheDao_IsCacheInUse() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheDao.IsCacheInUse(cache.Id));
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void RatingDaoTest_FilterCache_CheckRatings() {
            ICacheDao cacheDao = DALFactory.GetCacheDao();
            DeleteTestData();
            CreateTestData();
            List<int> ratings = new List<int>();
            ratings.Add(1);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.AreEqual(0, cacheList.Count());
            
            ratings.Add(3);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            ratings.Add(5);
            cacheList = cacheDao.FilterCaches(new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void LogBookDaoTest_GetAllLogBookEntriesOfCache() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());
            Assert.IsNotNull(ratingDao);

            ICacheDao cacheDao = DALFactory.GetCacheDao();
            Assert.IsNotNull(cacheDao);
            Assert.AreEqual(3, cacheDao.CountEntries());
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            Assert.IsNotNull(cacheList);

            if (cacheDao != null && cacheList != null) {
                foreach (Cache cache in cacheList) {
                    IEnumerable<Rating> ratingList = ratingDao.GetAllRatingsOfCache(cache.Id);
                    Assert.IsNotNull(ratingList);
                    Assert.AreEqual(1, ratingList.Count());

                    ratingList = ratingDao.GetAllRatingsOfCache(0);
                    Assert.IsNull(ratingList);
                }
            }
            DeleteTestData();
        }

        #endregion

        #region Test Update

        [TestMethod]
        public void RatingDaoTest_ChangeRate() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());
            Assert.IsNotNull(ratingDao);

            IEnumerable<Rating> ratingList = ratingDao.GetAll();
            if (ratingList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.IsNotNull(rating);
                    rating.Rate = 4;
                    ratingDao.Update(rating);
                }
            }
            ratingList = ratingDao.GetAll();
            if (ratingList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.AreEqual(4, rating.Rate);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void RatingDaoTest_ChangeUser() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());
            Assert.IsNotNull(ratingDao);

            IEnumerable<Rating> ratingList = ratingDao.GetAll();
            if (ratingList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.IsNotNull(rating);
                    rating.UserId = 1;
                    ratingDao.Update(rating);
                }
            }
            ratingList = ratingDao.GetAll();
            if (ratingList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.AreEqual(1, rating.UserId);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void RatingDaoTest_ChangeCache() {
            IRatingDao ratingDao = DALFactory.GetRatingDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(3, ratingDao.CountEntries());
            Assert.IsNotNull(ratingDao);

            ICacheDao cacheDao = DALFactory.GetCacheDao();
            IEnumerable<Cache> cacheList = cacheDao.GetAll();
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(3, cacheList.Count());
            IEnumerator<Cache> cacheIt = cacheList.GetEnumerator();
            Assert.IsNotNull(cacheIt);
            Cache cache = null;
            if (cacheIt.MoveNext()) {
                cache = cacheIt.Current;
            }

            IEnumerable<Rating> ratingList = ratingDao.GetAll();
            if (ratingList != null && cacheList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.IsNotNull(rating);

                    rating.CacheId = cache.Id;
                    ratingDao.Update(rating);
                }
            }
            ratingList = ratingDao.GetAll();
            if (ratingList != null) {
                foreach (Rating rating in ratingList) {
                    Assert.AreEqual(cache.Id, rating.CacheId);
                }
            }
            DeleteTestData();
        }

        #endregion
    }
}