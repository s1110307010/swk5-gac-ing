﻿namespace GeoCaching.UnitTest {
    using System;
    using GeoCaching.DAL;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class UserDaoTest {
        #region private HelperClasses
        private void CreateTestData() {
            UnitTestUtil.CreateTestCaches(DALFactory.GetCacheDao());
            UnitTestUtil.CreateTestLogBooks(DALFactory.GetLogBookDao());
        }

        private void DeleteTestData() {
            UnitTestUtil.DeleteTestLogBooks(DALFactory.GetLogBookDao());
            UnitTestUtil.DeleteTestCaches(DALFactory.GetCacheDao());
//            UnitTestUtil.DeleteTestUsers(DALManager.GetUserDao());
        }

        private void RestoreUserData() {
            UnitTestUtil.RestoreTestUsers(DALFactory.GetUserDao());
        }
        #endregion

        #region Other Tests

        [TestMethod]
        public void UserDaoTest_GetCountOfEntries_NoEntryFound() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            IEnumerable<User> userList = userDao.GetAll();
            if (userList != null) {
                IEnumerator<User> userIt = userList.GetEnumerator();
                Assert.IsNull(userIt.Current);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_CreateAndDeleteTestData() {
            RestoreUserData();
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }


        [TestMethod]
        public void UserDaoTest_GetAll() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            
            IEnumerable<User> userList = userDao.GetAll();
            Assert.IsNotNull(userList);
            if (userList != null) {
                int countUser = 0;
                foreach (User user in userList) {
                    countUser++;
                }
                Assert.AreEqual(2, countUser);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserDaoTest_Get() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());

            IEnumerable<User> userList = userDao.GetAll();
            Assert.IsNotNull(userList);
            if (userList != null) {
                User user2 = null;
                foreach (User user in userList) {
                    user2 = userDao.Get(user.Id);
                    Assert.AreEqual(user.Id, user2.Id);
                    Assert.AreEqual(user.Email, user2.Email);
                    Assert.AreEqual(user.Name, user2.Name);
                    Assert.AreEqual(user.Password, user2.Password);
                    Assert.AreEqual(user.ResidenceCoordinates.Latitude, user2.ResidenceCoordinates.Latitude);
                    Assert.AreEqual(user.ResidenceCoordinates.Longitude, user2.ResidenceCoordinates.Longitude);
                    Assert.AreEqual(user.ResidenceCoordinates.SpatialReferenceId, user2.ResidenceCoordinates.SpatialReferenceId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserDaoTest_GetUserByName_Correct() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());

            User user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            Assert.AreEqual(user.Email, "andreas.etzelstorfer@schachermayer.at");

            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_GetUserByName_NoUserFound() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());

            User user = userDao.GetByUserName("NoUserFound");
            Assert.IsNull(user);
            

            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_ExistsUser_NoUserFound() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.IsFalse(userDao.Exists("Andreas Etzelstorfer - not found"));
            Assert.IsTrue(userDao.Exists("Andreas Etzelstorfer"));
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_IsEmailAdressInUse() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.IsFalse(userDao.IsEmailAdressInUse("email.is.not.in@use.at", 1));
            Assert.IsFalse(userDao.IsEmailAdressInUse("andreas.etzelstorfer@schachermayer.at", userDao.GetByUserName("Andreas Etzelstorfer").Id));
            Assert.IsTrue(userDao.IsEmailAdressInUse("andreas.etzelstorfer@schachermayer.at", userDao.GetByUserName("Julia Wiesinger").Id));
            DeleteTestData();
            RestoreUserData();
        }
       #endregion

        #region Test Update

        [TestMethod]
        public void UserDaoTest_ChangeEmail() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            Assert.IsNotNull(userDao);

            User user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Email = "andreas.etzelstorfer2222@schachermayer.at";
                userDao.Update(user);
            }
            user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("andreas.etzelstorfer2222@schachermayer.at", user.Email);
                user.Email = "andreas.etzelstorfer@schachermayer.at";
                userDao.Update(user);
                Assert.AreEqual("andreas.etzelstorfer@schachermayer.at", user.Email);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_ChangeName() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            Assert.IsNotNull(userDao);

            User user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Name = "Andreas Etzelstorfer (Test)";
                userDao.Update(user);
            }
            user = userDao.GetByUserName("Andreas Etzelstorfer (Test)");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("Andreas Etzelstorfer (Test)", user.Name);
                user.Name = "Andreas Etzelstorfer";
                userDao.Update(user);
                Assert.AreEqual("Andreas Etzelstorfer", user.Name);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserDaoTest_ChangePassword() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            Assert.IsNotNull(userDao);

            User user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Password = "xxx";
                userDao.Update(user);
            }
            user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("xxx", user.Password);
                user.Password = "swk5";
                userDao.Update(user);
                Assert.AreEqual("swk5", user.Password);
            }
            DeleteTestData();
            RestoreUserData();
        }

        
        [TestMethod]
        public void UserDaoTest_CreateTestUser() {
            UnitTestUtil.CreateTestUsers(DALFactory.GetUserDao());
            UnitTestUtil.DeleteTestUsers(DALFactory.GetUserDao());
        }

        [TestMethod]
        public void UserDaoTest_ChangeCoordinate() {
            RestoreUserData();
            IUserDao userDao = DALFactory.GetUserDao();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userDao.CountEntries());
            Assert.IsNotNull(userDao);

            Coordinate co = new Coordinate();
            co.Latitude = 44.444444;
            co.Longitude = 55.555555;
            co.SpatialReferenceId = 4326;


            User user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.ResidenceCoordinates = co;
                userDao.Update(user);
            }
            user = userDao.GetByUserName("Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual(co.Latitude, user.ResidenceCoordinates.Latitude);
                Assert.AreEqual(co.Longitude, user.ResidenceCoordinates.Longitude);
                Assert.AreEqual(co.SpatialReferenceId, user.ResidenceCoordinates.SpatialReferenceId);


                co.Latitude = 51.46189;
                co.Longitude = -0.926690;

                user.ResidenceCoordinates = co;
                userDao.Update(user);
                Assert.AreEqual(co.Latitude, user.ResidenceCoordinates.Latitude);
                Assert.AreEqual(co.Longitude, user.ResidenceCoordinates.Longitude);
                Assert.AreEqual(co.SpatialReferenceId, user.ResidenceCoordinates.SpatialReferenceId);
            }
            DeleteTestData();
            RestoreUserData();
        }

        #endregion
    }
}