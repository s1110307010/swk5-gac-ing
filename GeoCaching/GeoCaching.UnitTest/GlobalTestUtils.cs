﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Linq;
    using System.Data;
    using GeoCaching.DAL;
    using GeoCaching.Entities;
	using System.Diagnostics.CodeAnalysis;

	[ExcludeFromCodeCoverage]
    public static class GlobalTestUtils {
        #region Constants

        const string DELETE_USER = "DELETE FROM [dbo].[User] WHERE [Id] = @id";
        const string DELETE_ALL_PICTURES = "DELETE FROM [dbo].[Picture]";
        const string DELETE_ALL_RATINGS = "DELETE FROM [dbo].[Rate]";
        const string DELETE_ALL_LOGBOOK = "DELETE FROM [dbo].[LogBook]";
        const string DELETE_ALL_CACHES = "DELETE FROM [dbo].[Cache]";
        const string DELETE_ALL_ROLES = "DELETE FROM [dbo].[User_Role]";

        #endregion

        #region Create Test Enities

        public static User CreateUser(string username, string password,
            string email, double longitude, double latitude, int spatialRef) {
            User user = new User();
            user.Name = username;
            user.Password = password;
            user.Email = email;

            Coordinate co = new Coordinate();
            co.Longitude = longitude;
            co.Latitude = latitude;
            co.SpatialReferenceId = spatialRef;

            user.ResidenceCoordinates = co;
            return user;
        }

        public static Cache CreateCache(string name, int ownerId, string puzzle,
                int searchLevel, int areaLevel, CacheSize size, CacheState state,
                CacheType type, double longitude, double latitude, int spatialRef) {
            Cache cache = new Cache();

            cache.Name = name;
            cache.OwnerId = ownerId;
            cache.Puzzle = puzzle;
            cache.SearchLevel = searchLevel;
            cache.Size = size;
            cache.State = state;
            cache.Type = type;
            cache.AreaLevel = areaLevel;
            cache.CreationDate = DateTime.Now;

            Coordinate co = new Coordinate();
            co.Longitude = longitude;
            co.Latitude = latitude;
            co.SpatialReferenceId = spatialRef;
            cache.Coordinates = co;

            return cache;
        }

        public static Picture CreatePicture(int cacheId, byte[] imgData) {
            Picture pic = new Picture();
            pic.CacheId = cacheId;
            pic.ImageData = imgData;
            return pic;
        }

        public static Rating CreateRate(int cacheId, int userId, byte ratingValue) {
            Rating rate = new Rating();

            rate.CacheId = cacheId;
            rate.UserId = userId;
            rate.Rate = ratingValue;
            rate.CreationDate = DateTime.Now;

            return rate;
        }

        public static LogBook CreateLogBook(int cacheId, int userId, SearchState searchState, string comment) {
            LogBook logBook = new LogBook();

            logBook.CacheId = cacheId;
            logBook.UserId = userId;
            logBook.SearchState = searchState;
            logBook.Comment = comment;
            logBook.CreationDate = DateTime.Now;

            return logBook;
        }

        #endregion

        #region Database Methods

        public static bool DeleteUser(int id) {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_USER;

                IDbDataParameter idParam = command.CreateParameter();
                idParam.ParameterName = "@id";
                idParam.Value = id;
                command.Parameters.Add(idParam);

                return command.ExecuteNonQuery() == 1;
            }
        }


        public static int DeleteAllPictures() {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_ALL_PICTURES;
                return command.ExecuteNonQuery();
            }
        }

        public static int DeleteAllRatings() {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_ALL_RATINGS;
                return command.ExecuteNonQuery();
            }
        }

        public static int DeleteAllLogBooks() {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_ALL_LOGBOOK;
                return command.ExecuteNonQuery();
            }
        }

        public static int DeleteAllCaches() {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_ALL_CACHES;
                return command.ExecuteNonQuery();
            }
        }

        public static int DeleteAllRoles() {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = DELETE_ALL_ROLES;
                return command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}