﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class LogBookBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            BLUtils.CreateTestLogBooks(BLFactory.GetLogbookManager());
//            BLUtils.CreateTestPictures(BLFactory.GetCacheManager());
        }

        private void DeleteTestData() {
//            GlobalTestUtils.DeleteAllPictures();
            GlobalTestUtils.DeleteAllLogBooks();
//            GlobalTestUtils.DeleteAllRatings();
            GlobalTestUtils.DeleteAllCaches();
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void LogBookBLTest_GetCountOfEntries_NoEntryFound() {
            ILogbookManager logBookManager = BLFactory.GetLogbookManager();
            BLUtils.DeleteTestLogBooks(BLFactory.GetLogbookManager());
            BLUtils.DeleteTestCaches(BLFactory.GetCacheManager());
            Assert.IsNull(logBookManager.GetAllLogBooks(default(string), default(string)));
            IEnumerable<LogBook> logBookList = logBookManager.GetAllLogBooks(default(string), default(string));
            if (logBookList != null) {
                IEnumerator<LogBook> logBookIt = logBookList.GetEnumerator();
                Assert.IsNull(logBookIt.Current);
            }
        }


        [TestMethod]
        public void logBookManagerTest_CreateAndDeleteLogBook() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }

        [TestMethod]
        public void logBookManagerTest_DeleteLogBook() {
            ILogbookManager logBookManager = BLFactory.GetLogbookManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<LogBook> logBookList = logBookManager.GetAllLogBooks(default(string), default(string));
            Assert.IsNotNull(logBookList);
            if (logBookList != null) {
                foreach (LogBook logBook in logBookList) {
                    logBookManager.DeleteLogBook(default(string), default(string), logBook.Id);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void logBookManagerTest_GetAll() {
            ILogbookManager logBookManager = BLFactory.GetLogbookManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, logBookManager.GetAllLogBooks(default(string), default(string)).Count());

            IEnumerable<LogBook> logBookList = logBookManager.GetAllLogBooks(default(string), default(string));
            if (logBookManager != null) {
                int countLogBook = 0;
                foreach (LogBook logBook in logBookList) {
                    countLogBook++;
                }
                Assert.AreEqual(2, countLogBook);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void LogBookTest_Get() {
            ILogbookManager logBookManager = BLFactory.GetLogbookManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, logBookManager.GetAllLogBooks(default(string), default(string)).Count());

            IEnumerable<LogBook> logBookList = logBookManager.GetAllLogBooks(default(string), default(string));
            if (logBookList != null) {
                LogBook logBook2 = null;
                foreach (LogBook logBook in logBookList) {
                    logBook2 = logBookManager.GetLogBook(default(string), default(string), logBook.Id);
                    Assert.AreEqual(logBook.Id, logBook2.Id);
                    Assert.AreEqual(logBook.CacheId, logBook2.CacheId);
                    Assert.AreEqual(logBook.Comment, logBook2.Comment);
                    Assert.AreEqual(logBook.CreationDate, logBook2.CreationDate);
                    Assert.AreEqual(logBook.SearchState, logBook2.SearchState);
                    Assert.AreEqual(logBook.UserId, logBook2.UserId);
                }
            }
            DeleteTestData();
        }



        [TestMethod]
        public void logBookManagerTest_FilterCache_CheckSearchState() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            SearchStateList searchStates = new SearchStateList();
            searchStates.SearchStates.Add(SearchState.NotFound);

            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, searchStates);
            Assert.AreEqual(0, cacheList.Count());

            searchStates.SearchStates.Add(SearchState.Found);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, searchStates);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void logBookManagerTest_cacheManager_IsCacheInUse() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheManager.IsCacheInUse(default(string), default(string), cache.Id));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void logBookManagerTest_GetAllLogBookEntriesOfCache() {
            DeleteTestData();
            CreateTestData();
            ILogbookManager logBookManager = BLFactory.GetLogbookManager();
            Assert.AreEqual(2, logBookManager.GetAllLogBooks(default(string), default(string)).Count());
            Assert.IsNotNull(logBookManager);

            ICacheManager cacheManager = BLFactory.GetCacheManager();
            Assert.IsNotNull(cacheManager);
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            Assert.IsNotNull(cacheList);

            if (cacheManager != null && cacheList != null) {
                foreach (Cache cache in cacheList) {
                    IEnumerable<LogBook> logBookList = logBookManager.GetAllLogBookEntriesOfCache(default(string), default(string), cache.Id);
                    Assert.IsNotNull(logBookList);
                    Assert.AreEqual(1, logBookList.Count());

                    logBookList = logBookManager.GetAllLogBookEntriesOfCache(default(string), default(string), 0);
                    Assert.IsNull(logBookList);
                }
            }
            DeleteTestData();
        }
        #endregion

        #region statistics

        [TestMethod]
        public void logBookManagerTest_GetCountOfCachesByState() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            ILogbookManager logbookManager = BLFactory.GetLogbookManager();
            DeleteTestData();
            CreateTestData();

            CacheList caches = new CacheList() { Caches = cacheManager.GetAllCaches(default(string), default(string)).ToList() };

            Assert.AreEqual(2, logbookManager.GetCountOfCachesByState(default(string), default(string), 1, caches, SearchState.Found));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 2, caches, SearchState.Found));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 3, caches, SearchState.Found));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 3, null, SearchState.Found));

            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 1, caches, SearchState.NotFound));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 2, caches, SearchState.NotFound));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 3, caches, SearchState.NotFound));
            Assert.AreEqual(0, logbookManager.GetCountOfCachesByState(default(string), default(string), 3, null, SearchState.NotFound));

            DeleteTestData();
        }
        #endregion
    }
}