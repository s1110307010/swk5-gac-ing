﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class ExportBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            BLUtils.CreateTestPictures(BLFactory.GetCacheManager());
        }

        private void DeleteTestData() {
            GlobalTestUtils.DeleteAllPictures();
            GlobalTestUtils.DeleteAllCaches();
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void ExportManagerTest_ExportCacheToWord() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            IExportManager exportManager = BLFactory.GetExportManager();

            CreateTestData();
            CacheList cacheList = new CacheList() { Caches = cacheManager.GetAllCaches(default(string), default(string)).ToList() };
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            exportManager.ExportCachesToWord(default(string), default(string), cacheList);    
            DeleteTestData();
        }
        #endregion
    }
}