﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class RateBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            BLUtils.CreateTestRatings(BLFactory.GetRatingManager());
//            BLUtils.CreateTestPictures(BLFactory.GetCacheManager());
        }

        private void DeleteTestData() {
//            GlobalTestUtils.DeleteAllPictures();
//            GlobalTestUtils.DeleteAllLogBooks();
            GlobalTestUtils.DeleteAllRatings();
            GlobalTestUtils.DeleteAllCaches();
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void ratingManagerTest_GetCountOfEntries_NoEntryFound() {
            IRatingManager ratingManager = BLFactory.GetRatingManager();
            DeleteTestData();
            Assert.IsNull(ratingManager.GetAllRatings(default(string), default(string)));
            IEnumerable<Rating> rateList = ratingManager.GetAllRatings(default(string), default(string));
            if (rateList != null) {
                IEnumerator<Rating> rateIt = rateList.GetEnumerator();
                Assert.IsNull(rateIt.Current);
            }
        }


        [TestMethod]
        public void ratingManagerTest_CreateAndDeleteRating() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }


        [TestMethod]
        public void ratingManagerTest_GetAll() {
            IRatingManager ratingManager = BLFactory.GetRatingManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, ratingManager.GetAllRatings(default(string), default(string)).Count());

            IEnumerable<Rating> ratingList = ratingManager.GetAllRatings(default(string), default(string));
            if (ratingManager != null) {
                int countRating = 0;
                foreach (Rating rating in ratingList) {
                    countRating++;
                }
                Assert.AreEqual(2, countRating);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void ratingManagerTest_Get() {
            IRatingManager ratingManager = BLFactory.GetRatingManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, ratingManager.GetAllRatings(default(string), default(string)).Count());

            IEnumerable<Rating> ratingList = ratingManager.GetAllRatings(default(string), default(string));
            if (ratingManager != null) {
                Rating rating2 = null;
                foreach (Rating rating in ratingList) {
                    rating2 = ratingManager.GetRating(default(string), default(string), rating.Id);
                    Assert.AreEqual(rating.Id, rating2.Id);
                    Assert.AreEqual(rating.CacheId, rating2.CacheId);
                    Assert.AreEqual(rating.CreationDate, rating2.CreationDate);
                    Assert.AreEqual(rating.Rate, rating2.Rate);
                    Assert.AreEqual(rating.UserId, rating2.UserId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void ratingManagerTest_CacheDao_IsCacheInUse() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheManager.IsCacheInUse(default(string), default(string), cache.Id));
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void ratingManagerTest_FilterCache_CheckRatings() {
            ICacheManager cacheDao = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            RatingList ratings = new RatingList();
            ratings.Ratings.Add(1);

            IEnumerable<Cache> cacheList = cacheDao.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.AreEqual(0, cacheList.Count());

            ratings.Ratings.Add(3);
            cacheList = cacheDao.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            ratings.Ratings.Add(5);
            cacheList = cacheDao.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), ratings, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void LogBookManagerTest_GetAllLogBookEntriesOfCache() {
            IRatingManager ratingManager = BLFactory.GetRatingManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, ratingManager.GetAllRatings(default(string), default(string)).Count());
            Assert.IsNotNull(ratingManager);

            ICacheManager cacheManager = BLFactory.GetCacheManager();
            Assert.IsNotNull(cacheManager);
            Assert.AreEqual(2, ratingManager.GetAllRatings(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            Assert.IsNotNull(cacheList);

            if (cacheManager != null && cacheList != null) {
                foreach (Cache cache in cacheList) {
                    IEnumerable<Rating> ratingList = ratingManager.GetAllRatingsOfCache(default(string), default(string), cache.Id);
                    Assert.IsNotNull(ratingList);
                    Assert.AreEqual(1, ratingList.Count());

                    ratingList = ratingManager.GetAllRatingsOfCache(default(string), default(string), 0);
                    Assert.IsNull(ratingList);
                }
            }
            DeleteTestData();
        }

        #endregion
    }
}