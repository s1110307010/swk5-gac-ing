﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class UserBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            BLUtils.CreateTestLogBooks(BLFactory.GetLogbookManager());
        }

        private void DeleteTestData() {
            GlobalTestUtils.DeleteAllLogBooks();
            GlobalTestUtils.DeleteAllCaches();
        }

        private void RestoreUserData() {
            BLUtils.RestoreTestUsers(BLFactory.GetUserManager());
        }
        #endregion

        #region Other Tests

        [TestMethod]
        public void UserManagerTest_GetCountOfEntries_NoEntryFound() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            IEnumerable<User> userList = userManager.GetAllUsers(default(string), default(string));
            if (userList != null) {
                IEnumerator<User> userIt = userList.GetEnumerator();
                Assert.IsNull(userIt.Current);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_CreateAndDeleteTestData() {
            RestoreUserData();
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }


        [TestMethod]
        public void UserManagerTest_GetAll() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());

            IEnumerable<User> userList = userManager.GetAllUsers(default(string), default(string));
            Assert.IsNotNull(userList);
            if (userList != null) {
                int countUser = 0;
                foreach (User user in userList) {
                    countUser++;
                }
                Assert.AreEqual(2, countUser);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_Get() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());

            IEnumerable<User> userList = userManager.GetAllUsers(default(string), default(string));
            Assert.IsNotNull(userList);
            if (userList != null) {
                User user2 = null;
                foreach (User user in userList) {
                    user2 = userManager.GetUserById(default(string), default(string), user.Id);
                    Assert.AreEqual(user.Id, user2.Id);
                    Assert.AreEqual(user.Email, user2.Email);
                    Assert.AreEqual(user.Name, user2.Name);
                    Assert.AreEqual(user.Password, user2.Password);
                    Assert.AreEqual(user.ResidenceCoordinates.Latitude, user2.ResidenceCoordinates.Latitude);
                    Assert.AreEqual(user.ResidenceCoordinates.Longitude, user2.ResidenceCoordinates.Longitude);
                    Assert.AreEqual(user.ResidenceCoordinates.SpatialReferenceId, user2.ResidenceCoordinates.SpatialReferenceId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_GetUserByName_Correct() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            Assert.AreEqual(user.Email, "andreas.etzelstorfer@schachermayer.at");

            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_GetUserByName_NoUserFound() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());

            User user = userManager.GetUserByName(default(string), default(string), "NoUserFound");
            Assert.IsNull(user);


            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_ExistsUser_NoUserFound() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.IsFalse(userManager.ExistsUser(default(string), default(string), "Andreas Etzelstorfer - not found"));
            Assert.IsFalse(userManager.ExistsUser(default(string), default(string), null));
            Assert.IsFalse(userManager.ExistsUser(default(string), default(string), ""));
            Assert.IsTrue(userManager.ExistsUser(default(string), default(string), "Andreas Etzelstorfer"));
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_IsEmailAdressInUse() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.IsFalse(userManager.IsEmailAdressInUse(default(string), default(string), "email.is.not.in@use.at", 1));
            Assert.IsFalse(userManager.IsEmailAdressInUse(default(string), default(string), "email.is.not.in@use.at", default(int)));
            Assert.IsFalse(userManager.IsEmailAdressInUse(default(string), default(string), null, default(int)));
            Assert.IsFalse(userManager.IsEmailAdressInUse(default(string), default(string), "", default(int)));
            Assert.IsFalse(userManager.IsEmailAdressInUse(default(string), default(string), "andreas.etzelstorfer@schachermayer.at", userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer").Id));
            Assert.IsTrue(userManager.IsEmailAdressInUse(default(string), default(string), "andreas.etzelstorfer@schachermayer.at", userManager.GetUserByName(default(string), default(string), "Julia Wiesinger").Id));
            DeleteTestData();
            RestoreUserData();
        }
        #endregion

        #region Test Update

        [TestMethod]
        public void UserManagerTest_UpdateUser_CacheIsNotValid_NameIsNull() {
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Name = null;
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
                user.Name = "";
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_UpdateUser_CacheIsNotValid_PasswordIsNull() {
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Password = null;
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
                user.Id = 0;
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_UpdateUser_CacheIsNotValid_EmailIsInvalid() {
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Email = null;
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
                user.Email = "wrong email adress fromat";
                Assert.IsFalse(userManager.Update(default(string), default(string), user));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_UpdateUser_CacheIsNotValid_CoordinatesAreInvalid() {
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                    Assert.IsNotNull(user);
                    user.ResidenceCoordinates.Latitude = -100;
                    Assert.IsFalse(userManager.Update(default(string), default(string), user));
                    user.ResidenceCoordinates.Latitude = 100;
                    Assert.IsFalse(userManager.Update(default(string), default(string), user));
                    user.ResidenceCoordinates.Latitude = 10;
                    user.ResidenceCoordinates.Longitude = -190;
                    Assert.IsFalse(userManager.Update(default(string), default(string), user));
                    user.ResidenceCoordinates.Longitude = 190;
                    Assert.IsFalse(userManager.Update(default(string), default(string), user));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void UserManagerTest_ChangeEmail() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Email = "andreas.etzelstorfer2222@schachermayer.at";
                userManager.Update(default(string), default(string), user);
            }
            user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("andreas.etzelstorfer2222@schachermayer.at", user.Email);
                user.Email = "andreas.etzelstorfer@schachermayer.at";
                userManager.Update(default(string), default(string), user);
                Assert.AreEqual("andreas.etzelstorfer@schachermayer.at", user.Email);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_ChangeName() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Name = "Andreas Etzelstorfer (Test)";
                userManager.Update(default(string), default(string), user);
            }
            user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer (Test)");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("Andreas Etzelstorfer (Test)", user.Name);
                user.Name = "Andreas Etzelstorfer";
                userManager.Update(default(string), default(string), user);
                Assert.AreEqual("Andreas Etzelstorfer", user.Name);
            }
            DeleteTestData();
            RestoreUserData();
        }

        [TestMethod]
        public void UserManagerTest_ChangePassword() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Password = "xxx";
                userManager.Update(default(string), default(string), user);
            }
            user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual("xxx", user.Password);
                user.Password = "swk5";
                userManager.Update(default(string), default(string), user);
                Assert.AreEqual("swk5", user.Password);
            }
            DeleteTestData();
            RestoreUserData();
        }


        [TestMethod]
        public void UserManagerTest_CreateTestUser() {
            BLUtils.CreateTestUsers(BLFactory.GetUserManager());
            BLUtils.DeleteTestUsers(BLFactory.GetUserManager());
        }

        [TestMethod]
        public void UserManagerTest_CreateTestUser_UserAlreadyExists() {
            BLUtils.CreateTestUsers(BLFactory.GetUserManager());
            IUserManager userManager = BLFactory.GetUserManager();
            Assert.IsFalse(userManager.Create(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer")));
            BLUtils.DeleteTestUsers(BLFactory.GetUserManager());
        }

        [TestMethod]
        public void UserManagerTest_CreateTestUser_WrongUser() {
            BLUtils.CreateTestUsers(BLFactory.GetUserManager());
            IUserManager userManager = BLFactory.GetUserManager();
            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.Name = "";
                Assert.IsFalse(userManager.Create(default(string), default(string), user));
            }
            BLUtils.DeleteTestUsers(BLFactory.GetUserManager());
        }

        [TestMethod]
        public void UserManagerTest_ChangeCoordinate() {
            RestoreUserData();
            IUserManager userManager = BLFactory.GetUserManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            Assert.IsNotNull(userManager);

            Coordinate co = new Coordinate();
            co.Latitude = 44.444444;
            co.Longitude = 55.555555;
            co.SpatialReferenceId = 4326;


            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                user.ResidenceCoordinates = co;
                userManager.Update(default(string), default(string), user);
            }
            user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer");
            Assert.IsNotNull(user);
            if (user != null) {
                Assert.AreEqual(co.Latitude, user.ResidenceCoordinates.Latitude);
                Assert.AreEqual(co.Longitude, user.ResidenceCoordinates.Longitude);
                Assert.AreEqual(co.SpatialReferenceId, user.ResidenceCoordinates.SpatialReferenceId);


                co.Latitude = 51.46189;
                co.Longitude = -0.926690;

                user.ResidenceCoordinates = co;
                userManager.Update(default(string), default(string), user);
                Assert.AreEqual(co.Latitude, user.ResidenceCoordinates.Latitude);
                Assert.AreEqual(co.Longitude, user.ResidenceCoordinates.Longitude);
                Assert.AreEqual(co.SpatialReferenceId, user.ResidenceCoordinates.SpatialReferenceId);
            }
            DeleteTestData();
            RestoreUserData();
        }

        #endregion
    }
}