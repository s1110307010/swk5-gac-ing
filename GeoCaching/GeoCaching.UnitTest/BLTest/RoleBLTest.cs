﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class RoleBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestRoles(BLFactory.GetRoleManager());
        }

        private void DeleteTestData() {
            GlobalTestUtils.DeleteAllRoles();
        }
        #endregion

		#region Other Tests

        [TestMethod]
        public void RoleManagerTest_GetAllRoles() {
            DeleteTestData();
            CreateTestData();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            IEnumerable<Role> roleList = roleManager.GetAllRoles(default(string), default(string));
            Assert.IsNotNull(roleList);
            Assert.AreEqual(2, roleList.Count());
        }

        [TestMethod]
        public void RoleManagerTest_GetRolesOfUser() {
            DeleteTestData();
            CreateTestData();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            IEnumerable<Role> roleList = roleManager.GetRolesOfUser(default(string), default(string), 0);
            Assert.IsNull(roleList);
            roleList = roleManager.GetRolesOfUser(default(string), default(string), 1);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(2, roleList.Count());
            roleList = roleManager.GetRolesOfUser(default(string), default(string), 2);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(1, roleList.Count());
            roleList = roleManager.GetRolesOfUser(default(string), default(string), 3);
            Assert.IsNotNull(roleList);
            Assert.AreEqual(1, roleList.Count());
        }

        [TestMethod]
        public void RoleManagerTest_RemoveRoleFromUser() {
            DeleteTestData();
        }

        [TestMethod]
        public void RoleManagerTest_RemoveRoleFromUser_NoUserEntered() {
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            roleManager.RemoveRolesFromUser(default(string), default(string), 1, new RoleList());
        }

        [TestMethod]
        public void RoleManagerTest_RemoveRoleFromUser_NoRoleEntered() {
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            roleManager.RemoveRolesFromUser(default(string), default(string), 1, new RoleList());
        }

        [TestMethod]
        public void RoleManagerTest_AddRoleToUser() {
            DeleteTestData();
            CreateTestData();
        }

        [TestMethod]
        public void RoleManagerTest_AddRoleToUser_NoUserEntered() {
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            roleManager.AssignRolesToUser(default(string), default(string), 1, new RoleList());
        }

        [TestMethod]
        public void RoleManagerTest_AddRoleToUser_NoRoleEntered() {
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            roleManager.AssignRolesToUser(default(string), default(string), 1, new RoleList());
        }

        [TestMethod]
        public void RoleManagerTest_IsUserHider_NoRoleFound() {
            IUserManager userManager = BLFactory.GetUserManager();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            DeleteTestData();
            Assert.IsFalse(roleManager.IsUserHider(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer").Id));
            Assert.IsFalse(roleManager.IsUserHider(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Julia Wiesinger").Id));
        }

        [TestMethod]
        public void RoleManagerTest_IsUserHider_RoleFound() {
            IUserManager userManager = BLFactory.GetUserManager();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            DeleteTestData();
            CreateTestData();
            Assert.IsTrue(roleManager.IsUserHider(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer").Id));
            Assert.IsFalse(roleManager.IsUserHider(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Julia Wiesinger").Id));
            DeleteTestData();
        }

        [TestMethod]
        public void RoleManagerTest_IsUserFinder_NoRoleFound() {
            IUserManager userManager = BLFactory.GetUserManager();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            DeleteTestData();
            Assert.IsFalse(roleManager.IsUserFinder(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer").Id));
            Assert.IsFalse(roleManager.IsUserFinder(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Julia Wiesinger").Id));
        }

        [TestMethod]
        public void RoleManagerTest_IsUserFinder_RoleFound() {
            IUserManager userManager = BLFactory.GetUserManager();
            IRoleManager roleManager = BLFactory.GetRoleManager();
            Assert.IsNotNull(roleManager);
            DeleteTestData();
            CreateTestData();
            Assert.IsTrue(roleManager.IsUserFinder(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer").Id));
            Assert.IsTrue(roleManager.IsUserFinder(default(string), default(string), userManager.GetUserByName(default(string), default(string), "Julia Wiesinger").Id));
            DeleteTestData();
        }
        #endregion
    }
}