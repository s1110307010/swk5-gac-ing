﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Linq;
    using System.Data;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using GeoCaching.Server;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public static class BLUtils {
        #region Cache Utilities
        public static void CreateTestCaches(Server.ICacheManager cacheManager) {
            //          Assert.AreEqual(0, cacheManager.GetAllCaches(default(string), default(string)).Count());
            foreach (Cache cache in BLCreateTestCacheList()) {
                Assert.IsTrue(cacheManager.CreateCache(default(string), default(string), cache));
            }
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
        }

        public static void DeleteTestCaches(Server.ICacheManager cacheManager) {
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheManager.DeleteCache(default(string), default(string), cache.Id));
                }
            }
            //             Assert.AreEqual(0, cacheManager.GetAllCaches(default(string), default(string)).Count());
        }

        public static System.Collections.Generic.IEnumerable<Cache> BLCreateTestCacheList() {
            return new List<Cache> {
                GlobalTestUtils.CreateCache("Cache1", 1, "Puzzle", 1, 4, CacheSize.Normal, CacheState.Available, CacheType.DriveIn, 47.735367, 13.339183, 4326),
                GlobalTestUtils.CreateCache("Cache2", 2, "Puzzle2", 4, 2, CacheSize.Small, CacheState.Available, CacheType.Moving, 40.735367, 23.339183, 4326),
                GlobalTestUtils.CreateCache("Cache3", 2, "Puzzle3", 3, 1, CacheSize.None, CacheState.NotAvailable, CacheType.Quiz, 45.735367, 17.339183, 4326)};
        }



        public static void CreateTestPictures(Server.ICacheManager cacheManager) {
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Picture picture in BLCreateTestPictureList()) {
                    Assert.IsTrue(cacheManager.AddPicture(default(string), default(string), picture));
                }
            }
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
        }

        public static void DeleteTestPictures(Server.ICacheManager cacheManager) {
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                IEnumerable<Picture> pictureList = null;
                foreach (Cache cache in cacheList) {
                    pictureList = cacheManager.GetPicturesOfCache(default(string), default(string), cache.Id);
                    if (pictureList != null) {
                        foreach (Picture picture in pictureList) {
                            Assert.IsTrue(cacheManager.RemovePicture(default(string), default(string), picture.Id));
                        }
                    }
                    Assert.IsNull(cacheManager.GetPicturesOfCache(default(string), default(string), cache.Id));
                }
            }
        }

        public static IEnumerable<Picture> BLCreateTestPictureList() {
            List<Picture> picList = null;
            IEnumerable<Cache> cacheList = BLFactory.GetCacheManager().GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                picList = new List<Picture>();
                foreach (Cache cache in cacheList) {
                    picList.Add(GlobalTestUtils.CreatePicture(cache.Id, new Byte[] { 1, 2, 3, 4 }));
                }
            }
            return picList;
        }
        #endregion

        #region Rating Utilities
        public static void CreateTestRatings(Server.IRatingManager ratingManager) {
            //            Assert.AreEqual(0, ratingManager.GetAllRatings(default(string), default(string)).Count());
            foreach (Rating rate in BLCreateTestRatingList()) {
                Assert.IsTrue(ratingManager.CreateRating(default(string), default(string), rate));
            }
            Assert.AreEqual(2, ratingManager.GetAllRatings(default(string), default(string)).Count());
        }

        public static void DeleteTestRatings(Server.IRatingManager ratingManager) {
            IEnumerable<Rating> cacheList = ratingManager.GetAllRatings(default(string), default(string));
            if (cacheList != null) {
                foreach (Rating rate in cacheList) {
                    Assert.IsTrue(ratingManager.DeleteRating(default(string), default(string), rate.Id));
                }
            }
            //            Assert.AreEqual(0, ratingManager.GetAllRatings(default(string), default(string)).Count());
        }

        public static IEnumerable<Rating> BLCreateTestRatingList() {
            IList<Rating> rateList = new List<Rating>();
            if (rateList != null) {
                IEnumerable<Cache> cacheList = BLFactory.GetCacheManager().GetAllCaches(default(string), default(string));
                if (cacheList != null) {
                    foreach (Cache cache in cacheList) {
                        rateList.Add(GlobalTestUtils.CreateRate(cache.Id, 1, 3));
                    }
                }
            }
            return rateList;
        }
        #endregion

        #region LogBook Utitilies
        public static void CreateTestLogBooks(Server.ILogbookManager logBookManager) {
            //            Assert.AreEqual(0, logBookManager.GetAllLogBooks().Count());
            foreach (LogBook logBook in BLCreateTestLogBookList()) {
                Assert.IsTrue(logBookManager.CreateLogBook(default(string), default(string), logBook));
            }
            Assert.AreEqual(2, logBookManager.GetAllLogBooks(default(string), default(string)).Count());
        }

        public static void DeleteTestLogBooks(Server.ILogbookManager logBookManager) {
            IEnumerable<LogBook> cacheList = logBookManager.GetAllLogBooks(default(string), default(string));
            if (cacheList != null) {
                foreach (LogBook logBook in cacheList) {
                    Assert.IsTrue(logBookManager.DeleteLogBook(default(string), default(string), logBook.Id));
                }
            }
            //            Assert.AreEqual(0, logBookManager.GetAllLogBooks().Count());
        }

        public static IEnumerable<LogBook> BLCreateTestLogBookList() {
            IList<LogBook> logBookList = new List<LogBook>();
            if (logBookList != null) {
                IEnumerable<Cache> cacheList = BLFactory.GetCacheManager().GetAllCaches(default(string), default(string));
                if (cacheList != null) {
                    foreach (Cache cache in cacheList) {
                        logBookList.Add(GlobalTestUtils.CreateLogBook(cache.Id, 1, SearchState.Found, "Comment" + cache.Id));
                    }
                }
            }
            return logBookList;
        }
        #endregion

        #region User Utilities
        public static void RestoreTestUsers(Server.IUserManager userManager) {
            IEnumerable<User> userList = userManager.GetAllUsers(default(string), default(string));
            Assert.IsNotNull(userList);
            if (userList != null) {
                foreach (User user in userList) {
                    if (user.Id != 3) {
                        Assert.IsTrue(userManager.Reactivate(default(string), default(string), user.Id));
                    }
                }
                Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
            }
        }

        public static void CreateTestUsers(IUserManager userManager) {
            DeleteTestUsers(userManager);
            foreach (User user in BLCreateTestUserList()) {
                Assert.IsTrue(userManager.Create(default(string), default(string), user));
            }
            Assert.AreEqual(5, userManager.GetAllUsers(default(string), default(string)).Count());
        }

        public static void DeleteTestUsers(IUserManager userManager) {
            User user = userManager.GetUserByName(default(string), default(string), "Andreas Etzelstorfer2");
            if (user != null) {
                Assert.IsTrue(userManager.Delete(default(string), default(string), user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            user = userManager.GetUserByName(default(string), default(string), "Julia2");
            if (user != null) {
                Assert.IsTrue(userManager.Delete(default(string), default(string), user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            user = userManager.GetUserByName(default(string), default(string), "Test User2");
            if (user != null) {
                Assert.IsTrue(userManager.Delete(default(string), default(string), user.Id));
                Assert.IsTrue(GlobalTestUtils.DeleteUser(user.Id));
            }
            Assert.AreEqual(2, userManager.GetAllUsers(default(string), default(string)).Count());
        }

        public static IEnumerable<User> BLCreateTestUserList() {
            return new List<User> {
                GlobalTestUtils.CreateUser("Andreas Etzelstorfer2", "password", "a.b2@sch.at", 47.735367, 13.339183, 4326),
                GlobalTestUtils.CreateUser("Julia2", "Wiesinger2", "u.w2b@sch.at", 40.735367, 23.339183, 4326),
                GlobalTestUtils.CreateUser("Test User2", "password2", "t.u2@sch.at", 45.735367, 17.339183, 4326)};
        }
        #endregion

        #region Role Utilities
        public static void CreateTestRoles(Server.IRoleManager roleManager) {
            Assert.IsNotNull(roleManager);
            if (roleManager != null) {
                RoleList roleList = new RoleList() { Roles = roleManager.GetAllRoles(default(string), default(string)).ToList() };
                roleManager.AssignRolesToUser(default(string), default(string), 1, roleList);
                
                roleList = null;
                roleList = new RoleList() { Roles = new List<Role>() { roleManager.GetRoleById(default(string), default(string), 2) } };
                roleManager.AssignRolesToUser(default(string), default(string), 2, roleList);

                roleList = null;
                roleList = new RoleList() { Roles = new List<Role>() { roleManager.GetRoleByName(default(string), default(string), "Hider") } };
                roleManager.AssignRolesToUser(default(string), default(string), 3, roleList);

                Assert.AreEqual(2, roleManager.GetRolesOfUser(default(string), default(string), 1).Count());
                Assert.AreEqual(1, roleManager.GetRolesOfUser(default(string), default(string), 2).Count());
                Assert.AreEqual(1, roleManager.GetRolesOfUser(default(string), default(string), 3).Count());
            }
        }

        public static void DeleteTestRoles(Server.IRoleManager roleManager) {
            int userId = 0;
            IEnumerable<Role> roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
            Assert.IsNull(roleList);

            roleManager.RemoveRolesFromUser(default(string), default(string), userId, new RoleList() { Roles = roleManager.GetAllRoles(default(string), default(string)).ToList() });
            roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
            Assert.IsNull(roleList);

            userId = 1;
            roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(2, roleList.Count());

                roleManager.RemoveRolesFromUser(default(string), default(string), userId, new RoleList() { Roles = roleManager.GetAllRoles(default(string), default(string)).ToList() });
                roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
                Assert.IsNull(roleList);
            }

            userId = 2;
            roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(1, roleList.Count());

                roleManager.RemoveRolesFromUser(default(string), default(string), userId, new RoleList() { Roles = roleManager.GetAllRoles(default(string), default(string)).ToList() });
                roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
                Assert.IsNull(roleList);
            }

            userId = 3;
            roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
            if (roleList != null) {
                Assert.IsNotNull(roleList);
                Assert.AreEqual(1, roleList.Count());

                roleManager.RemoveRolesFromUser(default(string), default(string), userId, new RoleList() { Roles = roleManager.GetAllRoles(default(string), default(string)).ToList() });
                roleList = roleManager.GetRolesOfUser(default(string), default(string), userId);
                Assert.IsNull(roleList);
            }
        }
        #endregion
    }
}