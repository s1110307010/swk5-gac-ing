﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class AuthenticationBLTest {
        #region Other Tests
        [TestMethod]
        public void AuthenticationTest_ValidateUser_InputIsNotValid() {
            IAuthentificationManager authenticationManager = BLFactory.GetAuthenticationManager();

            Assert.IsNull(authenticationManager.ValidateUser(null, "swk5"));
            Assert.IsNull(authenticationManager.ValidateUser("Andreas Etzelstorfer", null));
            Assert.IsNull(authenticationManager.ValidateUser("", "swk5"));
            Assert.IsNull(authenticationManager.ValidateUser("Andreas Etzelstorfer", ""));
        }

        [TestMethod]
        public void AuthenticationTest_ValidateUser_UserDoNotExist() {
            IAuthentificationManager authenticationManager = BLFactory.GetAuthenticationManager();

            Assert.IsNull(authenticationManager.ValidateUser("User do not Exist", "swk5"));
        }

        [TestMethod]
        public void AuthenticationTest_ValidateUser_PasswordIsNotCorrect() {
            IAuthentificationManager authenticationManager = BLFactory.GetAuthenticationManager();

            Assert.IsNull(authenticationManager.ValidateUser("Andreas Etzelstorfer", "password is wrong"));
            Assert.IsNull(authenticationManager.ValidateUser("Julia Wiesinger", "password is wrong"));
        }

        [TestMethod]
        public void AuthenticationTest_ValidateUser_Successful() {
            IAuthentificationManager authenticationManager = BLFactory.GetAuthenticationManager();
            IUserManager userManager = BLFactory.GetUserManager();
            IList<string> userList = new List<string>() { "Andreas Etzelstorfer", "Julia Wiesinger" };

            foreach (string username in userList) {
                User user = authenticationManager.ValidateUser(username, "swk5");
                User user2 = userManager.GetUserByName(default(string), default(string), username);

                Assert.IsNotNull(user);
                Assert.IsNotNull(user2);
                Assert.AreEqual(user.Id, user2.Id);
                Assert.AreEqual(user.Name, user2.Name);
                Assert.AreEqual(user.Password, user2.Password);
                Assert.AreEqual(user.Email, user2.Email);
                Assert.AreEqual(user.ResidenceCoordinates.Latitude, user2.ResidenceCoordinates.Latitude);
                Assert.AreEqual(user.ResidenceCoordinates.Longitude, user2.ResidenceCoordinates.Longitude);
                Assert.AreEqual(user.ResidenceCoordinates.SpatialReferenceId, user2.ResidenceCoordinates.SpatialReferenceId);
            }
        }
        #endregion
    }
}
