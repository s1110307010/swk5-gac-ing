﻿namespace GeoCaching.UnitTest {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using GeoCaching.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCaching.Server;
	using System.Diagnostics.CodeAnalysis;

    [TestClass]
	[ExcludeFromCodeCoverage]
    public class CacheBLTest {
        #region private HelperClasses
        private void CreateTestData() {
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            BLUtils.CreateTestPictures(BLFactory.GetCacheManager());
        }

        private void DeleteTestData() {
            GlobalTestUtils.DeleteAllPictures();
            GlobalTestUtils.DeleteAllLogBooks();
            GlobalTestUtils.DeleteAllRatings();
            GlobalTestUtils.DeleteAllCaches();
        }
        #endregion

        #region Other Tests
        [TestMethod]
        public void CacheManagerTest_GetCountOfEntries_NoEntryFound() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            var cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(0, cacheList.Count());
            }
        }

        [TestMethod]
        public void CacheManagerTest_CreateAndDeleteCache() {
            DeleteTestData();
            CreateTestData();
            DeleteTestData();
        }

        [TestMethod]
        public void CacheManagerTest_CreateDuplicateCache() {
            DeleteTestData();
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            Assert.IsTrue(cacheManager.CreateCache(default(string), default(string), GlobalTestUtils.CreateCache("Cache1", 1, "Puzzle", 1, 4, CacheSize.Normal, CacheState.Available, CacheType.DriveIn, 47.735367, 13.339183, 4326)));
            Assert.IsFalse(cacheManager.CreateCache(default(string), default(string), GlobalTestUtils.CreateCache("Cache1", 1, "Puzzle", 1, 4, CacheSize.Normal, CacheState.Available, CacheType.DriveIn, 47.735367, 13.339183, 4326)));
            DeleteTestData();
        }

        [TestMethod]
        public void CacheManagerTest_CreateCache_CacheNotValid() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            Assert.IsFalse(cacheManager.CreateCache(default(string), default(string), GlobalTestUtils.CreateCache("", 1, "", 1, 4, CacheSize.Normal, CacheState.Available, CacheType.DriveIn, 47.735367, 13.339183, 4326)));
            DeleteTestData();
        }

        [TestMethod]
        public void CacheManagerTest_GetAll() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                int countCaches = 0;
                foreach (Cache cache in cacheList) {
                    countCaches++;
                }
                Assert.AreEqual(2, countCaches);
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_Get() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Cache cache2 = null;
                foreach (Cache cache in cacheList) {
                    cache2 = cacheManager.GetCacheById(default(string), default(string), cache.Id);
                    Assert.AreEqual(cache.Id, cache2.Id);
                    Assert.AreEqual(cache.CreationDate, cache2.CreationDate);
                    Assert.AreEqual(cache.AreaLevel, cache2.AreaLevel);
                    Assert.AreEqual(cache.Coordinates.Latitude, cache2.Coordinates.Latitude);
                    Assert.AreEqual(cache.Coordinates.Longitude, cache2.Coordinates.Longitude);
                    Assert.AreEqual(cache.Coordinates.SpatialReferenceId, cache2.Coordinates.SpatialReferenceId);
                    Assert.AreEqual(cache.Name, cache2.Name);
                    Assert.AreEqual(cache.OwnerId, cache2.OwnerId);
                    Assert.AreEqual(cache.Puzzle, cache2.Puzzle);
                    Assert.AreEqual(cache.SearchLevel, cache2.SearchLevel);
                    Assert.AreEqual(cache.Size, cache2.Size);
                    Assert.AreEqual(cache.State, cache2.State);
                    Assert.AreEqual(cache.Type, cache2.Type);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_GetCacheById() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Cache cache2 = null;
                foreach (Cache cache in cacheList) {
                    cache2 = cacheManager.GetCacheByName(default(string), default(string), cache.Name);
                    Assert.AreEqual(cache.Id, cache2.Id);
                    Assert.AreEqual(cache.CreationDate, cache2.CreationDate);
                    Assert.AreEqual(cache.AreaLevel, cache2.AreaLevel);
                    Assert.AreEqual(cache.Coordinates.Latitude, cache2.Coordinates.Latitude);
                    Assert.AreEqual(cache.Coordinates.Longitude, cache2.Coordinates.Longitude);
                    Assert.AreEqual(cache.Coordinates.SpatialReferenceId, cache2.Coordinates.SpatialReferenceId);
                    Assert.AreEqual(cache.Name, cache2.Name);
                    Assert.AreEqual(cache.OwnerId, cache2.OwnerId);
                    Assert.AreEqual(cache.Puzzle, cache2.Puzzle);
                    Assert.AreEqual(cache.SearchLevel, cache2.SearchLevel);
                    Assert.AreEqual(cache.Size, cache2.Size);
                    Assert.AreEqual(cache.State, cache2.State);
                    Assert.AreEqual(cache.Type, cache2.Type);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_IsCacheInUse() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsFalse(cacheManager.IsCacheInUse(default(string), default(string), cache.Id));
                }
                Assert.IsFalse(cacheManager.IsCacheInUse(default(string), default(string), 0));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_DeleteCache() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            BLUtils.CreateTestCaches(BLFactory.GetCacheManager());
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsTrue(cacheManager.DeleteCache(default(string), default(string), cache.Id));
                }
                Assert.IsFalse(cacheManager.DeleteCache(default(string), default(string), 0));
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_DeleteCache_PicturesExists() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            GlobalTestUtils.DeleteAllLogBooks();
            GlobalTestUtils.DeleteAllRatings();
            foreach (Cache cache in cacheManager.GetAllCaches(default(string), default(string))) {
                Assert.IsTrue(cacheManager.DeleteCache(default(string), default(string), cache.Id));                
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_DeleteCache_NotPossibleBecauseInUse() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            BLUtils.CreateTestRatings(BLFactory.GetRatingManager());
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsFalse(cacheManager.DeleteCache(default(string), default(string), cache.Id));
                }
                Assert.IsFalse(cacheManager.DeleteCache(default(string), default(string), 0));
            }
            BLUtils.DeleteTestRatings(BLFactory.GetRatingManager());
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_GetCacheById_FoundEntry() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Cache cache2 = cacheManager.GetCacheById(default(string), default(string), cache.Id);
                    Assert.AreEqual(cache.Id, cache2.Id);
                    Assert.AreEqual(cache2.Name, cache2.Name);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_GetAllCaches_NoEntryFound() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            Assert.IsNull(cacheManager.GetAllCaches(default(string), default(string)));
        }

        [TestMethod]
        public void cacheManagerTest_Exists() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.IsTrue(cacheManager.Exists(default(string), default(string), "Cache1"));
            Assert.IsTrue(cacheManager.Exists(default(string), default(string), "Cache2"));
            Assert.IsTrue(cacheManager.Exists(default(string), default(string), "Cache3"));
            Assert.IsFalse(cacheManager.Exists(default(string), default(string), "Cache do not exist"));
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_FilterCache_NoEntry() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(2011, 11, 4), null, null, null, null, null, default(int), null, null);
            Assert.AreEqual(0, cacheList.Count());
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_FilterCache_AllEntryFound() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 1, null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 50, null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, null, new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 0, null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());
            DeleteTestData();
        }


        [TestMethod]
        public void cacheManagerTest_FilterCache_CheckCacheTypes() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            CacheTypeList cacheTypes = new CacheTypeList();

            cacheTypes.CacheTypes.Add(CacheType.DriveIn);
            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());
            
            cacheTypes.CacheTypes.Add(CacheType.Multi);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.MathPhysics);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Moving);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Multi);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Other);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Quiz);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Traditional);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Virtual);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            cacheTypes.CacheTypes.Add(CacheType.Webcam);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), cacheTypes, null, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_FilterCache_CheckCacheSizes() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            CacheSizeList cacheSizes = new CacheSizeList();
            cacheSizes.CacheSizes.Add(CacheSize.Normal);

            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.CacheSizes.Add(CacheSize.None);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.CacheSizes.Add(CacheSize.Big);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.CacheSizes.Add(CacheSize.Mikro);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.CacheSizes.Add(CacheSize.Other);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            cacheSizes.CacheSizes.Add(CacheSize.Small);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, cacheSizes, null, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }


        [TestMethod]
        public void cacheManagerTest_FilterCache_CheckAreaLevels() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            AreaLevelList areaLevels = new AreaLevelList();
            areaLevels.AreaLevels.Add(1);

            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(0, cacheList.Count());

            areaLevels.AreaLevels.Add(2);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            areaLevels.AreaLevels.Add(3);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            areaLevels.AreaLevels.Add(4);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            areaLevels.AreaLevels.Add(5);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, areaLevels, null, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_FilterCache_CheckSearchLevels() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            SearchLevelList searchLevels = new SearchLevelList();
			searchLevels.SearchLevels.Add(1);

            IEnumerable<Cache> cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            searchLevels.SearchLevels.Add(2);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            searchLevels.SearchLevels.Add(3);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(1, cacheList.Count());

            searchLevels.SearchLevels.Add(4);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            searchLevels.SearchLevels.Add(5);
            cacheList = cacheManager.FilterCaches(default(string), default(string), new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), null, null, null, searchLevels, null, default(int), null, null);
            Assert.IsNotNull(cacheList);
            Assert.AreEqual(2, cacheList.Count());

            DeleteTestData();
        }

        #endregion

        #region Picture Tests
        [TestMethod]
        public void cacheManagerTest_DeleteAllPictures() {
            DeleteTestData();
            CreateTestData();
        }



        [TestMethod]
        public void cacheManagerTest_GetCountOfPictures_EntryFound() {
            int countPictures = 0;
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    IEnumerable<Picture> picList = cacheManager.GetPicturesOfCache(default(string), default(string), cache.Id);
                    Assert.IsNotNull(picList);
                    countPictures = 0;
                    foreach (Picture picture in picList) {
                        countPictures++;
                    }
                    Assert.AreEqual(1, countPictures);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_AddPictureToCache() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cacheManager.GetPicturesOfCache(default(string), default(string), cache.Id);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_RemovePictureFromCache() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    IEnumerable<Picture> pictureList = cacheManager.GetPicturesOfCache(default(string), default(string), cache.Id);
                    if (pictureList != null) {
                        foreach (Picture picture in pictureList) {
                            Assert.IsTrue(cacheManager.RemovePicture(default(string), default(string), picture.Id));
                        }
                    }
                }
            }
            DeleteTestData();
        }

        #endregion

        #region Cache Statistics Tests
        [TestMethod]
        public void CacheBLTest_GetAreaLevelPercentage() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.33, cacheManager.GetAreaLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.33, cacheManager.GetAreaLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(0, cacheManager.GetAreaLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(33.33, cacheManager.GetAreaLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(0, cacheManager.GetAreaLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 5));
            Assert.AreEqual(33.33, cacheManager.GetAreaLevelPercentage(default(string), default(string), new Coordinate() { Longitude = 45.735367, Latitude = 17.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.33, cacheManager.GetAreaLevelPercentage(default(string), default(string), new Coordinate() { Longitude = 40.735367, Latitude = 25.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
        }

        [TestMethod]
        public void CacheBLTest_GetSearchLevelPercentage() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.33, cacheManager.GetSearchLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(0, cacheManager.GetSearchLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 2));
            Assert.AreEqual(33.33, cacheManager.GetSearchLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 3));
            Assert.AreEqual(33.33, cacheManager.GetSearchLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 4));
            Assert.AreEqual(0, cacheManager.GetSearchLevelPercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 5));
            Assert.AreEqual(33.33, cacheManager.GetSearchLevelPercentage(default(string), default(string), new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
            Assert.AreEqual(33.33, cacheManager.GetSearchLevelPercentage(default(string), default(string), new Coordinate() { Longitude = 40.735367, Latitude = 20.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), 1));
        }

        [TestMethod]
        public void CacheBLTest_GetSearchStateCountOfCache() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            foreach (Cache cache in cacheList) {
                Assert.AreEqual(0, cacheManager.GetSearchStateCountOfCache(default(string), default(string), cache.Id, SearchState.Found));
            }
        }

        [TestMethod]
        public void CacheBLTest_GetSizePercentage() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(0, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Big));
            Assert.AreEqual(0, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Mikro));
            Assert.AreEqual(33.33, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.None));
            Assert.AreEqual(33.33, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Normal));
            Assert.AreEqual(0, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Other));
            Assert.AreEqual(33.33, cacheManager.GetSizePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Small));
            Assert.AreEqual(33.33, cacheManager.GetSizePercentage(default(string), default(string), new Coordinate() { Longitude = 40.735367, Latitude = 23.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Small));
            Assert.AreEqual(33.33, cacheManager.GetSizePercentage(default(string), default(string), new Coordinate() { Longitude = 35.735367, Latitude = 28.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheSize.Small));
        }

        [TestMethod]
        public void CacheBLTest_GetTypePercentage() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.DriveIn));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.MathPhysics));
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Moving));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Multi));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Other));
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Quiz));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Traditional));
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Quiz));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Traditional));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Virtual));
            Assert.AreEqual(0, cacheManager.GetTypePercentage(default(string), default(string), null, 0, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.Webcam));
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), new Coordinate() { Longitude = 47.735367, Latitude = 13.339183, SpatialReferenceId = 4326 }, 1, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.DriveIn));
            Assert.AreEqual(33.33, cacheManager.GetTypePercentage(default(string), default(string), new Coordinate() { Longitude = 40.735367, Latitude = 20.339183, SpatialReferenceId = 4326 }, 10, new DateTime(2007, 5, 26), new DateTime(9999, 11, 4), CacheType.DriveIn));
        }

        [TestMethod]
        public void CacheBLTest_GetCountOfHidedCaches() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();

            CacheList caches = new CacheList() { Caches = cacheManager.GetAllCaches(default(string), default(string)).ToList() };

            Assert.AreEqual(1, cacheManager.GetCountOfHidedCaches(default(string), default(string), 1, caches));
            Assert.AreEqual(1, cacheManager.GetCountOfHidedCaches(default(string), default(string), 2, caches));
            Assert.AreEqual(0, cacheManager.GetCountOfHidedCaches(default(string), default(string), 3, caches));
            Assert.AreEqual(0, cacheManager.GetCountOfHidedCaches(default(string), default(string), 3, null));
            
            DeleteTestData();
        }
        #endregion        
        
        #region Update Caches
        [TestMethod]
        public void cacheManagerTest_UpdateCache_CacheIsNotValid_NameIsNull() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Name = null;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_UpdateCache_CacheIsNotValid_SerchLevelIsInvalid() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.SearchLevel = 0;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                    cache.SearchLevel = 10;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_UpdateCache_CacheIsNotValid_AreaLevelIsInvalid() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.AreaLevel = 0;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                    cache.AreaLevel = 10;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_UpdateCache_CacheIsNotValid_PuzzleIsNull() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Puzzle = null;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                }
            }
            DeleteTestData();
        }

        //[TestMethod]
        //public void cacheManagerTest_UpdateCache_CacheIsNotValid_SizeIsInvalid() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
        //    if (cacheList != null) {
        //        foreach (Cache cache in cacheList) {
        //            Assert.IsNotNull(cache);
        //            cache.Size = default(CacheSize);
        //            Assert.IsFalse(cacheManager.UpdateCache(cache));
        //        }
        //    }
        //    DeleteTestData();
        //}

        //[TestMethod]
        //public void cacheManagerTest_UpdateCache_CacheIsNotValid_TypeIsInvalid() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
        //    if (cacheList != null) {
        //        foreach (Cache cache in cacheList) {
        //            Assert.IsNotNull(cache);
        //            cache.Type = default(CacheType);
        //            Assert.IsFalse(cacheManager.UpdateCache(cache));
        //        }
        //    }
        //    DeleteTestData();
        //}

        //[TestMethod]
        //public void cacheManagerTest_UpdateCache_CacheIsNotValid_StateIsInvalid() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
        //    if (cacheList != null) {
        //        foreach (Cache cache in cacheList) {
        //            Assert.IsNotNull(cache);
        //            cache.State = 0;
        //            Assert.IsFalse(cacheManager.UpdateCache(cache));
        //        }
        //    }
        //    DeleteTestData();
        //}

        [TestMethod]
        public void cacheManagerTest_UpdateCache_CacheIsNotValid_CoordinatesAreInvalid() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Coordinates.Latitude = -100;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                    cache.Coordinates.Latitude = 100;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                    cache.Coordinates.Latitude = 10;
                    cache.Coordinates.Longitude = -190;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                    cache.Coordinates.Longitude = 190;
                    Assert.IsFalse(cacheManager.UpdateCache(default(string), default(string), cache));
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangeSearchLevel() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.SearchLevel = 3;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(3, cache.SearchLevel);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangeAreaLevel() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.AreaLevel = 3;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(3, cache.AreaLevel);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangePuzzle() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Puzzle = "Puzzle";
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual("Puzzle", cache.Puzzle);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangeName() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            int cacheId = default(int);
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Name = "TestName";
                    cacheManager.UpdateCache(default(string), default(string), cache);
                    cacheId = cache.Id;
                    break;
                }
            }
            Cache testCache = cacheManager.GetCacheById(default(string), default(string), cacheId);
            Assert.AreEqual("TestName", testCache.Name);
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangeCoordinate() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            Coordinate co = new Coordinate();
            co.Latitude = 44.444444;
            co.Longitude = 55.555555;
            co.SpatialReferenceId = 4326;
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Coordinates = co;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(co.Latitude, cache.Coordinates.Latitude);
                    Assert.AreEqual(co.Longitude, cache.Coordinates.Longitude);
                    Assert.AreEqual(co.SpatialReferenceId, cache.Coordinates.SpatialReferenceId);
                }
            }
            DeleteTestData();
        }

        [TestMethod]
        public void cacheManagerTest_ChangeOwnerId() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.OwnerId = 2;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheManager.GetAllCaches(default(string), default(string))) {
                    Assert.AreEqual(2, cache.OwnerId);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void cacheManagerTest_ChangeSize() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Size = CacheSize.Big;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheSize.Big, cache.Size);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void cacheManagerTest_ChangeStateId() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.State = CacheState.Available;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheState.Available, cache.State);
                }
            }
            DeleteTestData();
        }


        [TestMethod]
        public void cacheManagerTest_ChangeTypeId() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            IEnumerable<Cache> cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                foreach (Cache cache in cacheList) {
                    Assert.IsNotNull(cache);
                    cache.Type = CacheType.Moving;
                    cacheManager.UpdateCache(default(string), default(string), cache);
                }
            }
            cacheList = cacheManager.GetAllCaches(default(string), default(string));
            if (cacheList != null) {
                Assert.AreEqual(2, cacheManager.GetAllCaches(default(string), default(string)).Count());
                foreach (Cache cache in cacheList) {
                    Assert.AreEqual(CacheType.Moving, cache.Type);
                }
            }
            DeleteTestData();
        }
        #endregion

        #region Expected SqlException
        //[TestMethod]
        //[ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        //public void CachDaoTest_InsertDuplicateCache() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    foreach (Cache cache in UnitTestUtil.DalCreateTestCacheList()) {
        //        try {
        //            Assert.IsFalse(cacheManager.CreateCache(cache));
        //        } finally {
        //            DeleteTestData();
        //        }
        //    }
        //}

        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void CachDaoTest_ChangeOwnerId_UserNotExists() {
            ICacheManager cacheManager = BLFactory.GetCacheManager();
            DeleteTestData();
            CreateTestData();
            foreach (Cache cache in cacheManager.GetAllCaches(default(string), default(string))) {
                Assert.IsNotNull(cache);
                cache.OwnerId = -1;
                try {
                    cacheManager.UpdateCache(default(string), default(string), cache);
                } finally {
                    DeleteTestData();
                }
            }
        }

        //[TestMethod]
        //[ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        //public void CachDaoTest_ChangeSearchLevel_SearchLevelToHigh() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    foreach (Cache cache in cacheManager.GetAllCaches(default(string), default(string))) {
        //        Assert.IsNotNull(cache);
        //        cache.SearchLevel = 10;
        //        try {
        //            cacheManager.UpdateCache(cache);
        //        } finally {
        //            DeleteTestData();
        //        }
        //    }
        //}

        //[TestMethod]
        //[ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        //public void CachDaoTest_ChangeAreaLevel_AreaLevelToHigh() {
        //    ICacheManager cacheManager = BLFactory.GetCacheManager();
        //    DeleteTestData();
        //    CreateTestData();
        //    foreach (Cache cache in cacheManager.GetAllCaches(default(string), default(string))) {
        //        Assert.IsNotNull(cache);
        //        cache.AreaLevel = 10;
        //        try {
        //            cacheManager.UpdateCache(cache);
        //        } finally {
        //            DeleteTestData();
        //        }
        //    }
        //}
        #endregion    
    }
}