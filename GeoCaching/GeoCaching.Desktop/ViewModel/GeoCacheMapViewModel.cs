﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using Microsoft.Maps.MapControl.WPF;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Input;

	public class GeoCacheMapViewModel : ViewModelBase<GeoCacheMapViewModel> {
		#region Variables
        string username;
        string password;
        ICacheManager cacheManager;
		ICommand filterCommand;
		#endregion

		#region Constructors
		public GeoCacheMapViewModel() {
			cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;

			#region Pushpins
			this.PushpinCollection = new ObservableCollection<Pushpin>();
			SetPushpins(this.PushpinCollection, cacheManager.GetAllCaches(UserName, Password));
			#endregion

			#region FilterValues
			this.SearchLevels = new ObservableCollection<Data>() { 
					new Data() { Value = "1" }, 
					new Data() { Value = "2" }, 
					new Data() { Value = "3" }, 
					new Data() { Value = "4" }, 
					new Data() { Value = "5" }
				};

			this.AreaLevels = new ObservableCollection<Data> { 
					new Data() { Value = "1" }, 
					new Data() { Value = "2" }, 
					new Data() { Value = "3" }, 
					new Data() { Value = "4" }, 
					new Data() { Value = "5" }
				};

			ObservableCollection<Data> sizes = new ObservableCollection<Data>();
			foreach (CacheSize size in Enum.GetValues(typeof(CacheSize)))
				sizes.Add(new Data() { Value = size.ToString() });
			this.Sizes = sizes;

			ObservableCollection<Data> types = new ObservableCollection<Data>();
			foreach (CacheType type in Enum.GetValues(typeof(CacheType)))
				types.Add(new Data() { Value = type.ToString() });
			this.Types = types;
			#endregion
        }
		#endregion

		#region Commands
		/// <summary>
		/// Gets or sets the value for FilterCommand
		/// </summary>
		public ICommand FilterCommand {
			get {
				if (filterCommand == null) {
					filterCommand = new RelayCommand((item) => {
                        this.SetPushpins(this.PushpinCollection, cacheManager.FilterCaches(UserName, Password, 
                            new DateTime(1753, 1, 1),
                            DateTime.Now,
                            new CacheTypeList() { CacheTypes = this.Types.Where(t => t.IsSelected).Select(t => (CacheType)Enum.Parse(typeof(CacheType), t.Value)).ToList() },
                            new CacheSizeList() { CacheSizes = this.Sizes.Where(t => t.IsSelected).Select(t => (CacheSize)Enum.Parse(typeof(CacheSize), t.Value)).ToList() },
                            new AreaLevelList() { AreaLevels = this.AreaLevels.Where(t => t.IsSelected).Select(t => Convert.ToInt32(t.Value)).ToList() },
                            new SearchLevelList() { SearchLevels = this.SearchLevels.Where(t => t.IsSelected).Select(t => Convert.ToInt32(t.Value)).ToList() },
                            null, default(int), null, null));
					});
				}

				return filterCommand;
			}
		}
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        #region PushpinCollection
		public ObservableCollection<Pushpin> PushpinCollection {
			get {
				return this.pushpinCollection;
			}
			set {
				if (value != this.pushpinCollection) {
					this.pushpinCollection = value;
					RaisePropertyChangedEvent(vm => vm.PushpinCollection);
				}
			}
		}

		private ObservableCollection<Pushpin> pushpinCollection;
		#endregion

		#region SearchLevels
		/// <summary>
		/// Gets or sets the value for SearchLevels
		/// </summary>
		public ObservableCollection<Data> SearchLevels {
			get {
				return this.searchLevels;
			}
			set {
				if (value != this.searchLevels) {
					this.searchLevels = value;
					RaisePropertyChangedEvent(vm => vm.SearchLevels);
				}
			}
		}

		/// <summary>
		/// The local value field for SearchLevels
		/// </summary>
		private ObservableCollection<Data> searchLevels;
		#endregion

		#region AreaLevels
		/// <summary>
		/// Gets or sets the value for AreaLevels
		/// </summary>
		public ObservableCollection<Data> AreaLevels {
			get {
				return this.areaLevels;
			}
			set {
				if (value != this.areaLevels) {
					this.areaLevels = value;
					RaisePropertyChangedEvent(vm => vm.AreaLevels);
				}
			}
		}

		/// <summary>
		/// The local value field for AreaLevels
		/// </summary>
		private ObservableCollection<Data> areaLevels;
		#endregion

		#region Sizes
		/// <summary>
		/// Gets or sets the value for Sizes
		/// </summary>
		public ObservableCollection<Data> Sizes {
			get {
				return this.sizes;
			}
			set {
				if (value != this.sizes) {
					this.sizes = value;
					RaisePropertyChangedEvent(vm => vm.Sizes);
				}
			}
		}

		/// <summary>
		/// The local value field for Sizes
		/// </summary>
		private ObservableCollection<Data> sizes;
		#endregion

		#region Types
		/// <summary>
		/// Gets or sets the value for Types
		/// </summary>
		public ObservableCollection<Data> Types {
			get {
				return this.types;
			}
			set {
				if (value != this.types) {
					this.types = value;
					RaisePropertyChangedEvent(vm => vm.Types);
				}
			}
		}

		/// <summary>
		/// The local value field for Types
		/// </summary>
		private ObservableCollection<Data> types;
		#endregion
		#endregion

		#region Public Methods
		public void p_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {
			Cache cache = ((sender as Pushpin).DataContext as Cache);
			MessageBox.Show(GeoCachingHelper.GetToolTipText(cache), cache.Name);
			e.Handled = true;
		}

		public void p_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
			GeoCachingHelper.PinDoubleClick(sender, e, cacheManager, this.PushpinCollection);	
		}
		#endregion

		#region Private Methods
		private void SetPushpins(ObservableCollection<Pushpin> pushpins, IEnumerable<Cache> geoCaches) {
			if (geoCaches != null) {
				pushpins.Clear();
				foreach (Cache cache in geoCaches) {
					Pushpin p = new Pushpin() { Location = new Location(cache.Coordinates.Latitude, cache.Coordinates.Longitude) };
					p.DataContext = cache;
					p.MouseRightButtonDown += p_MouseRightButtonDown;
					p.MouseDoubleClick += p_MouseDoubleClick;
					ToolTipService.SetToolTip(p, GeoCachingHelper.GetToolTipText(cache));
					pushpins.Add(p);
				}
			}
		}
		#endregion
	}

	public class Data {
		/// <summary>
		/// Gets or sets the value for Value
		/// </summary>
		public string Value { get; set; }


		/// <summary>
		/// Gets or sets the value for IsSelected
		/// </summary>
		public bool IsSelected { get; set; }
	}
}