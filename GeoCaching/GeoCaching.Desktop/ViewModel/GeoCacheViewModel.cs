﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Input;
	using System.IO;

	public class GeoCacheViewModel : ViewModelBase<GeoCacheViewModel> {
		#region Variables
		Cache cache;
        string username;
        string password;
		bool isSelected;
		Picture selectedPicture;
		ICommand saveCommand;
		ICommand deleteCommand;
		ICommand addPictureCommand;
		ICommand removePictureCommand;
		ICacheManager cacheManager;
		#endregion

		#region Constructors
		public GeoCacheViewModel(ObservableCollection<GeoCacheViewModel> allGeoCaches) {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());
			cache = new Cache();
			cache.Coordinates = new Coordinate();
			cache.CreationDate = DateTime.Now;
			this.GeoCacheCollection = allGeoCaches;
		}
		#endregion

		#region Commands
		/// <summary>
		/// Gets or sets the value for DeleteCommand
		/// </summary>
		public ICommand DeleteCommand {
			get {
				if (deleteCommand == null)
					deleteCommand = new RelayCommand((item) => {
                        if (cacheManager.DeleteCache(UserName, Password, this.CacheItem.Id))
							this.GeoCacheCollection.Remove(this);
						else
							MessageBox.Show("Der GeoCache kann nicht gelöscht werden, da dieser in Verwendung ist!");
					});

				return deleteCommand;
			}
		}

		/// <summary>
		/// Gets or sets the value for EditCommand
		/// </summary>
		public ICommand SaveCommand {
			get {
				if (saveCommand == null)
					saveCommand = new RelayCommand((item) => {

						ICacheManager cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());

						this.CacheItem.OwnerId = UserAuthentificationManager.CurrentUser.Id;
						this.CacheItem.Coordinates.SpatialReferenceId = 4326;

						string error = "Der GeoCache konnte nicht gespeichert werden. Bitte füllen Sie alle Mussfelder aus und kontrollieren Sie Ihre Eingaben!";

						if (this.IsNewGeoCache) {
                            if (cacheManager.CreateCache(UserName, Password, this.CacheItem)) {
								this.IsNewGeoCache = false;
                                Cache cache = cacheManager.GetCacheByName(UserName, Password, this.CacheItem.Name);
								this.CacheItem = cache;

								if (this.GeoCacheCollection != null)
                                    this.GeoCacheCollection.Insert(0, this);

								if (this.CacheSaved != null)
									this.CacheSaved(this, EventArgs.Empty);

							}
							else
								MessageBox.Show(error);
						}
						else {
                            if (BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType()).UpdateCache(UserName, Password, this.CacheItem)) {
								if (this.CacheSaved != null)
									this.CacheSaved(this, EventArgs.Empty);
							}
							else
								MessageBox.Show(error);
						}

					});

				return saveCommand;
			}
		}

		public ICommand AddPictureCommand {
			get {
				if (addPictureCommand == null)
					addPictureCommand = new RelayCommand((item) => {
                        if (this.CacheItem.Id == default(int)) {
                            MessageBox.Show("Um Bilder hinzufügen zu können muss der Cache vorher gespeichert werden!");
                        }
                        else {
                            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                            dlg.FileName = ""; // Default file name
                            dlg.DefaultExt = ".jpg"; // Default file extension
                            dlg.Filter = "Pictures |*.jpg"; // Filter files by extension 

                            // Show save file dialog box
                            Nullable<bool> result = dlg.ShowDialog();

                            // Process save file dialog box results 
                            if (result == true) {
                                // Save document document here 
                                Picture picture = new Picture() { CacheId = this.CacheItem.Id, ImageData = File.ReadAllBytes(dlg.FileName) };
                                if (cacheManager.AddPicture(UserName, Password, picture)) {
                                    if (pictureCollection != null) {
                                        this.pictureCollection.Clear();
                                    }
                                    this.PictureCollection = new ObservableCollection<Picture>(cacheManager.GetPicturesOfCache(UserName, Password, this.CacheItem.Id));
                                }
                                else {
                                    MessageBox.Show("Bild konnte nicht zum Cache hinzugefügt werden!");
                                }

                            }
                        }
					});
				return addPictureCommand;
			}
		}

		public ICommand RemovePictureCommand {
			get {
				if (removePictureCommand == null)
					removePictureCommand = new RelayCommand((item) => {
						if (this.SelectedPicture != null) {
							ICacheManager cache = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());
                            if (cache.RemovePicture(UserName, Password, this.selectedPicture.Id)) {
								this.PictureCollection.Remove(this.selectedPicture);
								this.SelectedPicture = null;
							}
							else {
								MessageBox.Show("Bild konnte nicht gelöscht werden!");
							}
						}
						else {
							MessageBox.Show("Kein Bild zum Löschen selektiert!");
						}
					});
				return removePictureCommand;
			}
		}
		#endregion

		#region Event
		public event EventHandler CacheSaved;
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }
        
        #endregion

        #region Password
        public string Password {
            get { return password; }
        }
        
        #endregion

        #region CacheItem
        /// <summary>
		/// Gets or sets the value for CacheItem
		/// </summary>
		public Cache CacheItem {
			get {
				return this.cache;
			}
			set {
				if (value != this.cache) {
					this.cache = value;

					RaisePropertyChangedEvent(vm => vm.CacheItem);
				}
			}
		}
		#endregion

		#region Sizes
		/// <summary>
		/// Gets or sets the value for Sizes
		/// </summary>
		public Array Sizes {
			get {
				return Enum.GetValues(typeof(CacheSize));
			}
		}
		#endregion

		#region Types
		/// <summary>
		/// Gets or sets the value for Sizes
		/// </summary>
		public Array Types {
			get {
				return Enum.GetValues(typeof(CacheType));
			}
		}
		#endregion

		#region States
		/// <summary>
		/// Gets or sets the value for Sizes
		/// </summary>
		public Array States {
			get {
				return Enum.GetValues(typeof(CacheState));
			}
		}
		#endregion

		#region IsNewGeoCache
		/// <summary>
		/// Gets or sets the value for IsNewGeoCache
		/// </summary>
		public bool IsNewGeoCache { get; set; }
		#endregion

		#region IsSelected
		public bool IsSelected {
			get {
				return isSelected;
			}
			set {
				if (value != this.isSelected) {
					isSelected = value;
					RaisePropertyChangedEvent(vm => vm.IsSelected);
				}
			}
		}
		#endregion

		#region All Geo Caches
		/// <summary>
		/// Gets or sets the value for GeoCacheCollection
		/// </summary>
		public ObservableCollection<GeoCacheViewModel> GeoCacheCollection {
			get {
				return this.geoCacheCollection;
			}
			set {
				if (value != this.geoCacheCollection) {
					this.geoCacheCollection = value;
					RaisePropertyChangedEvent(vm => vm.GeoCacheCollection);
				}
			}
		}

		/// <summary>
		/// The local value field for GeoCacheCollection
		/// </summary>
		private ObservableCollection<GeoCacheViewModel> geoCacheCollection;
		#endregion

		#region All Pictures
		/// <summary>
		/// Gets or sets the value for GeoCacheCollection
		/// </summary>
		public ObservableCollection<Picture> PictureCollection {
			get {
				return this.pictureCollection;
			}
			set {
				if (value != this.pictureCollection) {
					this.pictureCollection = value;
					RaisePropertyChangedEvent(vm => vm.PictureCollection);
				}
			}
		}

		/// <summary>
		/// The local value field for GeoCacheCollection
		/// </summary>
		private ObservableCollection<Picture> pictureCollection;
		#endregion

		#region
		public Picture SelectedPicture {
			get { return selectedPicture; }
			set {
				if (selectedPicture != value) {
					selectedPicture = value;
					RaisePropertyChangedEvent(vm => vm.SelectedPicture);
				}
			}
		}
		#endregion
		#endregion
	}
}