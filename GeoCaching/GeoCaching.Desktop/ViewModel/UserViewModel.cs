﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GeoCaching.Entities;
using GeoCaching.Server;
using System.Collections.ObjectModel;

namespace GeoCaching.Desktop.ViewModel {

	public class UserViewModel : ViewModelBase<UserViewModel> {
		#region Variables
		AbstractUserManager userManager;
		IRoleManager roleManager;
		User user;
        string username;
        string password;
        bool isUserHider;
		bool isUserFinder;
		ICommand deleteCommand;
		ICommand saveCommand;
		#endregion

		#region Constructors
        public UserViewModel(ObservableCollection<UserViewModel> allUsers) {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            userManager = BLFactory.GetUserManager(DataAccessFactory.GetDataAccessType());
            roleManager = BLFactory.GetRoleManager(DataAccessFactory.GetDataAccessType());
            user = new User();
            user.ResidenceCoordinates = new Coordinate();
            this.UserCollection = allUsers;
        }
        #endregion

		#region Commands
		/// <summary>
		/// Gets or sets the value for DeleteCommand
		/// </summary>
		public ICommand DeleteCommand {
			get {
				if (deleteCommand == null)
					deleteCommand = new RelayCommand((item) => {
						if (UserAuthentificationManager.CurrentUser.Id != this.UserItem.Id) {
							this.UserCollection.Remove(this);
							userManager.Delete(UserName, Password, this.UserItem.Id);
						}
						else
							MessageBox.Show("Der aktuell angemeldete Benutzer kann nicht gelöscht werden!");
					});

				return deleteCommand;
			}
		}

		public ICommand SaveCommand {
			get {
				if (saveCommand == null) {
					saveCommand = new RelayCommand(param => {

						string error = "Der Benutzer konnte nicht gespeichert werden. Bitte füllen Sie alle Mussfelder aus und kontrollieren Sie Ihre Eingaben!";

						this.UserItem.ResidenceCoordinates.SpatialReferenceId = 4326;

                        RoleList assignedRoles = new RoleList() { Roles = GetUserRoles().ToList() };
						if (assignedRoles.Count() != 0) {
							if (this.IsNewUser) {
                                if (!userManager.ExistsUser(UserName, Password, this.UserItem.Name)) {
                                    if (!userManager.IsEmailAdressInUse(UserName, Password, this.UserItem.Email, this.UserItem.Id)) {
										User user = null;
                                        if (userManager.Create(UserName, Password, this.UserItem)) {
                                            user = userManager.GetUserByName(UserName, Password, this.UserItem.Name);
                                            this.UserItem = user;
											this.UserCollection.Insert(0, this);
                                            roleManager.RemoveRolesFromUser(UserName, Password, user.Id, new RoleList() { Roles = roleManager.GetAllRoles(UserName, Password).ToList() });
                                            roleManager.AssignRolesToUser(UserName, Password, user.Id, assignedRoles);
											// TODO : HACK
											Application.Current.Windows[1].Close();
										}
										else
											MessageBox.Show(error);
									}
									else
										MessageBox.Show("Es existiert bereits ein Benutzer mit dieser E-Mail Adresse. Bitte wählen Sie eine andere E-Mail Adresse!");
								}
								else
									MessageBox.Show("Ein Benutzer mit diesem Benutzernamen existiert bereits. Bitte wählen Sie einen anderen Benutzernamen!");
							}
							else {
                                if (userManager.Update(UserName, Password, this.UserItem)) {
									user = this.UserItem;
                                    roleManager.RemoveRolesFromUser(UserName, Password, user.Id, new RoleList() { Roles = roleManager.GetAllRoles(UserName, Password).ToList() });
                                    roleManager.AssignRolesToUser(UserName, Password, user.Id, assignedRoles);
									Application.Current.Windows[1].Close();
								}
								else
									MessageBox.Show(error);
							}
						}
						else
							MessageBox.Show(error);
					});
				}
				return saveCommand;
			}
		}
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        #region IsNewUser
		/// <summary>
		/// Gets or sets the value for IsNewUser
		/// </summary>
		public bool IsNewUser { get; set; }
		#endregion

		#region All Users
		/// <summary>
		/// Gets or sets the value for UserCollection
		/// </summary>
		public ObservableCollection<UserViewModel> UserCollection {
			get {
				return this.userCollection;
			}
			set {
				if (value != this.userCollection) {
					this.userCollection = value;
					RaisePropertyChangedEvent(vm => vm.UserCollection);
				}
			}
		}

		/// <summary>
		/// The local value field for UserCollection
		/// </summary>
		private ObservableCollection<UserViewModel> userCollection;
		#endregion

		#region UserItem
		/// <summary>
		/// Gets or sets the value for CacheItem
		/// </summary>
		public User UserItem {
			get {
				return this.user;
			}
			set {
				if (value != this.user) {
					this.user = value;
					RaisePropertyChangedEvent(vm => vm.UserItem);
				}
			}
		}
		#endregion

		#region IsUserHider
		public bool IsUserHider {
			get {
				return this.isUserHider;
			}
			set {
				if (value != this.isUserHider) {
					this.isUserHider = value;
					RaisePropertyChangedEvent(vm => vm.IsUserHider);
				}
			}
		}
		#endregion

		#region IsUserFinder
		public bool IsUserFinder {
			get {
				return this.isUserFinder;
			}
			set {
				if (value != this.isUserFinder) {
					this.isUserFinder = value;
					RaisePropertyChangedEvent(vm => vm.IsUserFinder);
				}
			}
		}
		#endregion
		#endregion

		#region Help Methods
		private IEnumerable<Role> GetUserRoles() {
			IList<Role> roles = new List<Role>();
			if (IsUserFinder)
                roles.Add(roleManager.GetRoleByName(UserName, Password, "Finder"));
			if (IsUserHider)
                roles.Add(roleManager.GetRoleByName(UserName, Password, "Hider"));
			return roles;
		}
		#endregion
	}
}
