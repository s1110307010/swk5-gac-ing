﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using Microsoft.Maps.MapControl.WPF;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Input;

	public class GeoCachingHelper {
        public static string GetToolTipText(Cache cache) {
			return String.Format(
				"{0}{1}{1}Schwierigkeit: {2}{1}Gelände: {3}{1}Größe: {4}{1}Typ: {5}",
				cache.Name,
				Environment.NewLine,
				cache.SearchLevel,
				cache.AreaLevel,
				cache.Size.ToString(),
				cache.Type.ToString());
		}

        public static void PinDoubleClick(object sender, MouseButtonEventArgs e, ICacheManager cacheManager, ObservableCollection<Pushpin> pushpinCollection) {
			Pushpin pin = (sender as Pushpin);
			Cache cache = (pin.DataContext as Cache);
			if (cache.OwnerId == UserAuthentificationManager.CurrentUser.Id) {
				GeoCacheViewModel vm = new GeoCacheViewModel(null);
				vm.CacheItem = cache;
				IEnumerable<Picture> pictureList = cacheManager.GetPicturesOfCache(UserAuthentificationManager.CurrentUser.Name, UserAuthentificationManager.CurrentUser.Password, cache.Id);
				if (pictureList != null) {
					vm.PictureCollection = new ObservableCollection<Picture>(pictureList);
				}
				vm.CacheSaved += (s, eventArgs) => { Application.Current.Windows[1].Close(); };

				if (new GeoCaching.Desktop.View.GeoCache(vm).ShowDialog() != null) {
					pin.DataContext = cache;
					pin.Location = new Location(cache.Coordinates.Latitude, cache.Coordinates.Longitude);
					ToolTipService.SetToolTip(pin, GeoCachingHelper.GetToolTipText(cache));
					pushpinCollection.Remove(pin);
					pushpinCollection.Add(pin);
				}
			}
			else
				MessageBox.Show("Dieser Cache darf nur von seinem Besitzer geändert werden!");
			e.Handled = true;
		}
	}
}