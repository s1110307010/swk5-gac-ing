﻿namespace GeoCaching.Desktop.ViewModel {
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Server;
    using System.Windows.Input;
    using System.Net;
    using System.Runtime.Serialization;
    using System.Resources;
    using System.IO;
    using System.Drawing;
    using System.Windows;

    public class ExportViewModel : ViewModelBase<ExportViewModel> {
        #region Variables
        string username;
        string password;
        ICacheManager cacheManager;
        ICommand exportWordCommand;
        #endregion

        #region Constructors
        public ExportViewModel() {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());
            ObservableCollection<GeoCacheViewModel> geoCaches = new ObservableCollection<GeoCacheViewModel>();

            IEnumerable<Entities.Cache> caches = cacheManager.GetAllCaches(UserName, Password);
            if (caches != null)
                foreach (Entities.Cache cache in caches)
                    geoCaches.Add(new GeoCacheViewModel(geoCaches) { CacheItem = cache, IsSelected = false });

            this.GeoCacheCollection = geoCaches;
        }
        #endregion

        #region Commands
        public ICommand ExportWordCommand {
            get {
                if (exportWordCommand == null)
                    exportWordCommand = new RelayCommand((item) => {
                        // http://msdn.microsoft.com/en-us/library/ff701713.aspx
                        IExportManager exportManager = BLFactory.GetExportManager(DataAccessFactory.GetDataAccessType());
                        List<Entities.Cache> selectedCaches = new List<Entities.Cache>();
                        foreach (var cache in this.GeoCacheCollection) {
                            if (cache.IsSelected) {
                                selectedCaches.Add(cache.CacheItem);
                            }
                        }

                        if (selectedCaches.Count > 0) {
                            Entities.CacheList selectedCacheList = new Entities.CacheList() { Caches = selectedCaches };

                            byte[] data = exportManager.ExportCachesToWord(UserName, Password, selectedCacheList);
                            string tempFile = Directory.GetCurrentDirectory() + "GeoCaching.docx";

                            Microsoft.Office.Interop.Word.Application wordAppl = new Microsoft.Office.Interop.Word.Application();
                            try {
                                File.WriteAllBytes(tempFile, data);
                                wordAppl.Visible = true;
                                wordAppl.Documents.Open(tempFile);
                            }
                            catch (System.IO.IOException ex) {
                                MessageBox.Show("Die Datei konnte nicht geöffnet werden!");
                                wordAppl.Quit();
                            }
                        }
                        else
                            MessageBox.Show("Bitte wählen Sie mindestens einen GeoCache aus, den Sie exportieren wollen!");
                    });
                return exportWordCommand;
            }
        }
        #endregion

        #region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        public ObservableCollection<GeoCacheViewModel> GeoCacheCollection {
            get {
                return this.geoCacheCollection;
            }
            set {
                if (value != this.geoCacheCollection) {
                    this.geoCacheCollection = value;
                    RaisePropertyChangedEvent(vm => vm.GeoCacheCollection);
                }
            }
        }

        private ObservableCollection<GeoCacheViewModel> geoCacheCollection;
        #endregion
    }
}