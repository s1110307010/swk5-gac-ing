﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using PieControls;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows.Input;
	using System.Windows.Media;

	public class StatisticGraphicViewModel {
		#region Variables
        string username;
        string password;
        ICacheManager cacheManager;
		ICommand filterCommand;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="StatisticGraphicViewModel"/> class.
		/// </summary>
		public StatisticGraphicViewModel() {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());

			this.SearchLevelSegments = new ObservableCollection<PieSegment>();
			this.AreaLevelSegments = new ObservableCollection<PieSegment>();
			this.SizeSegments = new ObservableCollection<PieSegment>();
			this.TypeSegments = new ObservableCollection<PieSegment>();

			DateTime from = DateTime.Now.AddYears(-1);
			DateTime to = DateTime.Now;
			SetPieCharts(null, 0, from, to);

			this.SelectedFromDate = from;
			this.SelectedToDate = to;

			User currentUser = UserAuthentificationManager.CurrentUser;
			this.Latitude = currentUser.ResidenceCoordinates.Latitude;
			this.Longitude = currentUser.ResidenceCoordinates.Longitude;
            this.Radius = 0;
        }
		#endregion

		#region Commands
		/// <summary>
		/// Gets or sets the value for FilterCommand
		/// </summary>
		public ICommand FilterCommand {
			get {
				if (filterCommand == null) {
					filterCommand = new RelayCommand((item) => {
						DateTime from = SelectedFromDate ?? new DateTime(1753, 1, 1);
						DateTime to = SelectedToDate ?? DateTime.Now;
						SetPieCharts(new Coordinate() { Latitude = this.Latitude, Longitude = this.Longitude, SpatialReferenceId = 4326 }, this.Radius, from, to);
						if (this.UpdateUI != null)
							this.UpdateUI();
					});
				}

				return filterCommand;
			}
		}
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        /// <summary>
		/// Gets or sets the value for SearchLevelSegments
		/// </summary>
		public ObservableCollection<PieSegment> SearchLevelSegments { get; set; }

		/// <summary>
		/// Gets or sets the value for AreaLevelSegments
		/// </summary>
		public ObservableCollection<PieSegment> AreaLevelSegments { get; set; }

		/// <summary>
		/// Gets or sets the value for SizeSegments
		/// </summary>
		public ObservableCollection<PieSegment> SizeSegments { get; set; }

		/// <summary>
		/// Gets or sets the value for TypeSegments
		/// </summary>
		public ObservableCollection<PieSegment> TypeSegments { get; set; }

		/// <summary>
		/// Gets or sets the value for SelectedFromDate
		/// </summary>
		public DateTime? SelectedFromDate { get; set; }

		/// <summary>
		/// Gets or sets the value for SelectedToDate
		/// </summary>
		public DateTime? SelectedToDate { get; set; }

		/// <summary>
		/// Gets or sets the value for Latitude
		/// </summary>
		public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the value for Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets the value for Radius
        /// </summary>
        public double Radius { get; set; }
        #endregion

		#region Help Methods
		private void SetPieCharts(Coordinate region, double radius, DateTime from, DateTime to) {
			IList<PieSegment> searchLevels = new List<PieSegment>();
            searchLevels.Add(new PieSegment() { Color = Colors.Green, Name = "1", Value = cacheManager.GetSearchLevelPercentage(UserName, Password, region, radius, from, to, 1) });
            searchLevels.Add(new PieSegment() { Color = Colors.Black, Name = "2", Value = cacheManager.GetSearchLevelPercentage(UserName, Password, region, radius, from, to, 2) });
            searchLevels.Add(new PieSegment() { Color = Colors.Blue, Name = "3", Value = cacheManager.GetSearchLevelPercentage(UserName, Password, region, radius, from, to, 3) });
            searchLevels.Add(new PieSegment() { Color = Colors.Red, Name = "4", Value = cacheManager.GetSearchLevelPercentage(UserName, Password, region, radius, from, to, 4) });
            searchLevels.Add(new PieSegment() { Color = Colors.Orange, Name = "5", Value = cacheManager.GetSearchLevelPercentage(UserName, Password, region, radius, from, to, 5) });

			this.SearchLevelSegments.Clear();
			foreach (PieSegment segment in searchLevels)
				this.SearchLevelSegments.Add(segment);

			IList<PieSegment> areaLevels = new List<PieSegment>();
            areaLevels.Add(new PieSegment() { Color = Colors.Green, Name = "1", Value = cacheManager.GetAreaLevelPercentage(UserName, Password, region, radius, from, to, 1) });
            areaLevels.Add(new PieSegment() { Color = Colors.Black, Name = "2", Value = cacheManager.GetAreaLevelPercentage(UserName, Password, region, radius, from, to, 2) });
            areaLevels.Add(new PieSegment() { Color = Colors.Blue, Name = "3", Value = cacheManager.GetAreaLevelPercentage(UserName, Password, region, radius, from, to, 3) });
            areaLevels.Add(new PieSegment() { Color = Colors.Red, Name = "4", Value = cacheManager.GetAreaLevelPercentage(UserName, Password, region, radius, from, to, 4) });
            areaLevels.Add(new PieSegment() { Color = Colors.Orange, Name = "5", Value = cacheManager.GetAreaLevelPercentage(UserName, Password, region, radius, from, to, 5) });

			this.AreaLevelSegments.Clear();
			foreach (PieSegment segment in areaLevels)
				this.AreaLevelSegments.Add(segment);

			IList<PieSegment> sizes = new List<PieSegment>();
            sizes.Add(new PieSegment() { Color = Colors.Green, Name = CacheSize.Big.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.Big) });
            sizes.Add(new PieSegment() { Color = Colors.Black, Name = CacheSize.Mikro.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.Mikro) });
            sizes.Add(new PieSegment() { Color = Colors.Blue, Name = CacheSize.None.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.None) });
            sizes.Add(new PieSegment() { Color = Colors.Red, Name = CacheSize.Normal.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.Normal) });
            sizes.Add(new PieSegment() { Color = Colors.Orange, Name = CacheSize.Other.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.Other) });
            sizes.Add(new PieSegment() { Color = Colors.Yellow, Name = CacheSize.Small.ToString(), Value = cacheManager.GetSizePercentage(UserName, Password, region, radius, from, to, CacheSize.Small) });

			this.SizeSegments.Clear();
			foreach (PieSegment segment in sizes)
				this.SizeSegments.Add(segment);

			IList<PieSegment> types = new List<PieSegment>();
            types.Add(new PieSegment() { Color = Colors.Green, Name = CacheType.DriveIn.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.DriveIn) });
            types.Add(new PieSegment() { Color = Colors.Black, Name = CacheType.MathPhysics.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.MathPhysics) });
            types.Add(new PieSegment() { Color = Colors.Blue, Name = CacheType.Moving.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Moving) });
            types.Add(new PieSegment() { Color = Colors.Red, Name = CacheType.Multi.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Multi) });
            types.Add(new PieSegment() { Color = Colors.Orange, Name = CacheType.Other.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Other) });
            types.Add(new PieSegment() { Color = Colors.Yellow, Name = CacheType.Quiz.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Quiz) });
            types.Add(new PieSegment() { Color = Colors.Silver, Name = CacheType.Traditional.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Traditional) });
            types.Add(new PieSegment() { Color = Colors.Turquoise, Name = CacheType.Virtual.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Virtual) });
            types.Add(new PieSegment() { Color = Colors.Violet, Name = CacheType.Webcam.ToString(), Value = cacheManager.GetTypePercentage(UserName, Password, region, radius, from, to, CacheType.Webcam) });

			this.TypeSegments.Clear();
			foreach (PieSegment segment in types)
				this.TypeSegments.Add(segment);
		}
		#endregion

		#region Delegates
		public Action UpdateUI;
		#endregion
	}
}