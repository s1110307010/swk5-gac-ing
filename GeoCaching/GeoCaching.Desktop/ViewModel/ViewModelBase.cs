﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace GeoCaching.Desktop.ViewModel {
	public abstract class ViewModelBase<TViewModel> : INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChangedEvent<TProperty>(Expression<Func<TViewModel, TProperty>> propertySelector) {
			if (PropertyChanged != null) {
				var propertyExpression = propertySelector.Body as MemberExpression;
				var propertyName = propertyExpression.Member.Name;

				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}