﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows.Input;

	public class StatisticTableFoundCachesViewModel : ViewModelBase<StatisticTableFoundCachesViewModel> {
		#region Variables
        string username;
        string password;
        ICacheManager cacheManager;
		ICommand filterCommand;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="StatisticTableFoundCachesViewModel"/> class.
		/// </summary>
		public StatisticTableFoundCachesViewModel() {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());

			IUserManager userManager = BLFactory.GetUserManager(DataAccessFactory.GetDataAccessType());
            User user = userManager.GetUserById(UserName, Password, UserAuthentificationManager.CurrentUser.Id);
			this.Latitude = user.ResidenceCoordinates.Latitude;
			this.Longitude = user.ResidenceCoordinates.Longitude;

            this.FilteredGeoCaches = new ObservableCollection<StatisticUser>();

			#region FilterValues
			ObservableCollection<Data> searchLevels = new ObservableCollection<Data>();
			for (int i = 1; i <= 5; i++)
				searchLevels.Add(new Data() { Value = i.ToString() });
			this.SearchLevels = searchLevels;

			ObservableCollection<Data> areaLevels = new ObservableCollection<Data>();
			for (int i = 1; i <= 5; i++)
				areaLevels.Add(new Data() { Value = i.ToString() });
			this.AreaLevels = areaLevels;

			ObservableCollection<Data> sizes = new ObservableCollection<Data>();
			foreach (CacheSize size in Enum.GetValues(typeof(CacheSize)))
				sizes.Add(new Data() { Value = size.ToString() });
			this.Sizes = sizes;

			ObservableCollection<Data> types = new ObservableCollection<Data>();
			foreach (CacheType type in Enum.GetValues(typeof(CacheType)))
				types.Add(new Data() { Value = type.ToString() });
			this.Types = types;

			ObservableCollection<Data> ratings = new ObservableCollection<Data>();
			for (int i = 1; i <= 10; i++)
				ratings.Add(new Data() { Value = i.ToString() });
			this.Ratings = ratings;

			this.SelectedFromDate = DateTime.Today.AddYears(-1);
			this.SelectedToDate = DateTime.Today;
			#endregion
        }
		#endregion

		#region Commands
		/// <summary>
		/// Gets or sets the value for FilterCommand
		/// </summary>
		public ICommand FilterCommand {
			get {
				if (filterCommand == null) {
					filterCommand = new RelayCommand((item) => {
                        IEnumerable<Cache> filteredCaches = cacheManager.FilterCaches(UserName, Password, 
                            this.SelectedFromDate ?? new DateTime(1753, 1, 1),
                            this.SelectedToDate ?? DateTime.Today,
                            new CacheTypeList() { CacheTypes = this.Types.Where(t => t.IsSelected).Select(t => (CacheType)Enum.Parse(typeof(CacheType), t.Value)).ToList() },
                            new CacheSizeList() { CacheSizes = this.Sizes.Where(t => t.IsSelected).Select(t => (CacheSize)Enum.Parse(typeof(CacheSize), t.Value)).ToList() },
                            new AreaLevelList() { AreaLevels = this.AreaLevels.Where(t => t.IsSelected).Select(t => Convert.ToInt32(t.Value)).ToList() },
                            new SearchLevelList() { SearchLevels = this.SearchLevels.Where(t => t.IsSelected).Select(t => Convert.ToInt32(t.Value)).ToList() },
                            new Coordinate() { Latitude = this.Latitude, Longitude = this.Longitude, SpatialReferenceId = 4326 },
                            this.Radius,
                            new RatingList() { Ratings = this.Ratings.Where(t => t.IsSelected).Select(t => Convert.ToInt32(t.Value)).ToList() }, null);

                        IEnumerable<User> userList = BLFactory.GetUserManager(DataAccessFactory.GetDataAccessType()).GetAllUsers(UserName, Password);
						IList<StatisticUser> result = new List<StatisticUser>(); 
                        foreach (User user in userList) {
							result.Add(new StatisticUser() { UserItem = user, Count = BLFactory.GetLogbookManager(DataAccessFactory.GetDataAccessType()).GetCountOfCachesByState(UserName, Password, user.Id, new CacheList() { Caches = filteredCaches.ToList() }, SearchState.Found) });    
                        }
                        result = result.OrderByDescending(c => c.Count).ToList();
                        
						this.FilteredGeoCaches.Clear();
						foreach (StatisticUser geoCacheItem in result)
							this.FilteredGeoCaches.Add(geoCacheItem);
					});

				}

				return filterCommand;
			}
		}
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        #region Filtered GeoCaches
		/// <summary>
		/// Gets or sets the value for FilteredGeoCaches
		/// </summary>
		public ObservableCollection<StatisticUser> FilteredGeoCaches {
			get {
				return this.filteredGeoCaches;
			}
			set {
				if (value != this.filteredGeoCaches) {
					this.filteredGeoCaches = value;
					RaisePropertyChangedEvent(vm => vm.FilteredGeoCaches);
				}
			}
		}

		/// <summary>
		/// The local value field for FilteredGeoCaches
		/// </summary>
		private ObservableCollection<StatisticUser> filteredGeoCaches;
		#endregion

		#region SearchLevels
		/// <summary>
		/// Gets or sets the value for SearchLevels
		/// </summary>
		public ObservableCollection<Data> SearchLevels {
			get {
				return this.searchLevels;
			}
			set {
				if (value != this.searchLevels) {
					this.searchLevels = value;
					RaisePropertyChangedEvent(vm => vm.SearchLevels);
				}
			}
		}

		/// <summary>
		/// The local value field for SearchLevels
		/// </summary>
		private ObservableCollection<Data> searchLevels;
		#endregion

		#region AreaLevels
		/// <summary>
		/// Gets or sets the value for AreaLevels
		/// </summary>
		public ObservableCollection<Data> AreaLevels {
			get {
				return this.areaLevels;
			}
			set {
				if (value != this.areaLevels) {
					this.areaLevels = value;
					RaisePropertyChangedEvent(vm => vm.AreaLevels);
				}
			}
		}

		/// <summary>
		/// The local value field for AreaLevels
		/// </summary>
		private ObservableCollection<Data> areaLevels;
		#endregion

		#region Sizes
		/// <summary>
		/// Gets or sets the value for Sizes
		/// </summary>
		public ObservableCollection<Data> Sizes {
			get {
				return this.sizes;
			}
			set {
				if (value != this.sizes) {
					this.sizes = value;
					RaisePropertyChangedEvent(vm => vm.Sizes);
				}
			}
		}

		/// <summary>
		/// The local value field for Sizes
		/// </summary>
		private ObservableCollection<Data> sizes;
		#endregion

		#region Types
		/// <summary>
		/// Gets or sets the value for Types
		/// </summary>
		public ObservableCollection<Data> Types {
			get {
				return this.types;
			}
			set {
				if (value != this.types) {
					this.types = value;
					RaisePropertyChangedEvent(vm => vm.Types);
				}
			}
		}

		/// <summary>
		/// The local value field for Types
		/// </summary>
		private ObservableCollection<Data> types;
		#endregion

		#region Ratings
		/// <summary>
		/// Gets or sets the value for Ratings
		/// </summary>
		public ObservableCollection<Data> Ratings {
			get {
				return this.ratings;
			}
			set {
				if (value != this.ratings) {
					this.ratings = value;
					RaisePropertyChangedEvent(vm => vm.Ratings);
				}
			}
		}

		/// <summary>
		/// The local value field for Ratings
		/// </summary>
		private ObservableCollection<Data> ratings;
		#endregion

		#region SelectedFromDate
		/// <summary>
		/// Gets or sets the value for SelectedFromDate
		/// </summary>
		public DateTime? SelectedFromDate { get; set; }
		#endregion

		#region SelectedToDate
		/// <summary>
		/// Gets or sets the value for SelectedToDate
		/// </summary>
		public DateTime? SelectedToDate { get; set; }
		#endregion

		#region Latitude
		/// <summary>
		/// Gets or sets the value for Latitude
		/// </summary>
		public double Latitude { get; set; }
		#endregion

		#region Longitude
		/// <summary>
		/// Gets or sets the value for Longitude
		/// </summary>
		public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets the value for Radius
        /// </summary>
        public double Radius { get; set; }
		#endregion
		#endregion
	}
}