﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using GeoCaching.Desktop.ViewModel;
using GeoCaching.Entities;
using GeoCaching.Server;

namespace GeoCaching.Desktop.View {
    public class UserAdministrationViewModel : ViewModelBase<UserAdministrationViewModel> {
        #region Variables
        string username;
        string password;
        IUserManager userManager;
        IRoleManager roleManager;
        #endregion

        #region Constructors
        public UserAdministrationViewModel() {
            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;
            userManager = BLFactory.GetUserManager(DataAccessFactory.GetDataAccessType());
            roleManager = BLFactory.GetRoleManager(DataAccessFactory.GetDataAccessType());



            AbstractUserManager abUserManager = BLFactory.GetUserManager();
            //IEnumerable<GeoCaching.Entities.User> users = userManager.GetAllUsers(UserName, Password);
            IEnumerable<Entities.User> users = abUserManager.GetAllUsers(UserName, Password);
            if (users != null) {
                ObservableCollection<UserViewModel> allUsers = new ObservableCollection<UserViewModel>();
                foreach (Entities.User user in users) {
                    allUsers.Add(new UserViewModel(allUsers) {
                        UserItem = user,
                        IsUserHider = roleManager.IsUserHider(UserName, Password, user.Id),
                        IsUserFinder = roleManager.IsUserFinder(UserName, Password, user.Id)
                    });
                }
                this.UserCollection = allUsers;
            }
        }
        #endregion

        #region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        /// <summary>
        /// Gets or sets the value for GeoCacheCollection
        /// </summary>
        public ObservableCollection<UserViewModel> UserCollection {
            get {
                return this.userCollection;
            }
            set {
                if (value != this.userCollection) {
                    this.userCollection = value;
                    RaisePropertyChangedEvent(vm => vm.UserCollection);
                }
            }
        }

        /// <summary>
        /// The local value field for UserCollection
        /// </summary>
        private ObservableCollection<UserViewModel> userCollection;
        #endregion
    }
}
