﻿namespace GeoCaching.Desktop.ViewModel {
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;

	public class GeoCacheAdministrationViewModel : ViewModelBase<GeoCacheAdministrationViewModel> {
		#region Variables
        string username;
        string password;
        ICacheManager cacheManager;
		#endregion


		#region Constructors
		public GeoCacheAdministrationViewModel() {
			cacheManager = BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType());
			ObservableCollection<GeoCacheViewModel> geoCaches = new ObservableCollection<GeoCacheViewModel>();
			ObservableCollection<Picture> pictures = null;

            this.username = UserAuthentificationManager.CurrentUser.Name;
            this.password = UserAuthentificationManager.CurrentUser.Password;

			IEnumerable<Cache> caches = cacheManager.GetAllCaches(UserName, Password).Where(cache => cache.OwnerId == UserAuthentificationManager.CurrentUser.Id);
			if (caches != null)
				foreach (Cache cache in caches) {
                    IEnumerable<Picture> picturesOfCache = cacheManager.GetPicturesOfCache(UserName, Password, cache.Id);
					pictures = new ObservableCollection<Picture>();
					if (picturesOfCache != null) {
						foreach (Picture picture in picturesOfCache) {
							pictures.Add(picture);
						}
					}

					GeoCacheViewModel vm = new GeoCacheViewModel(geoCaches) { CacheItem = cache, PictureCollection = pictures };
					vm.CacheSaved += vm_CacheSaved;

					geoCaches.Add(vm);

				}

			this.GeoCacheCollection = geoCaches;
		}
		#endregion

		#region Properties

        #region Username
        public string UserName {
            get { return username; }
        }

        #endregion

        #region Password
        public string Password {
            get { return password; }
        }

        #endregion

        public ObservableCollection<GeoCacheViewModel> GeoCacheCollection {
			get {
				return this.geoCacheCollection;
			}
			set {
				if (value != this.geoCacheCollection) {
					this.geoCacheCollection = value;
					RaisePropertyChangedEvent(vm => vm.GeoCacheCollection);
				}
			}
		}

		private ObservableCollection<GeoCacheViewModel> geoCacheCollection;
		#endregion

		#region Private Methods
		void vm_CacheSaved(object sender, EventArgs e) {
			Application.Current.Windows[1].Close();
		}
		#endregion
	}
}