﻿namespace GeoCaching.Desktop {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using GeoCaching.Entities;

	public static class UserAuthentificationManager {
		/// <summary>
		/// Gets or sets the value for CurrentUser
		/// </summary>
		public static User CurrentUser { get; set; }
	}
}
