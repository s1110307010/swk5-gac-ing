﻿namespace GeoCaching.Desktop {
	using GeoCaching.Server;
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public static class DataAccessFactory {
		public static DataAccessType GetDataAccessType() {
			string dataAccessType = ConfigurationManager.AppSettings["DataAccessType"];

			DataAccessType type;
			if (Enum.TryParse(dataAccessType, out type))
				return type;
			else
				throw new NotSupportedException("Configured DataAccessType is not supported!");
		}
	}
}