﻿using GeoCaching.Desktop.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeoCaching.Desktop.View {
    public partial class UserAdministration : UserControl {
        #region Constructors
        public UserAdministration() {
            InitializeComponent();
            this.DataContext = new UserAdministrationViewModel();
            this.DataContextChanged += UserAdministration_DataContextChanged;
        }

        void btnAdd_Click(object sender, RoutedEventArgs e) {
			new User((this.DataContext as UserAdministrationViewModel).UserCollection).ShowDialog();
        }
        #endregion

        #region Private Event Handler
        /// <summary>
        /// The data context of the user control has changed
        /// </summary>
        /// <param name="sender">the event's sender object</param>
        /// <param name="e">some event arguments for the change</param>
        void UserAdministration_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e) {
			new GeoCaching.Desktop.View.User(((sender as FrameworkElement).DataContext as UserViewModel)).ShowDialog();
        }
        #endregion
	}
}
