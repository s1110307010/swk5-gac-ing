﻿namespace GeoCaching.Desktop.View {
	using GeoCaching.Desktop.ViewModel;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Data;
	using System.Windows.Documents;
	using System.Windows.Input;
	using System.Windows.Media;
	using System.Windows.Media.Imaging;
	using System.Windows.Shapes;

	public partial class Main : Window {
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Main"/> class.
		/// </summary>
		public Main() {
			InitializeComponent();
			// the data context of the user control has changed
			this.DataContextChanged += Main_DataContextChanged;
			UserControl ctnMain = new GeoCacheAdministration();
			ctnMain.Name = "ctnMain";
			Grid.SetRow(ctnMain, 1);

			myGrid.Children.Add(ctnMain);
		}
		#endregion

		#region Private Methods
		private void MenuChanged(UserControl ctnMain) {
			if (ctnMain != null) {
				myGrid.Children.RemoveAt(1);

				ctnMain.Name = "ctnMain";
				Grid.SetRow(ctnMain, 1);

				myGrid.Children.Add(ctnMain);
			}
		}
		#endregion

		#region Private Event Handler
		/// <summary>
		/// The data context of the window has changed
		/// </summary>
		/// <param name="sender">the event's sender object</param>
		/// <param name="e">some event arguments for the change</param>
		private void Main_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
		#endregion

		#region Menu Event Handlers
		private void CloseWindow_Click(object sender, RoutedEventArgs e) {
			Application.Current.Windows[0].Close();
		}

		private void GeoCacheAdministration_Click(object sender, RoutedEventArgs e) {
			MenuChanged(new GeoCacheAdministration());
		}

		private void GeoCacheMap_Click(object sender, RoutedEventArgs e) {
			MenuChanged(new GeoCacheMap());
		}

		private void UserAdministration_Click(object sender, RoutedEventArgs e) {
			MenuChanged(new UserAdministration());
		}

        private void StatisticTableFoundCaches_Click(object sender, RoutedEventArgs e) {
            MenuChanged(new StatisticTableFoundCaches());
        }

        private void StatisticTableHidedCaches_Click(object sender, RoutedEventArgs e) {
            MenuChanged(new StatisticTableHidedCaches());
        }

        private void StatisticTableFoundCachesByCache_Click(object sender, RoutedEventArgs e) {
            MenuChanged(new StatisticTableFoundListCaches());
        }

        private void StatisticGrafic_Click(object sender, RoutedEventArgs e) {
			MenuChanged(new StatisticGraphic());
		}

		private void Export_Click(object sender, RoutedEventArgs e) {
			MenuChanged(new Export());
		}
		#endregion
	}
}