﻿namespace GeoCaching.Desktop {
    using GeoCaching.Desktop.View;
    using GeoCaching.Server;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

	public partial class Login : Window {
		#region Constructors
		public Login() {
			InitializeComponent();
			this.DataContextChanged += Login_DataContextChanged;
            this.tbUsername.Focus();
            this.tbUsername.Text = "Andreas Etzelstorfer";
            this.tbPassword.Password = "swk5";
		}
		#endregion

		#region Login
		void btnLogin_Click(object sender, RoutedEventArgs e) {
			IAuthentificationManager authentificationManager = BLFactory.GetAuthenticationManager(DataAccessFactory.GetDataAccessType());
			IRoleManager roleManager = BLFactory.GetRoleManager(DataAccessFactory.GetDataAccessType());
			GeoCaching.Entities.User user = authentificationManager.ValidateUser(tbUsername.Text, tbPassword.Password);
			if (user != null && roleManager.IsUserHider(this.tbUsername.Text, this.tbPassword.Password, user.Id)) {
				IUserManager userManager = BLFactory.GetUserManager(DataAccessFactory.GetDataAccessType());
				UserAuthentificationManager.CurrentUser = userManager.GetUserByName(this.tbUsername.Text, this.tbPassword.Password, tbUsername.Text);
				Application.Current.MainWindow = new Main();
				Application.Current.MainWindow.Show();
				this.Close();
			}
			else
				txtErrorMessage.Visibility = Visibility.Visible;
		}
        #endregion

        #region Private Event Handler
        /// <summary>
		/// The data context of the window has changed
		/// </summary>
		/// <param name="sender">the event's sender object</param>
		/// <param name="e">some event arguments for the change</param>
		private void Login_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
		#endregion
	}
}