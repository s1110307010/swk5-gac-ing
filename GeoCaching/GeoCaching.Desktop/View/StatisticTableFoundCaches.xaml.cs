﻿using GeoCaching.Desktop.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeoCaching.Desktop.View {
    public partial class StatisticTableFoundCaches : UserControl {
        public StatisticTableFoundCaches() {
            InitializeComponent();
			this.DataContext = new StatisticTableFoundCachesViewModel();
			SetDisplayDates();
        }

		private void SetDisplayDates() {
			DateTime start = new DateTime(1753, 1, 1);
			dpFrom.DisplayDateStart = start;
			dpTo.DisplayDateStart = start;
		}
    }
}
