﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PieControls;
using GeoCaching.Desktop.ViewModel;

namespace GeoCaching.Desktop.View {
	public partial class StatisticGraphic : UserControl {
		public StatisticGraphic() {
			InitializeComponent();

			StatisticGraphicViewModel vm = new StatisticGraphicViewModel();
			vm.UpdateUI = UpdateUI;
			this.DataContext = vm;

			UpdateUI();
		}

		private void UpdateUI() {
			StatisticGraphicViewModel vm = (this.DataContext as StatisticGraphicViewModel);

			pieSearchLevel.Visibility = vm.SearchLevelSegments.All(seg => seg.Value == 0) ? Visibility.Collapsed : System.Windows.Visibility.Visible;
			txtNoSearchLevelDataAvailable.Visibility = vm.SearchLevelSegments.All(seg => seg.Value == 0) ? Visibility.Visible : System.Windows.Visibility.Collapsed;
			pieSearchLevel.Data = vm.SearchLevelSegments;

			pieAreaLevel.Visibility = vm.AreaLevelSegments.All(seg => seg.Value == 0) ? Visibility.Collapsed : System.Windows.Visibility.Visible;
			txtNoAreaLevelDataAvailable.Visibility = vm.AreaLevelSegments.All(seg => seg.Value == 0) ? Visibility.Visible : System.Windows.Visibility.Collapsed;
			pieAreaLevel.Data = vm.AreaLevelSegments;

			pieSize.Visibility = vm.SizeSegments.All(seg => seg.Value == 0) ? Visibility.Collapsed : System.Windows.Visibility.Visible;
			txtNoSizeDataAvailable.Visibility = vm.SizeSegments.All(seg => seg.Value == 0) ? Visibility.Visible : System.Windows.Visibility.Collapsed;
			pieSize.Data = vm.SizeSegments;

			pieType.Visibility = vm.TypeSegments.All(seg => seg.Value == 0) ? Visibility.Collapsed : System.Windows.Visibility.Visible;
			txtNoTypeDataAvailable.Visibility = vm.TypeSegments.All(seg => seg.Value == 0) ? Visibility.Visible : System.Windows.Visibility.Collapsed;
			pieType.Data = vm.TypeSegments;
		}
	}
}