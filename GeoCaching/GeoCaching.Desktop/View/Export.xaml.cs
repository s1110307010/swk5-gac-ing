﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeoCaching.Desktop.ViewModel;

namespace GeoCaching.Desktop.View {
    public partial class Export : UserControl {
        public Export() {
            InitializeComponent();
            this.DataContext = new ExportViewModel();
            this.DataContextChanged += Export_DataContextChanged;
        }

        private void Export_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
        }

        private void btnExportWord_Click(object sender, RoutedEventArgs e) {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Caching"; // Default file name
            dlg.DefaultExt = ".dox"; // Default file extension
            dlg.Filter = "Word documents (.docx)|*.docx"; // Filter files by extension 

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true) {
                // Save document document here 
                string filename = dlg.FileName;
            }
        }
    }
}