﻿namespace GeoCaching.Desktop.View {
	using GeoCaching.Desktop.ViewModel;
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using Microsoft.Maps.MapControl.WPF;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Diagnostics;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Data;
	using System.Windows.Documents;
	using System.Windows.Input;
	using System.Windows.Media;
	using System.Windows.Media.Imaging;
	using System.Windows.Navigation;
	using System.Windows.Shapes;

	public partial class GeoCacheMap : UserControl {
		#region Constructors
		public GeoCacheMap() {
			InitializeComponent();
			this.DataContext = new GeoCacheMapViewModel();

			this.CacheMap.Center = new Location(0, 0);
			this.CacheMap.ZoomLevel = 3;
		}
		#endregion

		#region Private Event Handler
		/// <summary>
		/// The data context of the geo cache map control has changed
		/// </summary>
		/// <param name="sender">the event's sender object</param>
		/// <param name="e">some event arguments for the change</param>
		private void GeoCacheMap_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
		#endregion

		private void CacheMap_MouseDoubleClick_1(object sender, MouseButtonEventArgs e) {
			if (e.ButtonState == MouseButtonState.Pressed) {
				Point mousePosition = e.GetPosition(CacheMap);
				Location pinLocation = CacheMap.ViewportPointToLocation(mousePosition);

				Cache cache = new Cache() { Coordinates = new Coordinate() { Latitude = pinLocation.Latitude, Longitude = pinLocation.Longitude } };
				GeoCacheViewModel vm = new GeoCacheViewModel(null) { CacheItem = cache, IsNewGeoCache = true };
				vm.CacheSaved += vm_CacheSaved;
				new GeoCaching.Desktop.View.GeoCache(vm).ShowDialog();
            }
			e.Handled = true;
		}

		void vm_CacheSaved(object sender, EventArgs e) {
			GeoCacheViewModel vm = (sender as GeoCacheViewModel);
			Coordinate coordinates = vm.CacheItem.Coordinates;
			Pushpin pin = new Pushpin();
			pin.Location = new Location(coordinates.Latitude, coordinates.Longitude);
			pin.DataContext = vm.CacheItem;

			pin.MouseRightButtonDown += pin_MouseRightButtonDown;
			pin.MouseDoubleClick += pin_MouseDoubleClick;

			ToolTipService.SetToolTip(pin, GeoCachingHelper.GetToolTipText(vm.CacheItem));
			CacheMap.Children.Add(pin);

			Application.Current.Windows[1].Close();
		}

		void pin_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {
			Cache cache = ((sender as Pushpin).DataContext as Cache);
			MessageBox.Show(GeoCachingHelper.GetToolTipText(cache), cache.Name);
			e.Handled = true;
		}

		void pin_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            GeoCachingHelper.PinDoubleClick(sender, e, BLFactory.GetCacheManager(DataAccessFactory.GetDataAccessType()), (this.DataContext as GeoCacheMapViewModel).PushpinCollection);	
		}
	}
}