﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using GeoCaching.Desktop.ViewModel;
using GeoCaching.Entities;
using System.Collections.ObjectModel;

namespace GeoCaching.Desktop.View {
	public partial class User : Window {

		public User(ObservableCollection<UserViewModel> allUsers) {
			InitializeComponent();
			this.DataContext = new UserViewModel(allUsers) { IsNewUser = true };
		}

		public User(UserViewModel userViewModel) {
			userViewModel.IsNewUser = false;
			InitializeComponent();
			this.DataContext = userViewModel;

			Location location = new Location(userViewModel.UserItem.ResidenceCoordinates.Latitude, userViewModel.UserItem.ResidenceCoordinates.Longitude);
            UserMap.Center = location;
			UserMap.ZoomLevel = 20.0;

            Pushpin pin = new Pushpin();
            pin.Location = location;

            UserMap.Children.Add(pin);

			this.DataContextChanged += UserAdministration_DataContextChanged;
		}

		private void UserAdministration_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
	}
}
