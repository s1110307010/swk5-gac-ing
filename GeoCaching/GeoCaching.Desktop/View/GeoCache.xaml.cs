﻿namespace GeoCaching.Desktop.View {
	using GeoCaching.Desktop.ViewModel;
	using GeoCaching.Entities;
	using GeoCaching.Server;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Data;
	using System.Windows.Documents;
	using System.Windows.Input;
	using System.Windows.Media;
	using System.Windows.Media.Imaging;
	using System.Windows.Shapes;

	public partial class GeoCache : Window {
		#region Constructors
		public GeoCache(ObservableCollection<GeoCacheViewModel> allGeoCaches) {
			InitializeComponent();
			this.DataContextChanged += GeoCache_DataContextChanged;
			GeoCacheViewModel vm = new GeoCacheViewModel(allGeoCaches);
			vm.IsNewGeoCache = true;
			vm.CacheSaved += vm_CacheSaved;
			this.DataContext = vm;
		}

		public GeoCache(GeoCacheViewModel geoCacheViewModel) {
			InitializeComponent();
			this.DataContextChanged += GeoCache_DataContextChanged;
			this.DataContext = geoCacheViewModel;
		}
		#endregion

		#region Private Event Handler
		void vm_CacheSaved(object sender, EventArgs e) {
			Application.Current.Windows[1].Close();
		}

		/// <summary>
		/// The data context of the window has changed
		/// </summary>
		/// <param name="sender">the event's sender object</param>
		/// <param name="e">some event arguments for the change</param>
		private void GeoCache_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
		#endregion
	}
}