﻿namespace GeoCaching.Desktop.View {
	using GeoCaching.Desktop.ViewModel;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Data;
	using System.Windows.Documents;
	using System.Windows.Input;
	using System.Windows.Media;
	using System.Windows.Media.Imaging;
	using System.Windows.Navigation;
	using System.Windows.Shapes;

	public partial class GeoCacheAdministration : UserControl {
		#region Constructors
		public GeoCacheAdministration() {
			InitializeComponent();
			this.DataContext = new GeoCacheAdministrationViewModel();
			this.DataContextChanged += GeoCacheAdministration_DataContextChanged;
		}
		#endregion

		#region Add Geo Cache
		void btnAdd_Click(object sender, RoutedEventArgs e) {
			new GeoCache((this.DataContext as GeoCacheAdministrationViewModel).GeoCacheCollection).ShowDialog();
		}
		#endregion

		#region Edit Geo Cache
		private void btnEdit_Click(object sender, RoutedEventArgs e) {
			new GeoCaching.Desktop.View.GeoCache(((sender as FrameworkElement).DataContext as GeoCacheViewModel)).ShowDialog();
		}
		#endregion

		#region Private Event Handler
		/// <summary>
		/// The data context of the user control has changed
		/// </summary>
		/// <param name="sender">the event's sender object</param>
		/// <param name="e">some event arguments for the change</param>
		private void GeoCacheAdministration_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
		}
		#endregion
	}
}