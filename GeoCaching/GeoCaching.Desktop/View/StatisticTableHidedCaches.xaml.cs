﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeoCaching.Desktop.ViewModel;

namespace GeoCaching.Desktop.View {
    public partial class StatisticTableHidedCaches : UserControl {
        public StatisticTableHidedCaches() {
            InitializeComponent();
            this.DataContext = new StatisticTableHidedCachesViewModel();
            SetDisplayDates();
        }

        private void SetDisplayDates() {
            DateTime start = new DateTime(1753, 1, 1);
            dpFrom.DisplayDateStart = start;
            dpTo.DisplayDateStart = start;
        }
    }

}
