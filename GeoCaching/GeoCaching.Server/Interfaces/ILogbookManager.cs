﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;

    public interface ILogbookManager {
		/// <summary>
		/// Gets the log book with the specified id.
		/// </summary>
		/// <param name="id">The id to get the log book entry for.</param>
		/// <returns>Returns the loog book entry with the specified id.</returns>
        LogBook GetLogBook(string userName, string password, int id);
        
		/// <summary>
		/// Gets all log book entries of the specified cache.
		/// </summary>
		/// <param name="cacheId">The cache id to get all log book entries for.</param>
		/// <returns>Returns all log book entries according to the specified cache.</returns>
        IEnumerable<LogBook> GetAllLogBookEntriesOfCache(string userName, string password, int cacheId);

        /// <summary>
        /// Get count of Caches of an user by state
        /// </summary>
        /// <param name="userId">The user to get count of entries</param>
        /// <param name="cacheList">The list of selected caches</param>
        /// <param name="state">The state of the searched logbook entries</param>
        /// <returns></returns>
        int GetCountOfCachesByState(string userName, string password, int userId, CacheList cacheList, SearchState state);
        
		/// <summary>
		/// Gets all log book entries.
		/// </summary>
		/// <returns>Returns all log book entires.</returns>
		IEnumerable<LogBook> GetAllLogBooks(string userName, string password);
        
		/// <summary>
		/// Creates the given log book entry.
		/// </summary>
		/// <param name="logBook">The log book entry to create.</param>
		/// <returns>Returns true if the creation process was successful.</returns>
        bool CreateLogBook(string userName, string password, LogBook logBook);
        
		/// <summary>
		/// Deletes the log book entry with the given id.
		/// </summary>
		/// <param name="id">The id of the log book entry to delete.</param>
		/// <returns>Returns true if the deletion process was successful.</returns>
        bool DeleteLogBook(string userName, string password, int id);
    }
}