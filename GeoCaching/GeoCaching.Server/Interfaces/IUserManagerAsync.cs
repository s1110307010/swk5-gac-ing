﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;

    public interface IUserManagerAsync {
		/// <summary>
		/// Gets the user by the specified id.
		/// </summary>
		/// <param name="id">The id to get the user for.</param>
		/// <returns>Returns the user with the specified id.</returns>
        Task<User> GetUserByIdAsync(string userName, string password, int id);

		/// <summary>
		/// Gets the user specified by the given username.
		/// </summary>
		/// <param name="userName">The username of the user to load.</param>
		/// <returns>Returns the user object if username exists, otherwise null.</returns>
        Task<User> GetUserByNameAsync(string userName, string password, string userNameToGet);
        
		/// <summary>
		/// Gets all users.
		/// </summary>
		/// <returns>Returns all users.</returns>
        Task<IEnumerable<User>> GetAllUsersAsync(string userName, string password);
		
		/// <summary>
		/// Creates the given user.
		/// </summary>
		/// <param name="user">The user to create.</param>
		/// <returns>Returns true if the creation process was successful.</returns>
        Task<bool> CreateAsync(string userName, string password, User user);
        
		/// <summary>
		/// Updates the given user.
		/// </summary>
		/// <param name="user">The user to update.</param>
		/// <returns>Returns true if the update process was successful.</returns>
        Task<bool> UpdateAsync(string userName, string password, User user);
		
		/// <summary>
		/// Deletes the user with the specified id.
		/// </summary>
		/// <param name="id">The id of the user to delete.</param>
		/// <returns>Returns true if the deletion process was successful.</returns>
        Task<bool> DeleteAsync(string userName, string password, int id);

		/// <summary>
		/// Gets whether a user with the given username exists already.
		/// </summary>
		/// <param name="username">The username to check for.</param>
		/// <returns>Returns true, if a user with given username exists already.</returns>
        Task<bool> ExistsUserAsync(string userName, string password, string userNameToGet);

		/// <summary>
		/// Gets whether a given email address is used for a given user.
		/// </summary>
		/// <param name="email">The email address to check.</param>
		/// <param name="userId">The user to check the email address for.</param>
		/// <returns>Returns true if the email address is used for the given user, false otherwise.</returns>
        Task<bool> IsEmailAdressInUseAsync(string userName, string password, string email, int userId);

		/// <summary>
		/// Reactivate a deleted user with the given id.
		/// </summary>
		/// <param name="id">The id of the entity to reactivate.</param>
		/// <returns>Returns whether the entity was reactivated successfully.</returns>
        Task<bool> ReactivateAsync(string userName, string password, int id);
    }
}
