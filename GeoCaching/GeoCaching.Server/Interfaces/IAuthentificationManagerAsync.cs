﻿namespace GeoCaching.Server {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IAuthentificationManagerAsync {
		/// <summary>
		/// Validates whether the username and password match with existing user.
		/// </summary>
		/// <param name="userName">The username to validate.</param>
		/// <param name="password">The password to validate.</param>
		/// <returns>Returns the user if validation is correct, null otherwise.</returns>
		Task<User> ValidateUserAsync(string userName, string password);
	}
}