﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;

    public interface IRoleManagerAsync {
        #region GetAllRoles
		/// <summary>
		/// Gets all existing roles.
		/// </summary>
		/// <returns>Returns all roles.</returns>
        Task<IEnumerable<Role>> GetAllRolesAsync(string userName, string password);
        #endregion

        #region GetRoleById
		/// <summary>
		/// Gets a role by its id.
		/// </summary>
		/// <param name="id">The id of the role to get.</param>
		/// <returns>Returns the role specified by the id.</returns>
        Task<Role> GetRoleByIdAsync(string userName, string password, int id);
        #endregion

        #region GetRoleByName
		/// <summary>
		/// Gets a role by its name.
		/// </summary>
		/// <param name="name">The name of the role to get.</param>
		/// <returns>Returns the role specified by the name.</returns>
        Task<Role> GetRoleByNameAsync(string userName, string password, string name);
        #endregion

        #region AssignRolesToUser
		/// <summary>
		/// Assignes the given roles to the given user.
		/// </summary>
		/// <param name="userId">The user to assign the roles to.</param>
		/// <param name="roles">The roles to assign to the user.</param>
        void AssignRolesToUserAsync(string userName, string password, int userId, Entities.RoleList roles);
        #endregion

        #region RemoveRolesFromUser
		/// <summary>
		/// Removes the given roles from the specified user.
		/// </summary>
		/// <param name="userId">The user to remove the roles from.</param>
		/// <param name="roles">The roles to remove.</param>
        void RemoveRolesFromUserAsync(string userName, string password, int userId, Entities.RoleList roles);
        #endregion

        #region GetRolesOfUser
		/// <summary>
		/// Gets all roles which are assigned to the specified user.
		/// </summary>
		/// <param name="userId">The user to get the roles for.</param>
		/// <returns>Returns all roles of the user.</returns>
        Task<IEnumerable<Role>> GetRolesOfUserAsync(string userName, string password, int userId);
        #endregion

        #region IsUserHider
		/// <summary>
		/// Gets whether the user is a hider.
		/// </summary>
		/// <param name="userId">The user to check the role for.</param>
		/// <returns>Returns true if the specified user has the role hider.</returns>
        Task<bool> IsUserHiderAsync(string userName, string password, int userId);
        #endregion

        #region IsUserFinder
		/// <summary>
		/// Gets whether the user is a finder.
		/// </summary>
		/// <param name="userId">The user to check the role for.</param>
		/// <returns>Returns true if the specified user has the role finder.</returns>
        Task<bool> IsUserFinderAsync(string userName, string password, int userId);
        #endregion
    }
}
