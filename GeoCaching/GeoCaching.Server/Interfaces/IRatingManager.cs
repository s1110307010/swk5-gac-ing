﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;

    public interface IRatingManager {
		/// <summary>
		/// Gets the rating with the specified id.
		/// </summary>
		/// <param name="id">The id of the rating to get.</param>
		/// <returns>Returns the rating with the specified id.</returns>
        Rating GetRating(string userName, string password, int id);

		/// <summary>
		/// Gets all ratings assigned to the specified cache.
		/// </summary>
		/// <param name="cacheId">The cache to get the ratings for.</param>
		/// <returns>Returns all ratings of the specified cache.</returns>
        IEnumerable<Rating> GetAllRatingsOfCache(string userName, string password, int cacheId);

		/// <summary>
		/// Gets all ratings.
		/// </summary>
		/// <returns>Returns all ratings.</returns>
        IEnumerable<Rating> GetAllRatings(string userName, string password);
        
		/// <summary>
		/// Creates the given rating.
		/// </summary>
		/// <param name="rating">The rating to create.</param>
		/// <returns>Returns true if the creation process was successful.</returns>
        bool CreateRating(string userName, string password, Rating rating);

		/// <summary>
		/// Deletes the specified rating.
		/// </summary>
		/// <param name="id">The id of the rating to delete.</param>
		/// <returns>Returns true if the deletion process was successful.</returns>
        bool DeleteRating(string userName, string password, int id);
    }
}
