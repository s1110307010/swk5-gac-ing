﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IExportManagerAsync {
        #region Export Caches to Word-Document
		/// <summary>
		/// Exports the given caches into a word document.
		/// </summary>
		/// <param name="caches">The caches to export.</param>
		/// <returns>Returns true if the caches were exported successful.</returns>
        Task<byte[]> ExportCachesToWordAsync(string userName, string password, Entities.CacheList caches);
        #endregion
    }
}
