﻿namespace GeoCaching.Server {
    using GeoCaching.Server;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class BLDataAccessFactory {

        private static BLDataAccessFactory instance;

        private string mailSender;
        private string mailSenderDisplayname;
        private string mailSubject;
        private string mailServerName;
        private int mailServerPort;
        private string mailUsername;
        private string mailPassword;
        private string bingAPIKey;

        private BLDataAccessFactory() {
            mailSender = ConfigurationManager.AppSettings["MAIL_SENDER"];
            mailSenderDisplayname = ConfigurationManager.AppSettings["MAIL_SENDER_DISPLAYNAME"];
            mailSubject = ConfigurationManager.AppSettings["MAIL_SUBJECT"];
            mailServerName = ConfigurationManager.AppSettings["MAIL_SERVER_NAME"];
            mailServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_SERVER_PORT"]);
            mailUsername = ConfigurationManager.AppSettings["MAIL_USERNAME"];
            mailPassword = ConfigurationManager.AppSettings["MAIL_PASSWORD"];
            bingAPIKey = ConfigurationManager.AppSettings["BING_API_KEY"];
        }

        public static BLDataAccessFactory GetInstance() {
            if (instance == null) {
                return new BLDataAccessFactory();
            }
            else {
                return instance;
            }
        }


        public string MailSender {
            get { return mailSender; }
        }


        public string MailSenderDisplayname {
            get { return mailSenderDisplayname; }
        }

        public string MailSubject {
            get { return mailSubject; }
        }

        public string MailServerName {
            get { return mailServerName; }
        }

        public int MailServerPort {
            get { return mailServerPort; }
        }

        public string MailUsername {
            get { return mailUsername; }
        }

        public string MailPassword {
            get { return mailPassword; }
        }

        public string BingAPIKey {
            get { return bingAPIKey; }
        }
    }
}