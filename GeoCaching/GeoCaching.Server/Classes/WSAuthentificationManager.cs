﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSAuthentificationManager : AbstractAuthentificationManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();
        
        public override User ValidateUser(string userName, string password) {
            var user = service.ValidateUser(userName, password);
            if (user == null)
                return null;
            return CreateUserFromWSUser(user);
        }

        #region Helper Methods
        private User CreateUserFromWSUser(WS.User wsUser) {
            return new User() {
                Id = wsUser.Id,
                Name = wsUser.Name,
                Password = wsUser.Password,
                Email = wsUser.Email,
                ResidenceCoordinates = new Coordinate() {
                    Latitude = wsUser.ResidenceCoordinates.Latitude,
                    Longitude = wsUser.ResidenceCoordinates.Longitude,
                    SpatialReferenceId = wsUser.ResidenceCoordinates.SpatialReferenceId
                }
            };
        }

        private WS.User CreateWSUserFromUser(User dUser) {
            return new WS.User() {
                Id = dUser.Id,
                Name = dUser.Name,
                Password = dUser.Password,
                Email = dUser.Email,
                ResidenceCoordinates = new WS.Coordinate() {
                    Latitude = dUser.ResidenceCoordinates.Latitude,
                    Longitude = dUser.ResidenceCoordinates.Longitude,
                    SpatialReferenceId = dUser.ResidenceCoordinates.SpatialReferenceId
                }
            };
        }
        #endregion

    }
}
