﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSCacheManager : AbstractCacheManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();


        #region ICacheManager Members

        public override IEnumerable<Entities.Cache> GetAllCaches(string userName, string password) {
            var cacheList = service.GetAllCaches(userName, password);
            if (cacheList == null)
                return null;
            return cacheList.Select(
                c => CreateCacheFromWSCache(c)).ToList();
        }

        public override Cache GetCacheById(string userName, string password, int id) {
            return CreateCacheFromWSCache(service.GetCacheById(userName, password, id));
        }

        public override Cache GetCacheByName(string userName, string password, string name) {
            return CreateCacheFromWSCache(service.GetCacheByName(userName, password, name));
        }

        public override bool CreateCache(string userName, string password, Cache cache) {
            return service.CreateCache(userName, password, CreateWSCacheFromCache(cache));
        }

        public override bool UpdateCache(string userName, string password, Cache cache) {
            return service.UpdateCache(userName, password, CreateWSCacheFromCache(cache));
        }

        public override bool DeleteCache(string userName, string password, int id) {
            return service.DeleteCache(userName, password, id);
        }

        public override IEnumerable<Picture> GetPicturesOfCache(string userName, string password, int cacheId) {
            var pictureList = service.GetPicturesOfCache(userName, password, cacheId);
            if (pictureList == null)
                return null;
            return pictureList.Select(p => CreatePictureFromWSPicture(p));
        }

        public override bool AddPicture(string userName, string password, Picture picture) {
            return service.AddPicture(userName, password, CreateWSPictureFromPicture(picture));
        }

        public override bool RemovePicture(string userName, string password, int pictureId) {
            return service.RemovePicture(userName, password, pictureId);
        }

        public override bool IsCacheInUse(string userName, string password, int cacheId) {
            return service.IsCacheInUse(userName, password, cacheId);
        }

        public override bool Exists(string userName, string password, string cacheName) {
            return service.Exists(userName, password, cacheName);
        }

        public override int GetSearchStateCountOfCache(string userName, string password, int cacheId, SearchState searchState) {
            return service.GetSearchStateCountOfCache(userName, password, cacheId, (WS.SearchState)searchState);
        }

        public override int GetCountOfHidedCaches(string userName, string password, int userId, CacheList cacheList) {
            if (cacheList != null && cacheList.Caches != null) {
                WS.CacheList wsCacheList = new WS.CacheList();
                wsCacheList.Caches = cacheList.Caches.Select(c => CreateWSCacheFromCache(c)).ToArray();
                return service.GetCountOfHidedCaches(userName, password, userId, wsCacheList);
            }
            return default(int);
        }

        public override double GetSearchLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int searchLevel) {
            WS.Coordinate wsCoordinate = null;
            if (region != null && region.Latitude != default(double) && region.Longitude != default(double) & region.SpatialReferenceId != default(int))
                wsCoordinate = new WS.Coordinate() {
                    Latitude = region.Latitude,
                    Longitude = region.Longitude,
                    SpatialReferenceId = region.SpatialReferenceId
                };

            return service.GetSearchLevelPercentage(userName, password, wsCoordinate, radius, from, to, searchLevel);
        }

        public override double GetAreaLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int areaLevel) {
            WS.Coordinate wsCoordinate = null;
            if (region != null && region.Latitude != default(double) && region.Longitude != default(double) & region.SpatialReferenceId != default(int))
                wsCoordinate = new WS.Coordinate() {
                    Latitude = region.Latitude,
                    Longitude = region.Longitude,
                    SpatialReferenceId = region.SpatialReferenceId
                };

            return service.GetAreaLevelPercentage(userName, password, wsCoordinate, radius, from, to, areaLevel);
        }

        public override double GetSizePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheSize size) {
            WS.Coordinate wsCoordinate = null;
            if (region != null && region.Latitude != default(double) && region.Longitude != default(double) & region.SpatialReferenceId != default(int))
                wsCoordinate = new WS.Coordinate() {
                    Latitude = region.Latitude,
                    Longitude = region.Longitude,
                    SpatialReferenceId = region.SpatialReferenceId
                };

            return service.GetSizePercentage(userName, password, wsCoordinate, radius, from, to, (WS.CacheSize)size);
        }

        public override double GetTypePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheType type) {
            WS.Coordinate wsCoordinate = null;
            if (region != null && region.Latitude != default(double) && region.Longitude != default(double) & region.SpatialReferenceId != default(int))
                wsCoordinate = new WS.Coordinate() {
                    Latitude = region.Latitude,
                    Longitude = region.Longitude,
                    SpatialReferenceId = region.SpatialReferenceId
                };

            return service.GetTypePercentage(userName, password, wsCoordinate, radius, from, to, (WS.CacheType)type);
        }

        public override IEnumerable<Cache> FilterCaches(string userName, string password, DateTime from, DateTime to, CacheTypeList cacheTypes, CacheSizeList cacheSizes, AreaLevelList areaLevels, SearchLevelList searchLevels, Coordinate region, double radius, RatingList ratings, SearchStateList searchStates) {
            WS.CacheTypeList wsCacheTypeList = new WS.CacheTypeList();
            WS.CacheSizeList wsCacheSizeList = new WS.CacheSizeList();
            WS.AreaLevelList wsAreaLevelList = new WS.AreaLevelList();
            WS.SearchLevelList wsSearchLevelList = new WS.SearchLevelList();
            WS.Coordinate wsCoordinate = new WS.Coordinate();
            WS.SearchStateList wsSearchStateList = new WS.SearchStateList();
            WS.RatingList wsRatingList = new WS.RatingList();
            if (cacheTypes != null && cacheTypes.CacheTypes != null)
                wsCacheTypeList.CacheTypes = cacheTypes.CacheTypes.Select(c => (WS.CacheType)(c)).ToArray();
            if (cacheSizes != null && cacheSizes.CacheSizes != null)
                wsCacheSizeList.CacheSizes = cacheSizes.CacheSizes.Select(c => (WS.CacheSize)(c)).ToArray();
            if (areaLevels != null && areaLevels.AreaLevels != null)
                wsAreaLevelList.AreaLevels = areaLevels.AreaLevels.ToArray();
            if (searchLevels != null && searchLevels.SearchLevels != null)
                wsSearchLevelList.SearchLevels = searchLevels.SearchLevels.ToArray();
            if (region != null && region.Latitude != default(double) && region.Longitude != default(double) & region.SpatialReferenceId != default(int)) {
                wsCoordinate.Latitude = region.Latitude;
                wsCoordinate.Longitude = region.Longitude;
                wsCoordinate.SpatialReferenceId = region.SpatialReferenceId;
            }
            if (searchStates != null && searchStates.SearchStates != null)
                wsSearchStateList.SearchStates = searchStates.SearchStates.Select(c => (WS.SearchState)(c)).ToArray();
            if (ratings != null && ratings.Ratings != null)
                wsRatingList.Ratings = ratings.Ratings.ToArray();



            return service.FilterCaches(userName, password, from, to, wsCacheTypeList, wsCacheSizeList, wsAreaLevelList, wsSearchLevelList, wsCoordinate, radius, wsRatingList, wsSearchStateList).Select(c => CreateCacheFromWSCache(c)).ToList();
        }

        #endregion

        #region Helper Methods
        private Cache CreateCacheFromWSCache(WS.Cache wsCache) {
            return new Cache() {
                Id = wsCache.Id,
                Name = wsCache.Name,
                OwnerId = wsCache.OwnerId,
                Puzzle = wsCache.Puzzle,
                AreaLevel = wsCache.AreaLevel,
                Coordinates = new Coordinate() {
                    Latitude = wsCache.Coordinates.Latitude,
                    Longitude = wsCache.Coordinates.Longitude,
                    SpatialReferenceId = wsCache.Coordinates.SpatialReferenceId
                },
                CreationDate = wsCache.CreationDate,
                SearchLevel = wsCache.SearchLevel,
                Size = (CacheSize)wsCache.Size,
                State = (CacheState)wsCache.State,
                Type = (CacheType)wsCache.Type
            };
        }

        private WS.Cache CreateWSCacheFromCache(Cache dCache) {
            return new WS.Cache() {
                Id = dCache.Id,
                Name = dCache.Name,
                OwnerId = dCache.OwnerId,
                Puzzle = dCache.Puzzle,
                AreaLevel = dCache.AreaLevel,
                Coordinates = new WS.Coordinate() {
                    Latitude = dCache.Coordinates.Latitude,
                    Longitude = dCache.Coordinates.Longitude,
                    SpatialReferenceId = dCache.Coordinates.SpatialReferenceId
                },
                CreationDate = dCache.CreationDate,
                SearchLevel = dCache.SearchLevel,
                Size = (WS.CacheSize)dCache.Size,
                State = (WS.CacheState)dCache.State,
                Type = (WS.CacheType)dCache.Type
            };
        }

        private Picture CreatePictureFromWSPicture(WS.Picture wsPicture) {
            return new Picture() {
                Id = wsPicture.Id,
                CacheId = wsPicture.CacheId,
                ImageData = wsPicture.ImageData
            };
        }

        private WS.Picture CreateWSPictureFromPicture(Picture dPicture) {
            return new WS.Picture() {
                Id = dPicture.Id,
                CacheId = dPicture.CacheId,
                ImageData = dPicture.ImageData
            };
        }

        #endregion
    }
}
