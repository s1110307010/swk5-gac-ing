﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSLogbookManager : AbstractLogbookManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();

        public override LogBook GetLogBook(string userName, string password, int id) {
            return CreateLogBookFromWSLogBook(service.GetLogBook(userName, password, id));
        }

        public override IEnumerable<LogBook> GetAllLogBookEntriesOfCache(string userName, string password, int cacheId) {
            var cacheList = service.GetAllLogBookEntriesOfCache(userName, password, cacheId);
            if (cacheList == null)
                return null;
            return cacheList.Select(
                l => CreateLogBookFromWSLogBook(l)).ToList();
        }

        public override int GetCountOfCachesByState(string userName, string password, int userId, CacheList cacheList, SearchState state) {
            WS.CacheList wsCacheList = new WS.CacheList();
            if (cacheList != null && cacheList.Caches != null)
                wsCacheList.Caches = cacheList.Caches.Select(c => CreateWSCacheFromCache(c)).ToArray();

            return service.GetCountOfCachesByState(userName, password, userId, wsCacheList, (WS.SearchState)state);
        }

        public override IEnumerable<LogBook> GetAllLogBooks(string userName, string password) {
            var logBookList = service.GetAllLogBooks(userName, password);
            if (logBookList == null)
                return null;

            return logBookList.Select(
                l => CreateLogBookFromWSLogBook(l)).ToList();
        }

        public override bool CreateLogBook(string userName, string password, LogBook logBook) {
            return service.CreateLogBook(userName, password, CreateWSLogBookFromLogBook(logBook));
        }

        public override bool DeleteLogBook(string userName, string password, int id) {
            return service.DeleteLogBook(userName, password, id);
        }

        #region Helper Methods
        private LogBook CreateLogBookFromWSLogBook(WS.LogBook wsLogBook) {
            return new LogBook() {
                Id = wsLogBook.Id,
                UserId = wsLogBook.UserId,
                CacheId = wsLogBook.CacheId,
                SearchState = (SearchState)wsLogBook.SearchState,
                Comment = wsLogBook.Comment,
                CreationDate = wsLogBook.CreationDate
            };
        }

        private WS.LogBook CreateWSLogBookFromLogBook(LogBook dLogBook) {
            return new WS.LogBook() {
                Id = dLogBook.Id,
                UserId = dLogBook.UserId,
                CacheId = dLogBook.CacheId,
                SearchState = (WS.SearchState)dLogBook.SearchState,
                Comment = dLogBook.Comment,
                CreationDate = dLogBook.CreationDate
            };
        }

        private WS.Cache CreateWSCacheFromCache(Cache dCache) {
            return new WS.Cache() {
                Id = dCache.Id,
                Name = dCache.Name,
                OwnerId = dCache.OwnerId,
                Puzzle = dCache.Puzzle,
                AreaLevel = dCache.AreaLevel,
                Coordinates = new WS.Coordinate() {
                    Latitude = dCache.Coordinates.Latitude,
                    Longitude = dCache.Coordinates.Longitude,
                    SpatialReferenceId = dCache.Coordinates.SpatialReferenceId
                },
                CreationDate = dCache.CreationDate,
                SearchLevel = dCache.SearchLevel,
                Size = (WS.CacheSize)dCache.Size,
                State = (WS.CacheState)dCache.State,
                Type = (WS.CacheType)dCache.Type
            };
        }
        #endregion


    }
}
