﻿namespace GeoCaching.Server {
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	/// <summary>
	/// Implementation for the RegularExpressions class
	/// </summary>
	public static class RegularExpressions {
		const string EMAIL_EXPR = @"^(\w+([-+.]+\w+)*-?)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,6}|[0-9]{1,3})$";

		/// <summary>
		/// Gets or sets the value for Email
		/// </summary>
		public static string Email { get { return EMAIL_EXPR; } }
	}
}
