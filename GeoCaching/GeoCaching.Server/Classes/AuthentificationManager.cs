﻿namespace GeoCaching.Server {
    using GeoCaching.DAL;
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

    public class AuthentificationManager : AbstractAuthentificationManager {
		#region Variables
		IUserDao userDao;
		#endregion

		#region Constructor
		public AuthentificationManager() {
			this.userDao = DALFactory.GetUserDao();
		}
		#endregion

		#region IAuthentificationManager Members
        public override User ValidateUser(string userName, string password) {
			if (String.IsNullOrEmpty(userName))
				return null;
			if (String.IsNullOrEmpty(password))
				return null;

			User user = this.userDao.GetByUserName(userName);
			if (user == null)
				return null;

			if (!String.Equals(password, user.Password))
				return null;

			return user;
		}
		#endregion
	}
}