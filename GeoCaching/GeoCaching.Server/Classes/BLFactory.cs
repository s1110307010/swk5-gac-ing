﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public enum DataAccessType { Db, WebService }

    public static class BLFactory {
        #region GetAuthentificationManager
        public static AbstractAuthentificationManager GetAuthenticationManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.AuthentificationManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSAuthentificationManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetCacheManager
        public static AbstractCacheManager GetCacheManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.CacheManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSCacheManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetLogBookManager
        public static AbstractLogbookManager GetLogbookManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.LogbookManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSLogbookManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetRatingManager
        public static AbstractRatingManager GetRatingManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.RatingManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSRatingManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetRoleManager
        public static AbstractRoleManager GetRoleManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.RoleManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSRoleManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetUserManager
        public static AbstractUserManager GetUserManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.UserManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSUserManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion

        #region GetExportManager
        public static AbstractExportManager GetExportManager(DataAccessType type = DataAccessType.Db) {
            switch (type) {
                case DataAccessType.Db:
                    return new GeoCaching.Server.ExportManager();

                case DataAccessType.WebService:
                    return new GeoCaching.Server.WSExportManager();

                default:
					throw new ArgumentException("Invalid DataAccessType");
            }
        }
        #endregion
    }
}