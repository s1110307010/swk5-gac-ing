﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSExportManager : AbstractExportManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();

        public override byte[] ExportCachesToWord(string userName, string password, Entities.CacheList caches) {
            if (caches != null && caches.Caches != null)
                return service.ExportCachesToWord(userName, password, new WS.CacheList() {
                    Caches = caches.Caches.Select(c => CreateWSCacheFromCache(c)).ToArray()
                });
            return null;
        }

        private WS.Cache CreateWSCacheFromCache(Cache dCache) {
            return new WS.Cache() {
                Id = dCache.Id,
                Name = dCache.Name,
                OwnerId = dCache.OwnerId,
                Puzzle = dCache.Puzzle,
                AreaLevel = dCache.AreaLevel,
                Coordinates = new WS.Coordinate() {
                    Latitude = dCache.Coordinates.Latitude,
                    Longitude = dCache.Coordinates.Longitude,
                    SpatialReferenceId = dCache.Coordinates.SpatialReferenceId
                },
                CreationDate = dCache.CreationDate,
                SearchLevel = dCache.SearchLevel,
                Size = (WS.CacheSize)dCache.Size,
                State = (WS.CacheState)dCache.State,
                Type = (WS.CacheType)dCache.Type
            };
        }

    }
}
