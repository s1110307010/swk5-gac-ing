﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GeoCaching.DAL;

    public class RatingManager : AbstractRatingManager {
        #region Variables
        IRatingDao ratingDao;
        #endregion

        #region Constructor
        public RatingManager() {
            ratingDao = DALFactory.GetRatingDao();
        }
        #endregion

        #region IRatingManager Members

        public override Entities.Rating GetRating(string userName, string password, int id) {
            return ratingDao.Get(id);
        }

        public override IEnumerable<Entities.Rating> GetAllRatingsOfCache(string userName, string password, int cacheId) {
            var ratings = ratingDao.GetAllRatingsOfCache(cacheId);
            if (ratings != null) {
                return ratings.OrderBy(rating => rating.CreationDate);
            }
            else {
                return null;
            }
        }

        public override IEnumerable<Entities.Rating> GetAllRatings(string userName, string password) {
            var ratings = ratingDao.GetAll();
            if (ratings != null) {
                return ratings.OrderBy(rating => rating.CreationDate);
            }
            else {
                return null;
            }
        }

        public override bool CreateRating(string userName, string password, Entities.Rating rating) {
            return ratingDao.Create(rating);
        }

        public override bool DeleteRating(string userName, string password, int id) {
            return ratingDao.Delete(id);
        }

        #endregion
    }
}
