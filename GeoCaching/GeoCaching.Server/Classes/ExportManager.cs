﻿namespace GeoCaching.Server {
    using GeoCaching.DAL;
    using GeoCaching.Entities;
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Net;
    using System.Drawing;

    public class ExportManager : AbstractExportManager {

        BLDataAccessFactory dataAccess; 
        string documentPath;
        string documentFile;
        byte[] docArray = null;
        Microsoft.Office.Interop.Word.Application application;
        Microsoft.Office.Interop.Word.Document documentation;

        public ExportManager() {
            dataAccess = BLDataAccessFactory.GetInstance();
            this.documentPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            this.documentFile = documentPath + "\\GeoCache.docx";
        }

        #region AbstractExportManager
        public override byte[] ExportCachesToWord(string userName, string password, Entities.CacheList caches) {
            if (caches != null) {
                bool firstCache = true;
                application = new Microsoft.Office.Interop.Word.Application();
                application.Visible = false;
                documentation = application.Documents.Add();
                foreach (Entities.Cache cache in caches.Caches) {
                    if (!firstCache) {
                        application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdPageBreak);
                    }
                    InsertCacheDetail(userName, password, cache);
                    firstCache = false;
                }
            }

            this.documentFile = Directory.GetCurrentDirectory() + "\\GeoCache.docx";
            documentation.SaveAs2(this.documentFile);
            documentation.Close();
            application.Quit();

            docArray = File.ReadAllBytes(this.documentFile);
            File.Delete(this.documentFile);

            return docArray;
        }
        #endregion

        #region Helper Methods
        private void InsertCacheDetail(string userName, string password, Cache cache) {
            application.Selection.TypeText("Cache ID: " + cache.Id);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Bezeichnung: " + cache.Name);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Typ: " + cache.Type);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Suchlevel: " + cache.SearchLevel);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache AreaLevel: " + cache.AreaLevel);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Größe: " + cache.Size);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Status: " + cache.State);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.TypeText("Cache Erstellungsdatum: " + cache.CreationDate);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            if (new CacheManager().GetPicturesOfCache(userName, password, cache.Id) != null) {
                application.Selection.TypeText("Bilder zum Cache: ");
                application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
                InsertCachePictures(userName, password, cache);
                application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
                application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            }
            application.Selection.TypeText("Karte - Startpunkt des Cache: ");
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);
            InsertCacheMapLocation(cache);
            File.Delete(this.documentFile);
        }

        private void InsertCachePictures(string userName, string password, Cache cache) {
            IEnumerable<Entities.Picture> pictures = new CacheManager().GetPicturesOfCache(userName, password, cache.Id);

            if (pictures != null) {
                foreach (Entities.Picture picture in pictures) {
                    this.documentFile = Directory.GetCurrentDirectory() + "\\picture.jpg";
                    using (FileStream stream = new FileStream(this.documentFile, FileMode.Create, FileAccess.Write, FileShare.Read)) {
                        stream.Write(picture.ImageData, 0, picture.ImageData.Length);
                        application.Selection.InlineShapes.AddPicture(this.documentFile);
                    }
                }
            }
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);

        }

        private void InsertCacheMapLocation(Cache cache) {
            Uri imageRequest = new Uri(string.Format("http://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial/{0},{1}/15?mapSize=500,500&pp={0},{1};1&key={2}",
                cache.Coordinates.Latitude.ToString().Replace(',', '.'),
                cache.Coordinates.Longitude.ToString().Replace(',', '.'),
                dataAccess.BingAPIKey));
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(imageRequest);
            var response = request.GetResponse();

            this.documentFile = Directory.GetCurrentDirectory() + "\\picture.jpg";
            using (FileStream stream = new FileStream(this.documentFile, FileMode.Create, FileAccess.Write, FileShare.Read)) {
                var image = Image.FromStream(response.GetResponseStream());
                ImageConverter imgConverter = new ImageConverter();
                byte[] bytes = (byte[])imgConverter.ConvertTo(image, typeof(byte[]));
                stream.Write(bytes, 0, bytes.Length);
                application.Selection.InlineShapes.AddPicture(this.documentFile);
            }
            application.Selection.InsertBreak(Microsoft.Office.Interop.Word.WdBreakType.wdLineBreak);

        }
        #endregion
    }
}
