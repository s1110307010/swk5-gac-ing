﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GeoCaching.DAL;

    public class LogbookManager : AbstractLogbookManager {
        #region Variables
        ILogBookDao logbookDao;
        #endregion

        #region Constructor
        public LogbookManager() {
            logbookDao = DALFactory.GetLogBookDao();
        }
        #endregion

        #region IRoleManager Members
        public override Entities.LogBook GetLogBook(string userName, string password, int id) {
            return logbookDao.Get(id);
        }

        public override IEnumerable<Entities.LogBook> GetAllLogBookEntriesOfCache(string userName, string password, int cacheId) {
            var logBooks = logbookDao.GetAllLogBookEntriesOfCache(cacheId);
            if (logBooks != null) {
                return logBooks.OrderBy(logbook => logbook.CreationDate);
            }
            else {
                return null;
            }
        }

        public override int GetCountOfCachesByState(string userName, string password, int userId, Entities.CacheList cacheList, Entities.SearchState state) {
            if (cacheList == null || cacheList.Count() == 0) {
                return default(int);
            }
            else {
                IEnumerable<int> caches = new List<int>(cacheList.Caches.Select(t => t.Id));
                return logbookDao.GetCountOfCachesByState(userId, caches, state);
            }
        }

        public override IEnumerable<Entities.LogBook> GetAllLogBooks(string userName, string password) {
            var logBooks = logbookDao.GetAll();
            if (logBooks != null) {
                return logBooks.OrderBy(logbook => logbook.CreationDate);
            }
            else {
                return null;
            }
        }

        public override bool CreateLogBook(string userName, string password, Entities.LogBook logBook) {
            return logbookDao.Create(logBook);
        }

        public override bool DeleteLogBook(string userName, string password, int id) {
            return logbookDao.Delete(id);
        }
        #endregion
    }
}