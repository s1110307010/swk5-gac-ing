﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSRoleManager : AbstractRoleManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();

        public override IEnumerable<Role> GetAllRoles(string userName, string password) {
            var roleList = service.GetAllRoles(userName, password);
            if (roleList == null)
                return null;
            return roleList.Select(
                r => CreateRoleFromWSRole(r)).ToList();
        }

        public override Role GetRoleById(string userName, string password, int id) {
            return CreateRoleFromWSRole(service.GetRoleById(userName, password, id));
        }

        public override Role GetRoleByName(string userName, string password, string name) {
            return CreateRoleFromWSRole(service.GetRoleByName(userName, password, name));
        }

        public override void AssignRolesToUser(string userName, string password, int userId, RoleList roles) {
            WS.RoleList wsRoleList = new WS.RoleList();
            if (roles != null && roles.Roles != null)
                wsRoleList.Roles = roles.Roles.Select(c => CreateWSRoleFromRole(c)).ToArray();

            service.AssignRolesToUser(userName, password, userId, wsRoleList);
        }

        public override void RemoveRolesFromUser(string userName, string password, int userId, RoleList roles) {
            WS.RoleList wsRoleList = new WS.RoleList();
            if (roles != null && roles.Roles != null)
                wsRoleList.Roles = roles.Roles.Select(c => CreateWSRoleFromRole(c)).ToArray();

            service.RemoveRolesFromUser(userName, password, userId, wsRoleList);
        }

        public override IEnumerable<Role> GetRolesOfUser(string userName, string password, int userId) {
            var roleList = service.GetRolesOfUser(userName, password, userId);
            if (roleList == null)
                return null;
            return roleList.Select(r => CreateRoleFromWSRole(r)).ToList();
        }

        public override bool IsUserHider(string userName, string password, int userId) {
            return service.IsUserHider(userName, password, userId);
        }

        public override bool IsUserFinder(string userName, string password, int userId) {
            return service.IsUserFinder(userName, password, userId);
        }

        #region Helper Methods
        private Role CreateRoleFromWSRole(WS.Role wsUser) {
            return new Role() {
                Id = wsUser.Id,
                Name = wsUser.Name
            };
        }

        private WS.Role CreateWSRoleFromRole(Role dRole) {
            return new WS.Role() {
                Id = dRole.Id,
                Name = dRole.Name
            };
        }
        #endregion


    }
}
