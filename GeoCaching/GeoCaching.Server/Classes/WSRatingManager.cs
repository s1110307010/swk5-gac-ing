﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSRatingManager : AbstractRatingManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();

        public override Rating GetRating(string userName, string password, int id) {
            return CreateRatingFromWSRating(service.GetRating(userName, password, id));
        }

        public override IEnumerable<Rating> GetAllRatingsOfCache(string userName, string password, int cacheId) {
            var cacheList = service.GetAllRatingsOfCache(userName, password, cacheId);
            if (cacheList == null)
                return null;
            return cacheList.Select(
                r => CreateRatingFromWSRating(r)).ToList();
        }

        public override IEnumerable<Rating> GetAllRatings(string userName, string password) {
            var ratingList = service.GetAllRatings(userName, password);
            if (ratingList == null)
                return null;
            return ratingList.Select(
                r => CreateRatingFromWSRating(r)).ToList();
        }

        public override bool CreateRating(string userName, string password, Rating rating) {
            return service.CreateRating(userName, password, CreateWSRatingFromRating(rating));
        }

        public override bool DeleteRating(string userName, string password, int id) {
            return service.DeleteRating(userName, password, id);
        }

        #region Helper Methods
        private Rating CreateRatingFromWSRating(WS.Rating wsRating) {
            return new Rating() {
                Id = wsRating.Id,
                CacheId = wsRating.CacheId,
                UserId = wsRating.UserId,
                Rate = wsRating.Rate,
                CreationDate = wsRating.CreationDate
            };
        }

        private WS.Rating CreateWSRatingFromRating(Rating dRating) {
            return new WS.Rating() {
                Id = dRating.Id,
                CacheId = dRating.CacheId,
                UserId = dRating.UserId,
                Rate = dRating.Rate,
                CreationDate = dRating.CreationDate
            };
        }
        #endregion

    }
}
