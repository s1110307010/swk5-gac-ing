﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GeoCaching.Entities;
    using WS = GeoCaching.Server.GeoCachingService; // ALIAS


    public class WSUserManager : AbstractUserManager {

        private WS.GeoCachingService service = new WS.GeoCachingService();

        public override User GetUserById(string userName, string password, int id) {
            return CreateUserFromWSUser(service.GetUserById(userName, password, id));
        }

        public override User GetUserByName(string userName, string password, string userNameToGet) {
            return CreateUserFromWSUser(service.GetUserByName(userName, password, userName));
        }

        public override IEnumerable<User> GetAllUsers(string userName, string password) {
            var userList = service.GetAllUsers(userName, password);
            if (userList == null)
                return null;
            return userList.Select(u => CreateUserFromWSUser(u)).ToList();
        }

        public override bool Create(string userName, string password, User user) {
            return service.Create(userName, password, CreateWSUserFromUser(user));
        }

        public override bool Update(string userName, string password, User user) {
            return service.Update(userName, password, CreateWSUserFromUser(user));
        }

        public override bool Delete(string userName, string password, int id) {
            return service.Delete(userName, password, id);
        }

        public override bool ExistsUser(string userName, string password, string userNameToGet) {
            return service.ExistsUser(userName, password, userNameToGet);
        }

        public override bool IsEmailAdressInUse(string userName, string password, string email, int userId) {
            return service.IsEmailAdressInUse(userName, password, email, userId);
        }

        public override bool Reactivate(string userName, string password, int id) {
            return service.Reactivate(userName, password, id);
        }

        #region Helper Methods
        private User CreateUserFromWSUser(WS.User wsUser) {
            return new User() { 
                Id = wsUser.Id,
                Name = wsUser.Name,
                Password = wsUser.Password,
                Email = wsUser.Email,
                ResidenceCoordinates = new Coordinate() {
                    Latitude = wsUser.ResidenceCoordinates.Latitude,
                    Longitude = wsUser.ResidenceCoordinates.Longitude,
                    SpatialReferenceId = wsUser.ResidenceCoordinates.SpatialReferenceId
                }
            };
        }

        private WS.User CreateWSUserFromUser(User dUser) {
            return new WS.User() { 
                Id = dUser.Id,
                Name = dUser.Name,
                Password = dUser.Password,
                Email = dUser.Email,
                ResidenceCoordinates = new WS.Coordinate() {
                    Latitude = dUser.ResidenceCoordinates.Latitude,
                    Longitude = dUser.ResidenceCoordinates.Longitude,
                    SpatialReferenceId = dUser.ResidenceCoordinates.SpatialReferenceId
                }
            };
        }
        #endregion
 
    }
}
