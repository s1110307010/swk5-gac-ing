﻿namespace GeoCaching.Server {
    using GeoCaching.DAL;
    using GeoCaching.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CacheManager : AbstractCacheManager {
		#region Variables
		ICacheDao cacheDao;
		#endregion

		#region Constructor
		public CacheManager() {
			cacheDao = DALFactory.GetCacheDao();
		}
		#endregion

		#region ICacheManager Members
        public override Entities.Cache GetCacheById(string userName, string password, int id) {
            return cacheDao.Get(id);
        }

        public override Entities.Cache GetCacheByName(string userName, string password, string name) {
            return cacheDao.GetCacheByName(name);
        }

        public override IEnumerable<Entities.Cache> GetAllCaches(string userName, string password) {
            var caches = cacheDao.GetAll();
            if (caches != null) {
                return caches.OrderBy(cache => cache.Name).Where(c => c.State == CacheState.Available);
            } else {
                return null;
            }
		}

        public override bool DeleteCache(string userName, string password, int id) {
			if (!cacheDao.IsCacheInUse(id)) {
                IEnumerable<Entities.Picture> pictureList = cacheDao.GetPicturesOfCache(id);
                if (pictureList != null) {
                    foreach (Entities.Picture picture in pictureList) {
                        cacheDao.RemovePicture(picture.Id);
                    }                    
                }
				return cacheDao.Delete(id);
			} else {
				return false;
            }
		}

        public override bool CreateCache(string userName, string password, Entities.Cache cache) {
            if (IsCacheValid(userName, password, cache) && !cacheDao.Exists(cache.Name))
				return cacheDao.Create(cache);
			else
				return false;
		}

        private bool IsCacheValid(string userName, string password, Cache cache) {
			bool isValid = true;
			isValid &= !String.IsNullOrEmpty(cache.Name);
			isValid &= (cache.SearchLevel >= 1 && cache.SearchLevel <= 5);
			isValid &= (cache.AreaLevel >= 1 && cache.AreaLevel <= 5);
			isValid &= !String.IsNullOrEmpty(cache.Puzzle);
			isValid &= (cache.Size >= 0 && (int)cache.Size <= 5);
            isValid &= (cache.Type >= 0 && (int)cache.Type <= 8);
            isValid &= (cache.State >= 0 && (int)cache.State <= 1);
			isValid &= cache.Coordinates.Latitude >= -90 && cache.Coordinates.Latitude <= 90;
			isValid &= cache.Coordinates.Longitude >= -180 && cache.Coordinates.Longitude <= 180;
			return isValid;
		}

        public override bool UpdateCache(string userName, string password, Entities.Cache cache) {
            if (IsCacheValid(userName, password, cache))
				return cacheDao.Update(cache);
			else
				return false;
		}

        public override bool IsCacheInUse(string userName, string password, int cacheId) {
            return cacheDao.IsCacheInUse(cacheId);
        }

        public override bool Exists(string userName, string password, string cacheName) {
            return cacheDao.Exists(cacheName);
        }

        public override IEnumerable<Cache> FilterCaches(string userName, string password, DateTime from, DateTime to, CacheTypeList cacheTypes, CacheSizeList cacheSizes, AreaLevelList areaLevels, SearchLevelList searchLevels, Coordinate region, double radius, RatingList ratings, SearchStateList searchStates) {
            List<CacheType> typeList = null;
            List<CacheSize> sizeList = null;
            List<int> areaLevelList = null;
            List<int> searchLevelList = null;
            List<int> ratingList = null;
            List<SearchState> searchStateList = null;

            if (cacheTypes != null)
                typeList = cacheTypes.CacheTypes;
            if (cacheSizes != null)
                sizeList = cacheSizes.CacheSizes;
            if (areaLevels != null)
                areaLevelList = areaLevels.AreaLevels;
            if (searchLevels != null)
                searchLevelList = searchLevels.SearchLevels;
            if (ratings != null)
                ratingList = ratings.Ratings;
            if (searchStates != null)
                searchStateList = searchStates.SearchStates;
  
            return cacheDao.FilterCaches(from, to, typeList, sizeList, areaLevelList, searchLevelList, region, radius, ratingList, searchStateList).Where(c => c.State == CacheState.Available);
        }

        public override int GetSearchStateCountOfCache(string userName, string password, int cacheId, SearchState searchState) {
			return cacheDao.GetSearchStateCountOfCache(cacheId, searchState);
		}

        public override int GetCountOfHidedCaches(string userName, string password, int userId, CacheList cacheList) {
            if (cacheList == null || cacheList.Count() == 0) {
                return default(int);
            }
            else {
                IEnumerable<int> caches = new List<int>(cacheList.Caches.Select(t => t.Id));
                return cacheDao.GetCountOfHidedCaches(userId, caches);
            }
        }

        public override double GetSearchLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int searchLevel) {
			return Math.Round(cacheDao.GetSearchLevelPercentage(region, radius, from, to, searchLevel), 2);
		}

        public override double GetAreaLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int areaLevel) {
			return Math.Round(cacheDao.GetAreaLevelPercentage(region, radius, from, to, areaLevel), 2);
		}

        public override double GetSizePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheSize size) {
            return Math.Round(cacheDao.GetSizePercentage(region, radius, from, to, (int)size), 2);
		}

        public override double GetTypePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheType type) {
            return Math.Round(cacheDao.GetTypePercentage(region, radius, from, to, (int)type), 2);
		}

        #region Picture
        public override IEnumerable<Picture> GetPicturesOfCache(string userName, string password, int cacheId) {
            return cacheDao.GetPicturesOfCache(cacheId);
        }

        public override bool AddPicture(string userName, string password, Picture picture) {
            return cacheDao.AddPicture(picture);
        }

        public override bool RemovePicture(string userName, string password, int pictureId) {
            return cacheDao.RemovePicture(pictureId);
		}
		#endregion
		#endregion
	}
}
