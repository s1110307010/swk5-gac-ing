﻿namespace GeoCaching.Server {
	using GeoCaching.DAL;
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using System.Text;
	using System.Text.RegularExpressions;
	using System.Threading.Tasks;

    public class UserManager : AbstractUserManager {
		#region Variables
		IUserDao userDao;
		#endregion

		#region Constructor
		public UserManager() {
			userDao = DALFactory.GetUserDao();
		}
		#endregion

		#region IUserManager Members
        public override Entities.User GetUserById(string userName, string password, int id) {
			return userDao.Get(id);
		}

        public override Entities.User GetUserByName(string userName, string password, string userNameToGet) {
			return userDao.GetByUserName(userNameToGet);
		}

        public override IEnumerable<Entities.User> GetAllUsers(string userName, string password) {
			return userDao.GetAll().OrderBy(user => user.Name);
		}

        public override bool Create(string userName, string password, Entities.User user) {
			if (IsUserValid(user) && !userDao.Exists(user.Name)) {
				MailManager.SendMailNotification(user);
				return userDao.Create(user);
			}
			else
				return false;
		}

        public override bool Update(string userName, string password, Entities.User user) {
            if (IsUserValid(user) && !IsEmailAdressInUse(userName, password, user.Email, user.Id)) {
				User oldUser = userDao.Get(user.Id);
				if (oldUser.Email != user.Email)
					MailManager.SendMailNotification(user);
				return userDao.Update(user);
			}
			else
				return false;
		}

        public override bool Delete(string userName, string password, int id) {
			return userDao.Delete(id);
		}

        public override bool ExistsUser(string userName, string password, string userNameToGet) {
			if (String.IsNullOrEmpty(userNameToGet))
				return false;
			return userDao.Exists(userNameToGet);
		}

        public override bool IsEmailAdressInUse(string userName, string password, string email, int userId) {
			if (String.IsNullOrEmpty(email) || userId == default(int))
				return false;
			return userDao.IsEmailAdressInUse(email, userId);
		}

        public override bool Reactivate(string userName, string password, int id) {
            return userDao.Reactivate(id);
        }
		#endregion

		#region private Helper Methods

		
		private bool IsUserValid(User user) {
			bool isValid = true;
			isValid &= !String.IsNullOrEmpty(user.Name);
			isValid &= user.Id == 0 && !String.IsNullOrEmpty(user.Password) || user.Id != 0 && !String.IsNullOrEmpty(user.Password);
			isValid &= !String.IsNullOrEmpty(user.Email) && Regex.IsMatch(user.Email, RegularExpressions.Email);
			isValid &= user.ResidenceCoordinates.Latitude >= -90 && user.ResidenceCoordinates.Latitude <= 90;
			isValid &= user.ResidenceCoordinates.Longitude >= -180 && user.ResidenceCoordinates.Longitude <= 180;
			return isValid;
		}
		#endregion
	}
}