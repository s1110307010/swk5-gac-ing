﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GeoCaching.DAL;

    public class RoleManager : AbstractRoleManager {
        #region Variables
        IRoleDao roleDao;
        #endregion

        #region Constructor
        public RoleManager() {
            roleDao = DALFactory.GetRoleDao();
        }
        #endregion

        #region IRoleManager Members

        public override IEnumerable<Entities.Role> GetAllRoles(string userName, string password) {
            return roleDao.GetAllRoles().OrderBy(role => role.Name);
        }

        public override Entities.Role GetRoleById(string userName, string password, int id) {
            return roleDao.GetRoleById(id);
        }

        public override Entities.Role GetRoleByName(string userName, string password, string name) {
            return roleDao.GetRoleByName(name);
        }

        public override void AssignRolesToUser(string userName, string password, int userId, Entities.RoleList roles) {
			roleDao.AssignRolesToUser(userId, roles.Roles);
        }

        public override void RemoveRolesFromUser(string userName, string password, int userId, Entities.RoleList roles) {
			roleDao.RemoveRolesFromUser(userId, roles.Roles);
        }

        public override IEnumerable<Entities.Role> GetRolesOfUser(string userName, string password, int userId) {
            IEnumerable<Entities.Role> roleList = roleDao.GetRolesOfUser(userId);
            if (roleList != null) {
                return roleList.OrderBy(role => role.Name);
            } else {
                return null;
            }
        }

        public override bool IsUserHider(string userName, string password, int userId) {
            return FitUserRole(userId, "Hider");
        }

        public override bool IsUserFinder(string userName, string password, int userId) {
            return FitUserRole(userId, "Finder");
        }
        #endregion

        #region Private Helper Methods

        private bool FitUserRole(int userId, string roleName) {
            IEnumerable<Entities.Role> userRoles = roleDao.GetRolesOfUser(userId);
            if (userRoles == null) {
                return false;
            }
            foreach (Entities.Role role in userRoles) {
                if (role.Name.Equals(roleName)) {
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}