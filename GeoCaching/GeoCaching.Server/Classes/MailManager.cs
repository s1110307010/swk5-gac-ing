﻿namespace GeoCaching.Server {
    using GeoCaching.Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using System.Configuration;

    public static class MailManager {
        #region Constants


        #endregion

        #region SendMail
        public static void SendMailNotification(User user) {
            try {
                BLDataAccessFactory dataAcces = BLDataAccessFactory.GetInstance();
                MailMessage email = new MailMessage();
                email.From = new MailAddress(dataAcces.MailSender, dataAcces.MailSenderDisplayname, System.Text.Encoding.UTF8);
                email.To.Add(new MailAddress(user.Email));
                email.Subject = dataAcces.MailSubject;
                email.SubjectEncoding = System.Text.Encoding.UTF8;
                email.BodyEncoding = System.Text.Encoding.UTF8;
                email.Body = String.Format(
                    "Sie wurden auf der Plattform CAC#ING als Benutzer registriert und erhalten hiermit Ihre Zugangsdaten:{0}{0}Benutzername: {1}{0}Passwort: {2}{0}{0}Liebe Grüße{0}Das CAC#ING Team",
                    Environment.NewLine,
                    user.Name,
                    user.Password);

                SmtpClient mailClient = new SmtpClient(dataAcces.MailServerName, dataAcces.MailServerPort);
                mailClient.EnableSsl = true;
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = new System.Net.NetworkCredential(dataAcces.MailUsername, dataAcces.MailPassword);
                mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                mailClient.Send(email);
            }
            catch (SmtpException ex) {
                Console.WriteLine("Mail-Exception: " + ex);
            }

            //			mailClient.SendAsync(email, "Token");
        }
        #endregion
    }
}