﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractUserManager : IUserManager, IUserManagerAsync {

        #region IUserManager Members
        public abstract Entities.User GetUserById(string userName, string password, int id);
        public abstract Entities.User GetUserByName(string userName, string password, string userNameToGet);
        public abstract IEnumerable<Entities.User> GetAllUsers(string userName, string password);
        public abstract bool Create(string userName, string password, Entities.User user);
        public abstract bool Update(string userName, string password, Entities.User user);
        public abstract bool Delete(string userName, string password, int id);
        public abstract bool ExistsUser(string userName, string password, string userNameToGet);
        public abstract bool IsEmailAdressInUse(string userName, string password, string email, int userId);
        public abstract bool Reactivate(string userName, string password, int id);
        #endregion

        #region IUserManagerAsync Members
        public Task<Entities.User> GetUserByIdAsync(string userName, string password, int id) {
            return Task.Run(() => GetUserById(userName, password, id));
        }

        public Task<Entities.User> GetUserByNameAsync(string userName, string password, string userNameToGet) {
            return Task.Run(() => GetUserByName(userName, password, userNameToGet));
        }

        public Task<IEnumerable<Entities.User>> GetAllUsersAsync(string userName, string password) {
            return Task.Run(() => GetAllUsers(userName, password));
        }

        public Task<bool> CreateAsync(string userName, string password, Entities.User user) {
            return Task.Run(() => Create(userName, password, user));
        }

        public Task<bool> UpdateAsync(string userName, string password, Entities.User user) {
            return Task.Run(() => Update(userName, password, user));
        }

        public Task<bool> DeleteAsync(string userName, string password, int id) {
            return Task.Run(() => Delete(userName, password, id));
        }

        public Task<bool> ExistsUserAsync(string userName, string password, string userNameToGet) {
            return Task.Run(() => ExistsUser(userName, password, userNameToGet));
        }

        public Task<bool> IsEmailAdressInUseAsync(string userName, string password, string email, int userId) {
            return Task.Run(() => IsEmailAdressInUse(userName, password, email, userId));
        }

        public Task<bool> ReactivateAsync(string userName, string password, int id) {
            return Task.Run(() => Reactivate(userName, password, id));
        }
        #endregion
    }
}
