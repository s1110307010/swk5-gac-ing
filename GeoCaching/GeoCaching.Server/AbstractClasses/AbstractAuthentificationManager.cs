﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractAuthentificationManager : IAuthentificationManager, IAuthentificationManagerAsync {
        #region IAuthenificationManager Members
        public abstract Entities.User ValidateUser(string userName, string password);
        #endregion

        #region IAuthenificationManagerAsync Members
        public Task<Entities.User> ValidateUserAsync(string userName, string password) {
            return Task.Run(() => ValidateUser(userName, password));
        }
        #endregion
    }
}
