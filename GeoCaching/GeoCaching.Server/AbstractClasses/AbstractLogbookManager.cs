﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractLogbookManager : ILogbookManager, ILogbookManagerAsync {

        #region ILogbookManager Members
        public abstract Entities.LogBook GetLogBook(string userName, string password, int id);
        public abstract IEnumerable<Entities.LogBook> GetAllLogBookEntriesOfCache(string userName, string password, int cacheId);
        public abstract int GetCountOfCachesByState(string userName, string password, int userId, Entities.CacheList cacheList, Entities.SearchState state);
        public abstract IEnumerable<Entities.LogBook> GetAllLogBooks(string userName, string password);
        public abstract bool CreateLogBook(string userName, string password, Entities.LogBook logBook);
        public abstract bool DeleteLogBook(string userName, string password, int id);
        #endregion

        #region ILogbookManagerAsync Members
        public Task<Entities.LogBook> GetLogBookAsync(string userName, string password, int id) {
            return Task.Run(() => GetLogBook(userName, password, id));
        }

        public Task<IEnumerable<Entities.LogBook>> GetAllLogBookEntriesOfCacheAsync(string userName, string password, int cacheId) {
            return Task.Run(() => GetAllLogBookEntriesOfCache(userName, password, cacheId));
        }

        public Task<int> GetCountOfCachesByStateAsync(string userName, string password, int userId, Entities.CacheList cacheList, Entities.SearchState state) {
            return Task.Run(() => GetCountOfCachesByState(userName, password, userId, cacheList, state));
        }

        public Task<IEnumerable<Entities.LogBook>> GetAllLogBooksAsync(string userName, string password) {
            return Task.Run(() => GetAllLogBooks(userName, password));
        }

        public Task<bool> CreateLogBookAsync(string userName, string password, Entities.LogBook logBook) {
            return Task.Run(() => CreateLogBook(userName, password, logBook));
        }

        public Task<bool> DeleteLogBookAsync(string userName, string password, int id) {
            return Task.Run(() => DeleteLogBook(userName, password, id));
        }
        #endregion
    }
}
