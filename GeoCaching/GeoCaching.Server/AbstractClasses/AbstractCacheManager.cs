﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractCacheManager : ICacheManager, ICacheManagerAsync {

        #region ICacheManager Members
        public abstract IEnumerable<Entities.Cache> GetAllCaches(string userName, string password);
        public abstract Entities.Cache GetCacheById(string userName, string password, int id);
        public abstract Entities.Cache GetCacheByName(string userName, string password, string name);
        public abstract bool CreateCache(string userName, string password, Entities.Cache cache);
        public abstract bool UpdateCache(string userName, string password, Entities.Cache cache);
        public abstract bool DeleteCache(string userName, string password, int id);
        public abstract IEnumerable<Entities.Picture> GetPicturesOfCache(string userName, string password, int cacheId);
        public abstract bool AddPicture(string userName, string password, Entities.Picture picture);
        public abstract bool RemovePicture(string userName, string password, int pictureId);
        public abstract bool IsCacheInUse(string userName, string password, int cacheId);
        public abstract bool Exists(string userName, string password, string cacheName);
        public abstract int GetSearchStateCountOfCache(string userName, string password, int cacheId, Entities.SearchState searchState);
        public abstract int GetCountOfHidedCaches(string userName, string password, int userId, Entities.CacheList cacheList);
        public abstract double GetSearchLevelPercentage(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, int searchLevel);
        public abstract double GetAreaLevelPercentage(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, int areaLevel);
        public abstract double GetSizePercentage(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, Entities.CacheSize size);
        public abstract double GetTypePercentage(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, Entities.CacheType type);
        public abstract IEnumerable<Entities.Cache> FilterCaches(string userName, string password, DateTime from, DateTime to, Entities.CacheTypeList cacheTypes, Entities.CacheSizeList cacheSizes, Entities.AreaLevelList areaLevels, Entities.SearchLevelList searchLevels, Entities.Coordinate region, double radius, Entities.RatingList ratings, Entities.SearchStateList searchStates);
        #endregion

        #region ICacheManagerAsync Members
        public Task<IEnumerable<Entities.Cache>> GetAllCachesAsync(string userName, string password) {
            return Task.Run(() => GetAllCaches(userName, password));
        }

        public Task<Entities.Cache> GetCacheByIdAsync(string userName, string password, int id) {
            return Task.Run(() => GetCacheById(userName, password, id));
        }

        public Task<Entities.Cache> GetCacheByNameAsync(string userName, string password, string name) {
            return Task.Run(() => GetCacheByName(userName, password, name));
        }

        public Task<bool> CreateCacheAsync(string userName, string password, Entities.Cache cache) {
            return Task.Run(() => CreateCache(userName, password, cache));
        }

        public Task<bool> UpdateCacheAsync(string userName, string password, Entities.Cache cache) {
            return Task.Run(() => UpdateCache(userName, password, cache));
        }

        public Task<bool> DeleteCacheAsync(string userName, string password, int id) {
            return Task.Run(() => DeleteCache(userName, password, id));
        }

        public Task<IEnumerable<Entities.Picture>> GetPicturesOfCacheAsync(string userName, string password, int cacheId) {
            return Task.Run(() => GetPicturesOfCache(userName, password, cacheId));
        }

        public Task<bool> AddPictureAsync(string userName, string password, Entities.Picture picture) {
            return Task.Run(() => AddPicture(userName, password, picture));
        }

        public Task<bool> RemovePictureAsync(string userName, string password, int pictureId) {
            return Task.Run(() => RemovePicture(userName, password, pictureId));
        }

        public Task<bool> IsCacheInUseAsync(string userName, string password, int cacheId) {
            return Task.Run(() => IsCacheInUse(userName, password, cacheId));
        }

        public Task<bool> ExistAsync(string userName, string password, string cacheName) {
            return Task.Run(() => Exists(userName, password, cacheName));
        }

        public Task<int> GetSearchStateCountOfCacheAsync(string userName, string password, int cacheId, Entities.SearchState searchState) {
            return Task.Run(() => GetSearchStateCountOfCache(userName, password, cacheId, searchState));
        }

        public Task<int> GetCountOfHidedCachesAsync(string userName, string password, int userId, Entities.CacheList cacheList) {
            return Task.Run(() => GetCountOfHidedCaches(userName, password, userId, cacheList));
        }

        public Task<double> GetSearchLevelPercentageAsync(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, int searchLevel) {
            return Task.Run(() => GetSearchLevelPercentage(userName, password, region, radius, from, to, searchLevel));
        }

        public Task<double> GetAreaLevelPercentageAsync(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, int areaLevel) {
            return Task.Run(() => GetAreaLevelPercentage(userName, password, region, radius, from, to, areaLevel));
        }

        public Task<double> GetSizePercentageAsync(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, Entities.CacheSize size) {
            return Task.Run(() => GetSizePercentage(userName, password, region, radius, from, to, size));
        }

        public Task<double> GetTypePercentageAsync(string userName, string password, Entities.Coordinate region, double radius, DateTime from, DateTime to, Entities.CacheType type) {
            return Task.Run(() => GetTypePercentage(userName, password, region, radius, from, to, type));
        }

        public Task<IEnumerable<Entities.Cache>> FilterCachesAsync(string userName, string password, DateTime from, DateTime to, Entities.CacheTypeList cacheTypes, Entities.CacheSizeList cacheSizes, Entities.AreaLevelList areaLevels, Entities.SearchLevelList searchLevels, Entities.Coordinate region, double radius, Entities.RatingList ratings, Entities.SearchStateList searchStates) {
            return Task.Run(() => FilterCaches(userName, password, from, to, cacheTypes, cacheSizes, areaLevels, searchLevels, region, radius, ratings, searchStates));
        }

        #endregion
    }
}
