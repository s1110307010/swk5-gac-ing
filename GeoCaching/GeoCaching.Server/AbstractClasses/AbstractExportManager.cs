﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractExportManager : IExportManager, IExportManagerAsync {

        #region IExportManager Members
        public abstract byte[] ExportCachesToWord(string userName, string password, Entities.CacheList caches);
        #endregion

        #region IExportManagerAsync Members
        public Task<byte[]> ExportCachesToWordAsync(string userName, string password, Entities.CacheList caches) {
            return Task.Run(() => ExportCachesToWord(userName, password, caches));
        }
        #endregion
    }
}
