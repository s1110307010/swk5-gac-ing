﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractRoleManager : IRoleManager, IRoleManagerAsync {

        #region IRoleManager Members
        public abstract IEnumerable<Entities.Role> GetAllRoles(string userName, string password);
        public abstract Entities.Role GetRoleById(string userName, string password, int id);
        public abstract Entities.Role GetRoleByName(string userName, string password, string name);
        public abstract void AssignRolesToUser(string userName, string password, int userId, Entities.RoleList roles);
        public abstract void RemoveRolesFromUser(string userName, string password, int userId, Entities.RoleList roles);
        public abstract IEnumerable<Entities.Role> GetRolesOfUser(string userName, string password, int userId);
        public abstract bool IsUserHider(string userName, string password, int userId);
        public abstract bool IsUserFinder(string userName, string password, int userId);
        #endregion

        #region IRoleManagerAsync Members
        public Task<IEnumerable<Entities.Role>> GetAllRolesAsync(string userName, string password) {
            return Task.Run(() => GetAllRoles(userName, password));
        }

        public Task<Entities.Role> GetRoleByIdAsync(string userName, string password, int id) {
            return Task.Run(() => GetRoleById(userName, password, id));
        }

        public Task<Entities.Role> GetRoleByNameAsync(string userName, string password, string name) {
            return Task.Run(() => GetRoleByName(userName, password, name));
        }

        public void AssignRolesToUserAsync(string userName, string password, int userId, Entities.RoleList roles) {
            Task.Run(() => AssignRolesToUser(userName, password, userId, roles));
        }

        public void RemoveRolesFromUserAsync(string userName, string password, int userId, Entities.RoleList roles) {
            Task.Run(() => RemoveRolesFromUser(userName, password, userId, roles));
        }

        public Task<IEnumerable<Entities.Role>> GetRolesOfUserAsync(string userName, string password, int userId) {
            return Task.Run(() => GetRolesOfUser(userName, password, userId));
        }

        public Task<bool> IsUserHiderAsync(string userName, string password, int userId) {
            return Task.Run(() => IsUserHider(userName, password, userId));
        }

        public Task<bool> IsUserFinderAsync(string userName, string password, int userId) {
            return Task.Run(() => IsUserFinder(userName, password, userId));
        }
        #endregion
    }
}
