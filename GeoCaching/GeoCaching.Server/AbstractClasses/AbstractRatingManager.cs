﻿namespace GeoCaching.Server {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractRatingManager : IRatingManager, IRatingManagerAsync {

        #region IRatingManager Members
        public abstract Entities.Rating GetRating(string userName, string password, int id);
        public abstract IEnumerable<Entities.Rating> GetAllRatingsOfCache(string userName, string password, int cacheId);
        public abstract IEnumerable<Entities.Rating> GetAllRatings(string userName, string password);
        public abstract bool CreateRating(string userName, string password, Entities.Rating rating);
        public abstract bool DeleteRating(string userName, string password, int id);
        #endregion

        #region IRatingManagerAsync Members
        public Task<Entities.Rating> GetRatingAsync(string userName, string password, int id) {
            return Task.Run(() => GetRating(userName, password, id));
        }

        public Task<IEnumerable<Entities.Rating>> GetAllRatingsOfCacheAsync(string userName, string password, int cacheId) {
            return Task.Run(() => GetAllRatingsOfCache(userName, password, cacheId));
        }

        public Task<IEnumerable<Entities.Rating>> GetAllRatingsAsync(string userName, string password) {
            return Task.Run(() => GetAllRatings(userName, password));
        }

        public Task<bool> CreateRatingAsync(string userName, string password, Entities.Rating rating) {
            return Task.Run(() => CreateRating(userName, password, rating));
        }

        public Task<bool> DeleteRatingAsync(string userName, string password, int id) {
            return Task.Run(() => DeleteRating(userName, password, id));
        }
        #endregion
    }
}
