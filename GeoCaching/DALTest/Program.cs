﻿using GeoCaching.DAL;
using GeoCaching.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALTest {
	class Program {
		static void Main(string[] args) {

			// passt alles!
			ICacheDao cacheDao = DALManager.GetCacheDao();
			DateTime from = new DateTime(2007, 5, 26);
			DateTime to = new DateTime(2011, 11, 4);




			List<CacheType> cacheTypes = new List<CacheType>();
			cacheTypes.Add(CacheType.Virtual); // id 1
			cacheTypes.Add(CacheType.Multi); // id 2
			cacheTypes.Add(CacheType.Traditional); // id 3
			cacheTypes.Add(CacheType.Virtual); // id 4
			cacheTypes.Add(CacheType.Multi); // id 5

			List<CacheSize> cacheSizes = new List<CacheSize>();
			cacheSizes.Add(CacheSize.Normal);
			cacheSizes.Add(CacheSize.None);

			List<int> areaLevels = new List<int>();
			areaLevels.Add(2);
			areaLevels.Add(3);

			List<int> searchLevels = new List<int>();
			searchLevels.Add(1);
			searchLevels.Add(2);
			searchLevels.Add(3);

			List<int> ratings = new List<int>();
			ratings.Add(2);
			ratings.Add(5);

			List<SearchState> searchStates = new List<SearchState>();
			searchStates.Add(SearchState.Found);

			IEnumerable<Cache> caches = cacheDao.FilterCaches(from, to, cacheTypes, cacheSizes,
				areaLevels, searchLevels, new Coordinate(), ratings, searchStates);

			caches = cacheDao.FilterCaches(from, to, null, cacheSizes,
				areaLevels, searchLevels, new Coordinate(), ratings, searchStates);

			caches = cacheDao.FilterCaches(from, to, cacheTypes, null,
				areaLevels, searchLevels, new Coordinate(), ratings, searchStates);

			caches = cacheDao.FilterCaches(from, to, cacheTypes, cacheSizes,
				null, searchLevels, new Coordinate(), ratings, searchStates);

			caches = cacheDao.FilterCaches(from, to, cacheTypes, cacheSizes,
				areaLevels, null, new Coordinate(), ratings, searchStates);

			caches = cacheDao.FilterCaches(from, to, cacheTypes, cacheSizes,
				areaLevels, searchLevels, new Coordinate(), null, searchStates);

			caches = cacheDao.FilterCaches(from, to, cacheTypes, cacheSizes,
				areaLevels, searchLevels, new Coordinate(), ratings, null);

			caches = cacheDao.FilterCaches(from, to, null, null,
				null, null, new Coordinate(), null, null);
			

			// passt auch!
			//IUserDao userDao = DALManager.GetUserDao();

			//User user1 = userDao.GetByUserName("Andreas Etzelstorfer");
			//if (user1 != null)
			//	Console.WriteLine(userDao.GetByUserName("Andreas Etzelstorfer").Name);
			//else
			//	Console.WriteLine("Keine Daten gefunden");


			//IRatingDao ratingDao = DALManager.GetRatingDao();

			//var rateTest1 = ratingDao.Get(1);

			//IEnumerable<Rating> rateList = ratingDao.GetAllRatingsOfCache(4);
			//if (rateList != null) {
			//	foreach (Rating rate in rateList) {
			//		Console.WriteLine("{0}, {1}, {2}, {3}, {4}", rate.Id, rate.CacheId, rate.UserId, rate.Rate, rate.CreationDate);
			//	}
			//}

			//Rating rate2 = ratingDao.Get(1);
			//if (rate2 != null) {
			//	rate2.Rate = 5;
			//	ratingDao.Update(rate2);
			//	ratingDao.Create(rate2);
			//}

			//rateList = ratingDao.GetAll();
			//if (rateList != null) {
			//	foreach (Rating rate in rateList) {
			//		Console.WriteLine("{0}, {1}, {2}, {3}, {4}", rate.Id, rate.CacheId, rate.UserId, rate.Rate, rate.CreationDate);
			//	}
			//}

			//ratingDao.Delete(10);

			//ILogBookDao logBookDao = DALManager.GetLogBookDao();

			//IEnumerable<LogBook> logBookList = logBookDao.GetAllLogBookEntriesOfCache(1);

			//if (logBookList != null) {
			//	foreach (LogBook logBook in logBookList) {
			//		Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}", logBook.Id, logBook.UserId, logBook.CacheId, logBook.SearchState, logBook.Comment, logBook.CreationDate);
			//	}
			//}

			//LogBook log2 = logBookDao.Get(1);
			//if (log2 != null) {
			//	log2.Comment = "yes you are good";
			//	log2.SearchState = SearchState.Found;
			//	logBookDao.Update(log2);
			//	logBookDao.Create(log2);
			//}
			//logBookList = logBookDao.GetAll();
			//if (logBookList != null) {
			//	foreach (LogBook logBook in logBookList) {
			//		Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}", logBook.Id, logBook.UserId, logBook.CacheId, logBook.SearchState, logBook.Comment, logBook.CreationDate);
			//	}
			//}

			//logBookDao.Delete(10);

			//Console.ReadLine();
		}
	}
}
