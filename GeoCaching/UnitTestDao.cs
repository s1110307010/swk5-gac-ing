﻿namespace GeoCaching.DAL {
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class RatingDaoTest {
        [TestMethod]
        public void CountTest() {
            Assert.AreEqual(1, 1);
        }
    }

}

/*
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Swk5.Collections;

namespace HashDictionaryTest {

    [TestClass]
    public class HashDictionaryTests {
    
        [TestMethod]
        public void CountTest() {
            IDictionary<int, int> dict = new HashDictionary<int, int>();
            Assert.AreEqual(0, dict.Count);

            dict.Add(1, 10);
            Assert.AreEqual(1, dict.Count);

            dict.Add(2, 20);
            Assert.AreEqual(2, dict.Count);

            dict[2] = 30;
            Assert.AreEqual(2, dict.Count);
        }

        [TestMethod]
        public void SimpleAddTest() {
            IDictionary<int, int> dict = new HashDictionary<int, int>();
            dict.Add(1, 10);
            Assert.AreEqual(10, dict[1]);

            dict.Add(3, 30);
            Assert.AreEqual(10, dict[1]);
            Assert.AreEqual(30, dict[3]);

            dict.Add(2, 20);
            Assert.AreEqual(10, dict[1]);
            Assert.AreEqual(20, dict[2]);
            Assert.AreEqual(30, dict[3]);
        }

        [TestMethod]
        // [ExpectedException(typeof(ArgumentException))]
        public void AddExceptionTest() {
            IDictionary<int, int> dict = new HashDictionary<int, int>();
            dict.Add(1, 10);
            dict.Add(2, 20);
            dict[2] = 30;
            try {
                // Erwarte, dass etwas passiert. (passiert nix --> negativ test!)
                dict.Add(2, 40);
                Assert.Fail("It should noct be possible to add elemnts with the same key");
            }
            catch (ArgumentException) {
            }
        }

        [TestMethod]
        public void AddUser_UserDoesNotExist_CreatesUser() {
            // arrange

            // act

            // assert
        }
    }
}
*/