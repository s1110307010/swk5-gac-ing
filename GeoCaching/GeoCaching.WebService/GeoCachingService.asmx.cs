﻿using GeoCaching.Entities;
using GeoCaching.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace GeoCaching.WebService {
    /// <summary>
    /// Summary description for GeoCachingService
    /// </summary>
    [WebService(Namespace = "http://caching.fh-hagenberg.at")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GeoCachingService : System.Web.Services.WebService {

        private ICacheManager cacheManager = BLFactory.GetCacheManager(DataAccessType.Db);
        private IUserManager userManager = BLFactory.GetUserManager(DataAccessType.Db);
        private IRoleManager roleManager = BLFactory.GetRoleManager(DataAccessType.Db);
        private IExportManager exportManager = BLFactory.GetExportManager(DataAccessType.Db);
        private ILogbookManager logbookManager = BLFactory.GetLogbookManager(DataAccessType.Db);
        private IRatingManager ratingManager = BLFactory.GetRatingManager(DataAccessType.Db);
        private IAuthentificationManager authentificationManager = BLFactory.GetAuthenticationManager(DataAccessType.Db);

        #region Cache Methods
        [WebMethod]
        public List<Cache> GetAllCaches(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Cache> caches = cacheManager.GetAllCaches(default(string), default(string));
                if (caches != null)
                    return new List<Cache>(caches);
            }
            return null;
        }

        [WebMethod]
        public Cache GetCacheById(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetCacheById(default(string), default(string), id);
            }
            return null;
        }

        [WebMethod]
        public Cache GetCacheByName(string userName, string password, string name) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetCacheByName(default(string), default(string), name);
            }
            return null;
        }

        [WebMethod]
        public bool CreateCache(string userName, string password, Cache cache) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.CreateCache(default(string), default(string), cache);
            }
            return false;
        }

        [WebMethod]
        public bool UpdateCache(string userName, string password, Cache cache) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.UpdateCache(default(string), default(string), cache);
            }
            return false;
        }

        [WebMethod]
        public bool DeleteCache(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.DeleteCache(default(string), default(string), id);
            }
            return false;
        }

        [WebMethod]
        public List<Picture> GetPicturesOfCache(string userName, string password, int cacheId) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Picture> pictureList = cacheManager.GetPicturesOfCache(default(string), default(string), cacheId);
                if (pictureList != null)
                    return new List<Picture>(pictureList);
            }
            return null;
        }

        [WebMethod]
        public bool AddPicture(string userName, string password, Picture picture) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.AddPicture(default(string), default(string), picture);
            }
            return false;
        }

        [WebMethod]
        public bool RemovePicture(string userName, string password, int pictureId) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.RemovePicture(default(string), default(string), pictureId);
            }
            return false;
        }

        [WebMethod]
        public bool IsCacheInUse(string userName, string password, int cacheId) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.IsCacheInUse(default(string), default(string), cacheId);
            }
            return false;
        }

        [WebMethod]
        public bool Exists(string userName, string password, string cacheName) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.Exists(default(string), default(string), cacheName);
            }
            return false;
        }

        [WebMethod]
        public int GetSearchStateCountOfCache(string userName, string password, int cacheId, SearchState searchState) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetSearchStateCountOfCache(default(string), default(string), cacheId, searchState);
            }
            return default(int);
        }

        [WebMethod]
        public int GetCountOfHidedCaches(string userName, string password, int userId, CacheList cacheList) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetCountOfHidedCaches(default(string), default(string), userId, cacheList);
            }
            return default(int);
        }

        [WebMethod]
        public double GetSearchLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int searchLevel) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetSearchLevelPercentage(default(string), default(string), region, radius, from, to, searchLevel);
            }
            return default(double);
        }

        [WebMethod]
        public double GetAreaLevelPercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, int areaLevel) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetAreaLevelPercentage(default(string), default(string), region, radius, from, to, areaLevel);
            }
            return default(double);
        }

        [WebMethod]
        public double GetSizePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheSize size) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetSizePercentage(default(string), default(string), region, radius, from, to, size);
            }
            return default(double);
        }

        [WebMethod]
        public double GetTypePercentage(string userName, string password, Coordinate region, double radius, DateTime from, DateTime to, CacheType type) {
            if (ValidateUser(userName, password) != null) {
                return cacheManager.GetTypePercentage(default(string), default(string), region, radius, from, to, type);
            }
            return default(double);
        }

        [WebMethod]
        public StatisticUser[] GetCachesFoundByUser(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<User> users = userManager.GetAllUsers(userName, password);
                List<StatisticUser> result = new List<StatisticUser>();
                foreach (User user in users) {
                    StatisticUser statisticUser = new StatisticUser();
                    statisticUser.UserItem = user;
                    statisticUser.Count = logbookManager.GetCountOfCachesByState(userName, password, user.Id, new CacheList() { Caches = cacheManager.GetAllCaches(userName, password).ToList() }, SearchState.Found);
                    result.Add(statisticUser);
                }

                return result.ToArray();
            }

            return default(StatisticUser[]);
        }

        [WebMethod]
        public StatisticUser[] GetCachesHidedByUser(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<User> users = userManager.GetAllUsers(userName, password);
                List<StatisticUser> result = new List<StatisticUser>();
                foreach (User user in users) {
                    StatisticUser statisticUser = new StatisticUser();
                    statisticUser.UserItem = user;
                    statisticUser.Count = cacheManager.GetCountOfHidedCaches(userName, password, user.Id, new CacheList() { Caches = cacheManager.GetAllCaches(userName, password).ToList() });
                    result.Add(statisticUser);
                }

                return result.ToArray();
            }

            return default(StatisticUser[]);
        }

        [WebMethod]
        public StatisticGeoCache[] GetPopularCaches(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Cache> caches = cacheManager.GetAllCaches(userName, password);
                List<StatisticGeoCache> result = new List<StatisticGeoCache>();
                foreach (Cache cache in caches) {
                    StatisticGeoCache geoCache = new StatisticGeoCache();
                    geoCache.CacheItem = cache;
                    geoCache.Count = cacheManager.GetSearchStateCountOfCache(userName, password, cache.Id, SearchState.Found);
                    result.Add(geoCache);
                }

                return result.ToArray();
            }

            return default(StatisticGeoCache[]);
        }

        [WebMethod]
        public StatisticUser[] GetFilteredCachesFoundByUser(string userName, string password, Coordinate region, double radius) {
            if (ValidateUser(userName, password) != null) {
                List<StatisticUser> result = new List<StatisticUser>();
                IEnumerable<User> users = userManager.GetAllUsers(userName, password);
                IEnumerable<Cache> caches = cacheManager.FilterCaches(userName, password, new DateTime(1900, 1, 1), DateTime.Now, null, null, null, null, region, radius, null, null);
                if (caches != null) {
                    CacheList cacheList = new CacheList() { Caches = caches.ToList() };
                    foreach (User user in users) {
                        StatisticUser statisticUser = new StatisticUser();
                        statisticUser.UserItem = user;
                        statisticUser.Count = logbookManager.GetCountOfCachesByState(userName, password, user.Id, cacheList, SearchState.Found);
                        result.Add(statisticUser);
                    }
                }
                return result.ToArray();
            }

            return default(StatisticUser[]);
        }

        [WebMethod]
        public StatisticUser[] GetFilteredCachesHidedByUser(string userName, string password, Coordinate region, double radius) {
            if (ValidateUser(userName, password) != null) {
                List<StatisticUser> result = new List<StatisticUser>();
                IEnumerable<User> users = userManager.GetAllUsers(userName, password);
                IEnumerable<Cache> caches = cacheManager.FilterCaches(userName, password, new DateTime(1900, 1, 1), DateTime.Now, null, null, null, null, region, radius, null, null);
                if (caches != null) {
                    CacheList cacheList = new CacheList() { Caches = caches.ToList() };
                    foreach (User user in users) {
                        StatisticUser statisticUser = new StatisticUser();
                        statisticUser.UserItem = user;
                        statisticUser.Count = cacheManager.GetCountOfHidedCaches(userName, password, user.Id, cacheList);
                        result.Add(statisticUser);
                    }
                }
                return result.ToArray();
            }

            return default(StatisticUser[]);
        }

        [WebMethod]
        public StatisticGeoCache[] GetFilteredPopularCaches(string userName, string password, Coordinate region, double radius) {
            if (ValidateUser(userName, password) != null) {
                List<StatisticGeoCache> result = new List<StatisticGeoCache>();
                IEnumerable<User> users = userManager.GetAllUsers(userName, password);
                IEnumerable<Cache> caches = cacheManager.FilterCaches(userName, password, new DateTime(1900, 1, 1), DateTime.Now, null, null, null, null, region, radius, null, null);
                if (caches != null) {
                    CacheList cacheList = new CacheList() { Caches = caches.ToList() };
                    foreach (Cache cache in caches) {
                        StatisticGeoCache geoCache = new StatisticGeoCache();
                        geoCache.CacheItem = cache;
                        geoCache.Count = cacheManager.GetSearchStateCountOfCache(userName, password, cache.Id, SearchState.Found);
                        result.Add(geoCache);
                    }
                }
                return result.ToArray();
            }

            return default(StatisticGeoCache[]);
        }

        [WebMethod]
        public List<Cache> FilterCaches(string userName, string password,
            DateTime from, DateTime to,
            CacheTypeList cacheTypes,
            CacheSizeList cacheSizes,
            AreaLevelList areaLevels,
            SearchLevelList searchLevels,
            Coordinate region,
            double radius,
            RatingList ratings,
            SearchStateList searchStates) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Cache> caches = cacheManager.FilterCaches(default(string), default(string), from, to, cacheTypes, cacheSizes, areaLevels, searchLevels, region, radius, ratings, searchStates);
                if (caches != null)
                    return new List<Cache>(caches);
            }
            return null;
        }
        #endregion

        #region User Methods
        [WebMethod]
        public User GetUserById(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return userManager.GetUserById(default(string), default(string), id);
            }
            return null;
        }

        [WebMethod]
        public User GetUserByName(string userName, string password, string userNameToGet) {
            if (ValidateUser(userName, password) != null) {
                return userManager.GetUserByName(default(string), default(string), userNameToGet);
            }
            return null;
        }

        [WebMethod]
        public List<User> GetAllUsers(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<User> users = userManager.GetAllUsers(default(string), default(string));
                if (users != null)
                    return new List<User>(users);
            }
            return null;
        }

        [WebMethod]
        public bool Create(string userName, string password, User user) {
            if (ValidateUser(userName, password) != null) {
                return userManager.Create(default(string), default(string), user);
            }
            return false;
        }

        [WebMethod]
        public bool Update(string userName, string password, User user) {
            if (ValidateUser(userName, password) != null) {
                return userManager.Update(default(string), default(string), user);
            }
            return false;
        }

        [WebMethod]
        public bool Delete(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return userManager.Delete(default(string), default(string), id);
            }
            return false;
        }

        [WebMethod]
        public bool ExistsUser(string userName, string password, string userNameToGet) {
            if (ValidateUser(userName, password) != null) {
                return userManager.ExistsUser(default(string), default(string), userNameToGet);
            }
            return false;
        }

        [WebMethod]
        public bool IsEmailAdressInUse(string userName, string password, string email, int userId) {
            if (ValidateUser(userName, password) != null) {
                return userManager.IsEmailAdressInUse(default(string), default(string), email, userId);
            }
            return false;
        }

        [WebMethod]
        public bool Reactivate(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return userManager.Reactivate(default(string), default(string), id);
            }
            return false;
        }
        #endregion

        #region Role Methods
        [WebMethod]
        public List<Role> GetAllRoles(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Role> roles = roleManager.GetAllRoles(default(string), default(string));
                if (roles != null)
                    return new List<Role>(roles);
            }
            return null;
        }

        [WebMethod]
        public Role GetRoleById(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return roleManager.GetRoleById(default(string), default(string), id);
            }
            return null;
        }

        [WebMethod]
        public Role GetRoleByName(string userName, string password, string name) {
            if (ValidateUser(userName, password) != null) {
                return roleManager.GetRoleByName(default(string), default(string), name);
            }
            return null;
        }

        [WebMethod]
        public void AssignRolesToUser(string userName, string password, int userId, RoleList roles) {
            if (ValidateUser(userName, password) != null) {
                roleManager.AssignRolesToUser(default(string), default(string), userId, roles);
            }
        }

        [WebMethod]
        public void RemoveRolesFromUser(string userName, string password, int userId, Entities.RoleList roles) {
            if (ValidateUser(userName, password) != null) {
                roleManager.RemoveRolesFromUser(default(string), default(string), userId, roles);
            }
        }

        [WebMethod]
        public List<Role> GetRolesOfUser(string userName, string password, int userId) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Role> roles = roleManager.GetRolesOfUser(default(string), default(string), userId);
                if (roles != null)
                    return new List<Role>(roles);
            }
            return null;
        }

        [WebMethod]
        public bool IsUserHider(string userName, string password, int userId) {
            if (ValidateUser(userName, password) != null) {
                return roleManager.IsUserHider(default(string), default(string), userId);
            }
            return false;
        }

        [WebMethod]
        public bool IsUserFinder(string userName, string password, int userId) {
            if (ValidateUser(userName, password) != null) {
                return roleManager.IsUserFinder(default(string), default(string), userId);
            }
            return false;
        }
        #endregion

        #region Export Methods
        [WebMethod]
        public byte[] ExportCachesToWord(string userName, string password, CacheList caches) {
            if (ValidateUser(userName, password) != null) {
                return exportManager.ExportCachesToWord(default(string), default(string), caches);
            }
            return null;
        }
        #endregion

        #region Logbook Methods
        [WebMethod]
        public LogBook GetLogBook(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return logbookManager.GetLogBook(default(string), default(string), id);
            }
            return null;
        }

        [WebMethod]
        public List<LogBook> GetAllLogBookEntriesOfCache(string userName, string password, int cacheId) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<LogBook> logbooks = logbookManager.GetAllLogBookEntriesOfCache(default(string), default(string), cacheId);
                if (logbooks != null)
                    return new List<LogBook>(logbooks);
            }
            return null;
        }

        [WebMethod]
        public int GetCountOfCachesByState(string userName, string password, int userId, CacheList cacheList, SearchState state) {
            if (ValidateUser(userName, password) != null) {
                return logbookManager.GetCountOfCachesByState(default(string), default(string), userId, cacheList, state);
            }
            return default(int);
        }

        [WebMethod]
        public List<LogBook> GetAllLogBooks(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<LogBook> logbooks = logbookManager.GetAllLogBooks(default(string), default(string));
                if (logbooks != null)
                    return new List<LogBook>(logbooks);
            }
            return null;
        }

        [WebMethod]
        public bool CreateLogBook(string userName, string password, LogBook logBook) {
            if (ValidateUser(userName, password) != null) {
                return logbookManager.CreateLogBook(default(string), default(string), logBook);
            }
            return false;
        }

        [WebMethod]
        public bool DeleteLogBook(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return logbookManager.DeleteLogBook(default(string), default(string), id);
            }
            return false;
        }
        #endregion

        #region Rating Methods
        [WebMethod]
        public Rating GetRating(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return ratingManager.GetRating(default(string), default(string), id);
            }
            return null;
        }

        [WebMethod]
        public List<Rating> GetAllRatingsOfCache(string userName, string password, int cacheId) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Rating> ratings = ratingManager.GetAllRatingsOfCache(default(string), default(string), cacheId);
                if (ratings != null)
                    return new List<Rating>(ratings);
            }
            return null;
        }

        [WebMethod]
        public List<Rating> GetAllRatings(string userName, string password) {
            if (ValidateUser(userName, password) != null) {
                IEnumerable<Rating> ratings = ratingManager.GetAllRatings(default(string), default(string));
                if (ratings != null)
                    return new List<Rating>(ratings);
            }
            return null;
        }

        [WebMethod]
        public bool CreateRating(string userName, string password, Rating rating) {
            if (ValidateUser(userName, password) != null) {
                return ratingManager.CreateRating(default(string), default(string), rating);
            }
            return false;
        }

        [WebMethod]
        public bool DeleteRating(string userName, string password, int id) {
            if (ValidateUser(userName, password) != null) {
                return ratingManager.DeleteRating(default(string), default(string), id);
            }
            return false;
        }
        #endregion

        [WebMethod]
        #region Authentification Methods
        public User ValidateUser(string userName, string password) {
            return authentificationManager.ValidateUser(userName, password);
        }
        #endregion
    }
}
