﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface ILogBookDao : IDao<LogBook> {
		/// <summary>
		/// Gets all log book entries assigned to the specified cache.
		/// </summary>
		/// <param name="cacheId">The cache to get the log book entries for.</param>
		/// <returns>Returns all assigned log book entries.</returns>
		IEnumerable<LogBook> GetAllLogBookEntriesOfCache(int cacheId);

        /// <summary>
        /// Get count of Caches of an user by state
        /// </summary>
        /// <param name="userId">The user to get count of entries</param>
        /// <param name="cacheList">The list of selected caches</param>
        /// <param name="state">The state of the searched logbook entries</param>
        /// <returns></returns>
        int GetCountOfCachesByState(int userId, IEnumerable<int> cacheList, SearchState searchStateId);
	}
}
