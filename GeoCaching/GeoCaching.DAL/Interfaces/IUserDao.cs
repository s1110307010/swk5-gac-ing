﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IUserDao : IDao<User> {
		/// <summary>
		/// Gets the user specified by the given username.
		/// </summary>
		/// <param name="username">The username of the user to load.</param>
		/// <returns>Returns the user object if username exists, otherwise null.</returns>
		User GetByUserName(string username);

        /// <summary>
        /// Reactivate a deleted user with the given id.
        /// </summary>
        /// <param name="id">The id of the entity to reactivate.</param>
        /// <returns>Returns whether the entity was reactivated successfully.</returns>
        bool Reactivate(int id);

		/// <summary>
		/// Gets whether a user with the given username exists already.
		/// </summary>
		/// <param name="username">The username to check for.</param>
		/// <returns>Returns true, if a user with given username exists already.</returns>
		bool Exists(string username);

		/// <summary>
		/// Gets whether a given email address is used for a given user.
		/// </summary>
		/// <param name="email">The email address to check.</param>
		/// <param name="userId">The user to check the email address for.</param>
		/// <returns>Returns true if the email address is used for the given user, false otherwise.</returns>
		bool IsEmailAdressInUse(string email, int userId);
	}
}