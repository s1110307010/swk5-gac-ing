﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface ICacheDao : IDao<Cache> {
		/// <summary>
		/// Gets all pictures of a certain cache.
		/// </summary>
		/// <param name="cacheId">The cache to get the assigned pictures for.</param>
		/// <returns>Returns all pictures assigned to the specified cache.</returns>
		IEnumerable<Picture> GetPicturesOfCache(int cacheId);

        /// <summary>
        /// Gets the cache specified by the name.
        /// </summary>
        /// <param name="name">The name of the cache to get.</param>
        /// <returns>Returns the cache with the specified id.</returns>
        Cache GetCacheByName(string name);

        /// <summary>
		/// Adds the given picture.
		/// </summary>
		/// <param name="picture">The picture to add.</param>
		/// <returns>Returns whether the picture was sucessfully added.</returns>
		bool AddPicture(Picture picture);

		/// <summary>
		/// Removes the picture with the given id.
		/// </summary>
		/// <param name="pictureId">The picture to remove.</param>
		/// <returns>Returns whether the picture was successfully removed.</returns>
		bool RemovePicture(int pictureId);

		/// <summary>
		/// Gets whether there is at least one log book entry or rating assigned to the cache.
		/// </summary>
		/// <param name="cacheId">The cache to check whether it is in use.</param>
		/// <returns>Returns true if the cache is in use, false otherwise.</returns>
		bool IsCacheInUse(int cacheId);

		/// <summary>
		/// Gets whether a cache with the given name exists already.
		/// </summary>
		/// <param name="cacheName">The name of the cache to check.</param>
		/// <returns>Returns true, if a cache with the given name exists, false otherwise.</returns>
		bool Exists(string cacheName);

		/// <summary>
		/// Allows to filter the caches according to the given criteria.
		/// The different criteria are combined with AND. The values of a single criteria are combined with OR.
		/// </summary>
		/// <param name="from">The earliest creation date of a cache.</param>
		/// <param name="to">The latest creation date of a cache.</param>
		/// <param name="cacheTypes">The possible cache types a cache may have assigned.</param>
		/// <param name="cacheSizes">The possible cache sizes a cache may have assigned.</param>
		/// <param name="areaLevels">The possible area levels a cache may have assigned.</param>
		/// <param name="searchLevels">The possible search levels a cache may have assigned.</param>
		/// <param name="region">The region the cache has to be in.</param>
		/// <param name="ratings">The possible ratings a cache may have assigned.</param>
		/// <param name="searchStates">The possible search states the cache may have assigned.</param>
		/// <returns>Returns the filtered caches.</returns>
		IEnumerable<Cache> FilterCaches(
			DateTime from, DateTime to,
			IEnumerable<CacheType> cacheTypes,
			IEnumerable<CacheSize> cacheSizes,
			IEnumerable<int> areaLevels,
			IEnumerable<int> searchLevels,
			Coordinate region,
            double radius,
			IEnumerable<int> ratings,
			IEnumerable<SearchState> searchStates);

		/// <summary>
		/// Gets how often the cache with the given id was found or not found.
		/// </summary>
		/// <param name="cacheId">The cache to check the count for.</param>
		/// <param name="searchState">The search state to check for.</param>
		/// <returns>Returns how often the cache was found or not found.</returns>
		int GetSearchStateCountOfCache(int cacheId, SearchState searchState);

        /// <summary>
        /// Get count of hided caches from an user
        /// </summary>
        /// <param name="userId">User to get the cound</param>
        /// <param name="cacheList">List of filtered Caches for count</param>
        /// <returns>Count of hided caches</returns>
        int GetCountOfHidedCaches(int userId, IEnumerable<int> caches);

		/// <summary>
		/// Gets the percentage of all caches with given search level in relation to all caches according to the given criteria.
		/// </summary>
		/// <param name="region">The region the cache has to be in.</param>
        /// <param name="radius">The radius arround the selected cache</param>
		/// <param name="from">The earliest creation date of the caches.</param>
		/// <param name="to">The latest creation date of the caches.</param>
		/// <param name="searchLevel">The search level to get the percentage of.</param>
		/// <returns>Returns the percentage of the given search level in relation to the number of all caches.</returns>
        double GetSearchLevelPercentage(Coordinate region, double radius, DateTime from, DateTime to, int searchLevel);

		/// <summary>
		/// Gets the percentage of all caches with given area level in relation to all caches according to the given criteria.
		/// </summary>
		/// <param name="region">The region the cache has to be in.</param>
        /// <param name="radius">The radius arround the selected cache</param>
        /// <param name="from">The earliest creation date of the caches.</param>
		/// <param name="to">The latest creation date of the caches.</param>
		/// <param name="areaLevel">The area level to get the percentage of.</param>
		/// <returns>Returns the percentage of the given area level in relation to the number of all caches.</returns>
        double GetAreaLevelPercentage(Coordinate region, double radius, DateTime from, DateTime to, int areaLevel);

		/// <summary>
		/// Gets the percentage of all caches with given size in relation to all caches according to the given criteria.
		/// </summary>
		/// <param name="region">The region the cache has to be in.</param>
        /// <param name="radius">The radius arround the selected cache</param>
        /// <param name="from">The earliest creation date of the caches.</param>
		/// <param name="to">The latest creation date of the caches.</param>
		/// <param name="size">The size to get the percentage of.</param>
		/// <returns>Returns the percentage of the given size in relation to the number of all caches.</returns>
        double GetSizePercentage(Coordinate region, double radius, DateTime from, DateTime to, int size);

		/// <summary>
		/// Gets the percentage of all caches with given type in relation to all caches according to the given criteria.
		/// </summary>
		/// <param name="region">The region the cache has to be in.</param>
        /// <param name="radius">The radius arround the selected cache</param>
        /// <param name="from">The earliest creation date of the caches.</param>
		/// <param name="to">The latest creation date of the caches.</param>
		/// <param name="type">The type to get the percentage of.</param>
		/// <returns>Returns the percentage of the given type in relation to the number of all caches.</returns>
        double GetTypePercentage(Coordinate region, double radius, DateTime from, DateTime to, int type);
    }
}