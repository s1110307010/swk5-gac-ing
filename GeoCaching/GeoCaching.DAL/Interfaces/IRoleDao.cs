﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IRoleDao {
		#region GetAllRoles
		/// <summary>
		/// Gets all existing roles.
		/// </summary>
		/// <returns>Returns all roles.</returns>
		IEnumerable<Role> GetAllRoles();
		#endregion

        #region GetRoleById
		/// <summary>
		/// Gets a role by its id.
		/// </summary>
		/// <param name="id">The id of the role to get.</param>
		/// <returns>Returns the role specified by the id.</returns>
        Role GetRoleById(int id);
        #endregion

        #region GetRoleByName
		/// <summary>
		/// Gets a role by its name.
		/// </summary>
		/// <param name="name">The name of the role to get.</param>
		/// <returns>Returns the role specified by the name.</returns>
        Role GetRoleByName(string name);
        #endregion

		#region AssignRolesToUser
		/// <summary>
		/// Assignes the given roles to the given user.
		/// </summary>
		/// <param name="userId">The user to assign the roles to.</param>
		/// <param name="roles">The roles to assign to the user.</param>
		void AssignRolesToUser(int userId, IEnumerable<Role> roles);
		#endregion

		#region RemoveRolesFromUser
		/// <summary>
		/// Removes the given roles from the specified user.
		/// </summary>
		/// <param name="userId">The user to remove the roles from.</param>
		/// <param name="roles">The roles to remove.</param>
		void RemoveRolesFromUser(int userId, IEnumerable<Role> roles);
		#endregion

		#region GetRolesOfUser
		/// <summary>
		/// Gets all roles which are assigned to the specified user.
		/// </summary>
		/// <param name="userId">The user to get the roles for.</param>
		/// <returns>Returns all roles of the user.</returns>
		IEnumerable<Role> GetRolesOfUser(int userId);
		#endregion
	}
}