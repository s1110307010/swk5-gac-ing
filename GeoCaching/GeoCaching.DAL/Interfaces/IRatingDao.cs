﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IRatingDao : IDao<Rating> {
		/// <summary>
		/// Gets all ratings assigned to the specified cache.
		/// </summary>
		/// <param name="cacheId">The cache id to get the assigned ratings for.</param>
		/// <returns>Returns all assigned rating objects.</returns>
		IEnumerable<Rating> GetAllRatingsOfCache(int cacheId);
	}
}
