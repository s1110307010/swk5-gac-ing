﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public interface IDao<TEntity> where TEntity : IEntity {
		/// <summary>
		/// Gets the specified entity by id.
		/// </summary>
		/// <param name="id">The id of the entity to get.</param>
		/// <returns>Returns the whole entity.</returns>
		TEntity Get(int id);

		/// <summary>
		/// Gets all entities of type TEntity.
		/// </summary>
		/// <returns></returns>
		IEnumerable<TEntity> GetAll();

		/// <summary>
		/// Creates the given entity.
		/// </summary>
		/// <param name="entity">The entity to create.</param>
		/// <returns>Returns whether the entity was created successfully.</returns>
		bool Create(TEntity entity);

		/// <summary>
		/// Updates the given entity.
		/// </summary>
		/// <param name="entity">The entity to update.</param>
		/// <returns>Returns whether the entity was updated successfully.</returns>
		bool Update(TEntity entity);

		/// <summary>
		/// Deletes the entity with the given id.
		/// </summary>
		/// <param name="id">The id of the entity to delete.</param>
		/// <returns>Returns whether the entity was deleted successfully.</returns>
		bool Delete(int id);

        /// <summary>
        /// Get Count of entries
        /// </summary>
        /// <returns>Returns the count of entries</returns>
        int CountEntries();
	}
}