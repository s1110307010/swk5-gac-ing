﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using Microsoft.SqlServer.Types;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class DALUtils {
		#region CreateParameter
		public static IDbDataParameter CreateParameter(string parameterName, object parameterValue, IDbCommand command) {
			if (String.IsNullOrEmpty(parameterName))
				throw new ArgumentException("Parameter parameterName must not be null or empty!");
			if (parameterValue == null)
				throw new ArgumentNullException("parameterValue");
			if (command == null)
				throw new ArgumentNullException("command");

			IDbDataParameter param = command.CreateParameter();
			param.ParameterName = parameterName;
			param.Value = parameterValue;
			return param;
		}
		#endregion

		#region BuildGeographyCoordinates
		public static SqlGeography BuildGeographyCoordinates(Coordinate coordinates) {
			SqlGeographyBuilder builder = new SqlGeographyBuilder();
			builder.SetSrid(coordinates.SpatialReferenceId);
			builder.BeginGeography(OpenGisGeographyType.Point);
			builder.BeginFigure(coordinates.Latitude, coordinates.Longitude);
			builder.EndFigure();
			builder.EndGeography();
			SqlGeography geo = builder.ConstructedGeography;
			return geo;
		}
		#endregion
	}
}