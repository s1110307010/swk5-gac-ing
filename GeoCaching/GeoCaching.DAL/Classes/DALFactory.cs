﻿namespace GeoCaching.DAL {
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Data;
	using System.Data.SqlClient;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public static class DALFactory {
		#region GetConnection
		/// <summary>
		/// Returns an open connection to the database.
		/// </summary>
		/// <returns>Returns the connection object.</returns>
		public static IDbConnection GetConnection() {
			IDbConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString);
			connection.Open();
			return connection;
		}
		#endregion

		#region GetCacheDao
		/// <summary>
		/// Returns an implementation of ICacheDao.
		/// </summary>
		/// <returns>The cache dao.</returns>
		public static ICacheDao GetCacheDao() {
			return new CacheDao();
		}
		#endregion

		#region GetUserDao
		public static IUserDao GetUserDao() {
			return new UserDao();
		}
		#endregion

		#region GetRatingDao
		public static IRatingDao GetRatingDao() {
			return new RatingDao();
		}
		#endregion

		#region GetLogBookDao
		public static ILogBookDao GetLogBookDao() {
			return new LogBookDao();
		}
		#endregion

		#region GetRoleDao
		public static IRoleDao GetRoleDao() {
			return new RoleDao();
		}
		#endregion
	}
}