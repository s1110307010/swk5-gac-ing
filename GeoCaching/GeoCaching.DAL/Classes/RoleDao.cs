﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class RoleDao : IRoleDao {
		#region Constants
		const string SELECT_ALL_ROLES = @"SELECT [Id], [Name] FROM [dbo].[Role]";
        const string SELECT_ROLE_BY_ID = @"SELECT [Id], [Name] FROM [dbo].[Role] WHERE [Id] = @id";
        const string SELECT_ROLE_BY_NAME = @"SELECT [Id], [Name] FROM [dbo].[Role] WHERE [Name] = @name";
        const string SELECT_USER_ROLES = @"SELECT [RoleId], [Name] FROM [dbo].[User_Role] ur INNER JOIN [dbo].[Role] r ON ur.[roleId] = r.[id] WHERE [UserId] = @userId";
        const string ASSIGN_ROLE_TO_USER = @"INSERT INTO [dbo].[User_Role] ([UserId], [RoleId]) VALUES (@userId, @roleId)";
		const string REMOVE_ROLE_FROM_USER = @"DELETE FROM [dbo].[User_Role] WHERE [UserId] = @userId AND [RoleId] = @roleId";
		#endregion

		#region IRoleDao Members
		public IEnumerable<Entities.Role> GetAllRoles() {
			List<Role> roles = null;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_ALL_ROLES;

				IDataReader reader = command.ExecuteReader();
				while (reader.Read()) {
					if (roles == null)
						roles = new List<Role>();

					Role role = new Role();
					role.Id = reader.GetInt32(0);
					role.Name = reader.GetString(1).Trim();
					roles.Add(role);
				}
			}

			return roles;
		}

        public Role GetRoleById(int id) {
            Role role = null;
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = SELECT_ROLE_BY_ID;
                
                command.Parameters.Add(DALUtils.CreateParameter("@id", id, command));
//                IDbDataParameter param = command.CreateParameter();
//                param.ParameterName = "@roleId";
//                command.Parameters.Add(param);

                IDataReader reader = command.ExecuteReader();
                while (reader.Read()) {
                    role = new Role();
                    role.Id = reader.GetInt32(0);
                    role.Name = reader.GetString(1).Trim();
                }
            }

            return role;
        }

        public Role GetRoleByName(string name) {
            Role role = null;
            using (IDbConnection connection = DALFactory.GetConnection()) {
                IDbCommand command = connection.CreateCommand();
                command.CommandText = SELECT_ROLE_BY_NAME;

                command.Parameters.Add(DALUtils.CreateParameter("@name", name, command));
//                IDbDataParameter param = command.CreateParameter();
//                param.ParameterName = "@nameId";
//                command.Parameters.Add(param);

                IDataReader reader = command.ExecuteReader();
                while (reader.Read()) {
                    role = new Role();
                    role.Id = reader.GetInt32(0);
                    role.Name = reader.GetString(1).Trim();
                }
            }

            return role;
        }

		public void AssignRolesToUser(int userId, IEnumerable<Entities.Role> roles) {
			if (roles == null || roles.Count() == 0)
				return;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = ASSIGN_ROLE_TO_USER;
				command.Parameters.Add(DALUtils.CreateParameter("@userId", userId, command));
				IDbDataParameter param = command.CreateParameter();
				param.ParameterName = "@roleId";
				command.Parameters.Add(param);

				foreach (Role role in roles) {
					param.Value = role.Id;
					command.ExecuteNonQuery();
				}
			}
		}

		public void RemoveRolesFromUser(int userId, IEnumerable<Entities.Role> roles) {
			if (roles == null || roles.Count() == 0)
				return;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = REMOVE_ROLE_FROM_USER;
				command.Parameters.Add(DALUtils.CreateParameter("@userId", userId, command));
				IDbDataParameter param = command.CreateParameter();
				param.ParameterName = "@roleId";
				command.Parameters.Add(param);

				foreach (Role role in roles) {
					param.Value = role.Id;
					command.ExecuteNonQuery();
				}
			}
		}

		public IEnumerable<Entities.Role> GetRolesOfUser(int userId) {
			List<Role> roles = null;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_USER_ROLES;
				command.Parameters.Add(DALUtils.CreateParameter("@userId", userId, command));

				IDataReader reader = command.ExecuteReader();
				while (reader.Read()) {
					if (roles == null)
						roles = new List<Role>();
					
					Role role = new Role();
					role.Id = reader.GetInt32(0);
					role.Name = reader.GetString(1);
					roles.Add(role);
				}
			}
			return roles;
		}
		#endregion
	}
}