﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using Microsoft.SqlServer.Types;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.SqlClient;
	using System.Data.SqlTypes;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class CacheDao : ICacheDao {
		#region Constants
		const string SELECT_CACHE = "SELECT [Id], [UserId], [SizeId], [CacheStateId], [CacheTypeId], [Name], [Puzzle], [Coordinate], [SearchLevel], [AreaLevel], [CreateDate] FROM [dbo].[Cache]";
		const string DELETE_CACHE = "DELETE FROM [dbo].[Cache] WHERE [Id] = @id";
		const string CREATE_CACHE = @"INSERT INTO [dbo].[Cache] ([UserId], [SizeId], [CacheStateId], [CacheTypeId], [Name], [Puzzle], [Coordinate], [SearchLevel], [AreaLevel], [CreateDate]) 
									  VALUES (@userId, @sizeId, @cacheStateId, @cacheTypeId, @name, @puzzle, @coordinate, @searchLevel, @areaLevel, @creationDate)";
		const string UPDATE_CACHE = @"UPDATE [dbo].[Cache] SET [UserId] = @userId, [SizeId] = @sizeId, [CacheStateId] = @cacheStateId, [CacheTypeId] = @cacheTypeId, [Name] = @name, [Puzzle] = @puzzle, [Coordinate] = @coordinate,
									  [SearchLevel] = @searchLevel, [AreaLevel] = @areaLevel, [CreateDate] = @creationDate WHERE [Id] = @id";
		const string SELECT_COUNT_CACHE = "SELECT count([Id]) FROM [dbo].[Cache]";
		const string EXISTS_CACHE = "SELECT COUNT(*) FROM [dbo].[Cache] WHERE [Name] = @name";

		const string IS_CACHE_IN_USE = @"	SELECT SUM(cnt) FROM (
												SELECT COUNT(*) AS cnt FROM [dbo].[Rate] WHERE [cacheId] = @cacheId
												UNION ALL
												SELECT COUNT(*) AS cnt FROM [dbo].[LogBook] WHERE [cacheId] = @cacheId
											) help";

		const string SELECT_PICTURES_OF_CACHE = @"SELECT [Id], [CacheId], [Picture] FROM [dbo].[Picture] WHERE [CacheId] = @cacheId";
		const string DELETE_PICTURE = "DELETE FROM [dbo].[Picture] WHERE [Id] = @id";
		const string CREATE_PICTURE = "INSERT INTO [dbo].[Picture] ([CacheId], [Picture]) VALUES (@cacheId, @data)";

		const string GET_SEARCH_STATE_COUNT = "SELECT COUNT(*) FROM [dbo].[Logbook] WHERE [CacheId] = @cacheId AND [SearchStateId] = @searchStateId";

		const string PERCENTAGE = @"
			DECLARE @c INT;
			SET @c = (SELECT COUNT(*) FROM [dbo].[Cache] WHERE [createDate] BETWEEN @from AND @to)
			IF @c > 0
				BEGIN
					SELECT 100.0 / @c * (SELECT COUNT(*) FROM [dbo].[Cache] WHERE [{0}] = @value AND [createDate] BETWEEN @from AND @to 
                    AND [coordinate].[Lat]  BETWEEN @coorLat  - @radius AND @coorLat  + @radius
                    AND [coordinate].[Long] BETWEEN @coorLong - @radius AND @coorLong + @radius)
				END
			ELSE 
				BEGIN
					SELECT 0.0
				END";
		#endregion

		#region IDao<Cache> Members
		public Entities.Cache Get(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_CACHE + " WHERE [Id] = @id";

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@id", id, command));

				IDataReader reader = command.ExecuteReader();

				Cache cache = null;
				if (reader.Read()) {
					cache = new Cache();
					FillCache(cache, reader);
				}

				return cache;
			}
		}

		public IEnumerable<Cache> GetAll() {
			IList<Cache> caches = null;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_CACHE;

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (caches == null)
						caches = new List<Cache>();

					Cache cache = new Cache();
					FillCache(cache, reader);
					caches.Add(cache);
				}
			}

			return caches;

		}

		public bool Create(Entities.Cache entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = CREATE_CACHE;
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Update(Entities.Cache entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = UPDATE_CACHE;
				command.Parameters.Add(DALUtils.CreateParameter("@id", entity.Id, command));
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Delete(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = DELETE_CACHE;

				IDbDataParameter idParam = command.CreateParameter();
				idParam.ParameterName = "@id";
				idParam.Value = id;
				command.Parameters.Add(idParam);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public int CountEntries() {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_COUNT_CACHE;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;

				IDataReader reader = command.ExecuteReader();

				int result = 0;
				if (reader.Read()) {
					result = reader.GetInt32(0);
				}

				return result;
			}
		}
		#endregion

		#region ICacheDao Members
        public Entities.Cache GetCacheByName(string name) {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                string select = SELECT_CACHE + " WHERE [Name] = @name";

                IDbCommand command = connection.CreateCommand();
                command.CommandText = select;
                command.Parameters.Add(DALUtils.CreateParameter("@name", name, command));

                IDataReader reader = command.ExecuteReader();

                Cache cache = null;
                if (reader.Read()) {
                    cache = new Cache();
                    FillCache(cache, reader);
                }

                return cache;
            }
        }

        public double GetSearchLevelPercentage(Coordinate region, double radius, DateTime from, DateTime to, int searchLevel) {
			return this.GetCachePercentage(region, radius, from, to, "searchLevel", searchLevel);
		}

        public double GetAreaLevelPercentage(Coordinate region, double radius, DateTime from, DateTime to, int areaLevel) {
            return this.GetCachePercentage(region, radius, from, to, "areaLevel", areaLevel);
		}

        public double GetSizePercentage(Coordinate region, double radius, DateTime from, DateTime to, int size) {
            return this.GetCachePercentage(region, radius, from, to, "sizeId", size);
		}

        public double GetTypePercentage(Coordinate region, double radius, DateTime from, DateTime to, int type) {
            return this.GetCachePercentage(region, radius, from, to, "cacheTypeId", type);
		}

		public int GetSearchStateCountOfCache(int cacheId, SearchState searchState) {
			int count = 0;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = GET_SEARCH_STATE_COUNT;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", cacheId, command));
				command.Parameters.Add(DALUtils.CreateParameter("@searchStateId", (int)searchState, command));

				object result = command.ExecuteScalar();
				if (result != null)
					count = (int)result;
			}
			return count;
		}

        public int GetCountOfHidedCaches(int userId, IEnumerable<int> cacheList) {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                string select = SELECT_COUNT_CACHE
                    + " WHERE [userId] = @userid";
                StringBuilder cacheFilter = new StringBuilder();

                cacheFilter.Append(" AND [id] in (");
                bool firstRun = true;
                foreach (int cacheId in cacheList) {
                    if (!firstRun)
                        cacheFilter.Append(",");
                    cacheFilter.Append(cacheId);
                    firstRun = false;
                }
                cacheFilter.Append(")");

                IDbCommand command = connection.CreateCommand();
                command.CommandText = select + cacheFilter.ToString();
                command.Parameters.Add(DALUtils.CreateParameter("@userid", userId, command));

                IDataReader reader = command.ExecuteReader();

                int result = 0;
                if (reader.Read()) {
                    result = reader.GetInt32(0);
                }

                return result;
            }
        }
		
		public bool Exists(string cacheName) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = EXISTS_CACHE;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@name", cacheName, command));

				IDataReader reader = command.ExecuteReader();

				int result = 0;
				if (reader.Read()) {
					result = reader.GetInt32(0);
				}

				return result != 0;
			}
		}

		#region Pictures
		public IEnumerable<Entities.Picture> GetPicturesOfCache(int cacheId) {
			IList<Picture> pictures = null;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_PICTURES_OF_CACHE;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", cacheId, command));

				IDataReader reader = command.ExecuteReader();
				while (reader.Read()) {
					if (pictures == null)
						pictures = new List<Picture>();

					Picture p = new Picture();
					p.Id = reader.GetInt32(0);
					p.CacheId = reader.GetInt32(1);
					p.ImageData = (byte[])reader.GetValue(2);
					pictures.Add(p);
				}
			}

			return pictures;
		}

		public bool AddPicture(Picture picture) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = CREATE_PICTURE;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", picture.CacheId, command));
				command.Parameters.Add(DALUtils.CreateParameter("@data", picture.ImageData, command));

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool RemovePicture(int pictureId) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = DELETE_PICTURE;
				command.Parameters.Add(DALUtils.CreateParameter("@id", pictureId, command));

				return command.ExecuteNonQuery() == 1;
			}
		}
		#endregion

		public bool IsCacheInUse(int cacheId) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = IS_CACHE_IN_USE;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", cacheId, command));

				bool res = false;
				object result = command.ExecuteScalar();
				if (result != null)
					res = (int)result != 0;

				return res;
			}
		}

		public IEnumerable<Cache> FilterCaches(
			DateTime from, DateTime to,
			IEnumerable<CacheType> cacheTypes,
			IEnumerable<CacheSize> cacheSizes,
			IEnumerable<int> areaLevels,
			IEnumerable<int> searchLevels,
			Coordinate region,
            double radius,
			IEnumerable<int> ratings,
			IEnumerable<SearchState> searchStates) {

			IList<Cache> caches = new List<Cache>();

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();

				string select = String.Format("SELECT DISTINCT c.[Id] FROM [dbo].[Cache] c ");
				StringBuilder filter = new StringBuilder();
				StringBuilder join = new StringBuilder();

				// Filter date from and date to.
				filter.AppendFormat("WHERE (c.[CreateDate] BETWEEN @from AND @to) ");
				command.Parameters.Add(DALUtils.CreateParameter("@from", from, command));
				command.Parameters.Add(DALUtils.CreateParameter("@to", to, command));

				// Filter cache types.
				if (cacheTypes != null && cacheTypes.Count() != 0) {
					filter.AppendFormat("AND (");
					foreach (CacheType type in cacheTypes) {
						filter.AppendFormat("c.[CacheTypeId] = {0} OR ", (int)type);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

				// Filter cache sizes.
				if (cacheSizes != null && cacheSizes.Count() != 0) {
					filter.AppendFormat("AND (");
					foreach (CacheSize size in cacheSizes) {
						filter.AppendFormat("c.[SizeId] = {0} OR ", (int)size);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

				// Filter area levels.
				if (areaLevels != null && areaLevels.Count() != 0) {
					filter.AppendFormat("AND (");
					foreach (int level in areaLevels) {
						filter.AppendFormat("c.[AreaLevel] = {0} OR ", level);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

				// Filter search levels.
				if (searchLevels != null && searchLevels.Count() != 0) {
					filter.AppendFormat("AND (");
					foreach (int level in searchLevels) {
						filter.AppendFormat("c.[SearchLevel] = {0} OR ", level);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

                // filter region.
                if (region != null) {
                    if (radius == 0) {
                        radius = 9999;
                    }
                    filter.AppendFormat("AND [coordinate].[Lat] BETWEEN {0} - {1} AND {0} + {1} ", region.Latitude.ToString().Replace(',', '.'), radius.ToString().Replace(',', '.'));
                    filter.AppendFormat("AND [coordinate].[Long] BETWEEN {0} - {1} AND {0} + {1} ", region.Longitude.ToString().Replace(',', '.'), radius.ToString().Replace(',', '.'));
                }

				// Filter ratings.
				if (ratings != null && ratings.Count() != 0) {
					join.AppendFormat("INNER JOIN [dbo].[Rate] r ON c.[Id] = r.[CacheId] ");
					filter.AppendFormat("AND (");
					foreach (int rate in ratings) {
						filter.AppendFormat("r.[Rate] = {0} OR ", rate);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

				// Filter search states.
				if (searchStates != null && searchStates.Count() != 0) {
					join.AppendFormat("INNER JOIN [dbo].[LogBook] lb ON c.[Id] = lb.[CacheId] ");
					filter.AppendFormat("AND (");
					foreach (SearchState state in searchStates) {
						filter.AppendFormat("lb.[SearchStateId] = {0} OR ", (int)state);
					}
					filter.Remove(filter.Length - 3, 3);
					filter.AppendFormat(") ");
				}

				command.CommandText = String.Format(
					@"	SELECT [Id], [UserId], [SizeId], [CacheStateId], [CacheTypeId], [Name], [Puzzle], [Coordinate], [SearchLevel], [AreaLevel], [CreateDate] 
						FROM [dbo].[Cache] 
						WHERE [Id] IN (
							{0}{1}{2}
						)", select, join.ToString(), filter.ToString());

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					Cache cache = new Cache();
					FillCache(cache, reader);
					caches.Add(cache);
				}

				return caches;
			}
		}
		#endregion

		#region Help Methods
		private void AddParameters(Cache entity, IDbCommand command) {
			command.Parameters.Add(DALUtils.CreateParameter("@userId", entity.OwnerId, command));
			command.Parameters.Add(DALUtils.CreateParameter("@sizeId", (Int32)entity.Size, command));
			command.Parameters.Add(DALUtils.CreateParameter("@cacheStateId", (Int32)entity.State, command));
			command.Parameters.Add(DALUtils.CreateParameter("@cacheTypeId", (Int32)entity.Type, command));
			command.Parameters.Add(DALUtils.CreateParameter("@name", entity.Name, command));
			command.Parameters.Add(DALUtils.CreateParameter("@puzzle", entity.Puzzle, command));
			command.Parameters.Add(new SqlParameter("@coordinate", DALUtils.BuildGeographyCoordinates(entity.Coordinates)) { UdtTypeName = "Geography" });
			command.Parameters.Add(DALUtils.CreateParameter("@searchLevel", entity.SearchLevel, command));
			command.Parameters.Add(DALUtils.CreateParameter("@areaLevel", entity.AreaLevel, command));
			command.Parameters.Add(DALUtils.CreateParameter("@creationDate", entity.CreationDate, command));
		}

		private static void FillCache(Cache cache, IDataReader reader) {
			cache.Id = reader.GetInt32(0);
			cache.OwnerId = reader.GetInt32(1);
			cache.Size = (CacheSize)reader.GetInt32(2);
			cache.State = (CacheState)reader.GetInt32(3);
			cache.Type = (CacheType)reader.GetInt32(4);
			cache.Name = reader.GetString(5);
			cache.Puzzle = reader.GetString(6);
			SqlGeography geo = SqlGeography.Deserialize(((SqlDataReader)reader).GetSqlBytes(7));
			cache.Coordinates = new Coordinate() { Latitude = (double)geo.Lat, Longitude = (double)geo.Long, SpatialReferenceId = (Int32)geo.STSrid };
			cache.SearchLevel = reader.GetByte(8);
			cache.AreaLevel = reader.GetByte(9);
			cache.CreationDate = reader.GetDateTime(10);
		}

        private double GetCachePercentage(Coordinate region, double radius, DateTime from, DateTime to, string columnName, int value) {
			double count = 0.0;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = String.Format(PERCENTAGE, columnName);
                if (radius == 0) {
                    radius = 9999;
                }
                if (region == null) {
                    command.Parameters.Add(DALUtils.CreateParameter("@coorLat", 0, command));
                    command.Parameters.Add(DALUtils.CreateParameter("@coorLong", 0, command));
                }
                else {
                    command.Parameters.Add(DALUtils.CreateParameter("@coorLat", region.Latitude, command));
                    command.Parameters.Add(DALUtils.CreateParameter("@coorLong", region.Longitude, command));
                }
                command.Parameters.Add(DALUtils.CreateParameter("@radius", radius, command));
                command.Parameters.Add(DALUtils.CreateParameter("@value", value, command));
                command.Parameters.Add(DALUtils.CreateParameter("@from", from, command));
				command.Parameters.Add(DALUtils.CreateParameter("@to", to, command));

				object result = command.ExecuteScalar();
				if (result != null)
					count = Convert.ToDouble(result);
			}
			return count;
		}
		#endregion
    }
}
