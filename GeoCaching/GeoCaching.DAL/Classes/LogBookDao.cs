﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class LogBookDao : ILogBookDao {
		#region Constants
		const string SELECT_LOGBOOK = "SELECT [Id], [UserId], [CacheId], [SearchStateId], [Comment], [CreateDate] FROM [dbo].[LogBook]";
		const string DELETE_LOGBOOK = "DELETE FROM [dbo].[LogBook] WHERE [Id] = @id";
		const string CREATE_LOGBOOK = @"INSERT INTO [dbo].[LogBook] ([UserId], [CacheId], [SearchStateId], [Comment]) 
									  VALUES (@userId, @cacheId, @searchState, @comment)";
		const string UPDATE_LOGBOOK = @"UPDATE [dbo].[LogBook] SET [UserId] = @userId, [CacheId] = @cacheId, [SearchStateId] = @searchState, [Comment] = @comment, [CreateDate] = @creationDate WHERE [Id] = @id";
		const string SELECT_COUNT_LOGBOOK = "SELECT count([Id]) FROM [dbo].[LogBook]";
		const string SELECT_LOGBOOK_BY_CACHEID = "SELECT [Id], [UserId], [CacheId], [SearchStateId], [Comment], [CreateDate] FROM [dbo].[LogBook] WHERE [CacheId] = @cacheId";
		#endregion

		#region ILogBookDao
		public IEnumerable<LogBook> GetAllLogBookEntriesOfCache(int cacheId) {
			IList<LogBook> logBooks = null;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_LOGBOOK_BY_CACHEID;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", cacheId, command));

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (logBooks == null)
						logBooks = new List<LogBook>();

					LogBook logBook = new LogBook();
					logBook.Id = reader.GetInt32(0);
					logBook.UserId = reader.GetInt32(1);
					logBook.CacheId = reader.GetInt32(2);
					logBook.SearchState = (SearchState)reader.GetInt32(3);
					logBook.Comment = reader.GetString(4);
					logBook.CreationDate = reader.GetDateTime(5);
					logBooks.Add(logBook);
				}
			}
			return logBooks;
		}

        public int GetCountOfCachesByState(int userId, IEnumerable<int> cacheList, SearchState search) {
            using (IDbConnection connection = DALFactory.GetConnection()) {
                string select = SELECT_COUNT_LOGBOOK 
                    + " WHERE [userId] = @userid"
                    + " AND [searchStateId] = @searchstateid";
                StringBuilder cacheFilter = new StringBuilder();

                cacheFilter.Append(" AND [cacheId] in (");
                bool firstRun = true;
                foreach (int cacheId in cacheList) {
                    if (!firstRun)
                        cacheFilter.Append(",");
                    cacheFilter.Append(cacheId);
                    firstRun = false;
                }
                cacheFilter.Append(")");

                IDbCommand command = connection.CreateCommand();
                command.CommandText = select + cacheFilter.ToString();
                command.Parameters.Add(DALUtils.CreateParameter("@userid", userId, command));
                command.Parameters.Add(DALUtils.CreateParameter("@searchstateid", search, command));

                IDataReader reader = command.ExecuteReader();

                int result = 0;
                if (reader.Read()) {
                    result = reader.GetInt32(0);
                }

                return result;
            }
        }
		#endregion

		#region IDao<LogBook>
		public Entities.LogBook Get(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_LOGBOOK + " WHERE [Id] = @id";

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@id", id, command));

				IDataReader reader = command.ExecuteReader();

				LogBook logBook = null;
				if (reader.Read()) {
					logBook = new LogBook();
					FillRating(logBook, reader);
				}

				return logBook;
			}
		}

		public IEnumerable<Entities.LogBook> GetAll() {
			IList<LogBook> logBooks = null;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_LOGBOOK;

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (logBooks == null)
						logBooks = new List<LogBook>();

					LogBook logBook = new LogBook();
					FillRating(logBook, reader);
					logBooks.Add(logBook);
				}
			}

			return logBooks;
		}

		public bool Create(Entities.LogBook entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = CREATE_LOGBOOK;
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Update(Entities.LogBook entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = UPDATE_LOGBOOK;
				command.Parameters.Add(DALUtils.CreateParameter("@id", entity.Id, command));
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Delete(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = DELETE_LOGBOOK;

				IDbDataParameter idParam = command.CreateParameter();
				idParam.ParameterName = "@id";
				idParam.Value = id;
				command.Parameters.Add(idParam);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public int CountEntries() {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_COUNT_LOGBOOK;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;

				IDataReader reader = command.ExecuteReader();

				int result = 0;
				if (reader.Read()) {
					result =  reader.GetInt32(0);
				}

				return result;
			}
		}
		#endregion

		#region Help Methods
		private static void FillRating(LogBook logBook, IDataReader reader) {
			logBook.Id = reader.GetInt32(0);
			logBook.UserId = reader.GetInt32(1);
			logBook.CacheId = reader.GetInt32(2);
			logBook.SearchState = (SearchState)reader.GetInt32(3);
			logBook.Comment = reader.GetString(4);
			logBook.CreationDate = reader.GetDateTime(5);
		}

		private void AddParameters(LogBook rate, IDbCommand command) {
			command.Parameters.Add(DALUtils.CreateParameter("@userId", rate.UserId, command));
			command.Parameters.Add(DALUtils.CreateParameter("@cacheId", rate.CacheId, command));
			command.Parameters.Add(DALUtils.CreateParameter("@searchState", rate.SearchState, command));
			command.Parameters.Add(DALUtils.CreateParameter("@comment", rate.Comment, command));
			command.Parameters.Add(DALUtils.CreateParameter("@creationDate", rate.CreationDate, command));
		}
		#endregion
	}
}
