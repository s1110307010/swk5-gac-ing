﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using Microsoft.SqlServer.Types;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.SqlClient;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class UserDao : IUserDao {
		#region Constants
		const string SELECT_USER = "SELECT [Id], [Name], [Password], [Email], [Coordinate] FROM [dbo].[User]";
		const string DELETE_USER = "UPDATE [dbo].[User] SET [Deleted] = 1 WHERE [Id] = @id";
		const string REACTIVATE_USER = "UPDATE [dbo].[User] SET [Deleted] = 0 WHERE [Id] = @id";
		const string CREATE_USER = @"INSERT INTO [dbo].[User] ([Name], [Password], [Email], [Coordinate], [Deleted]) 
									  VALUES (@name, @password, @email, @coordinate, 0)";
		const string UPDATE_USER = @"UPDATE [dbo].[User] SET [Name] = @name, [Password] = @password, [Email] = @email, [Coordinate] = @coordinate WHERE [Id] = @id";
		const string SELECT_COUNT_USER = "SELECT count([Id]) FROM [dbo].[User] WHERE [Deleted] != 1";
		const string SELECT_USER_BY_NAME = "SELECT [Id], [Name], [Password], [Email], [Coordinate] FROM [dbo].[User] WHERE [Name] = @name";
		const string SELECT_USER_BY_EMAIL = "SELECT [Id], [Name], [Password], [Email], [Coordinate] FROM [dbo].[User] WHERE [Email] = @email";
		#endregion

		#region IUserDao Members
		public User GetByUserName(string username) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_USER_BY_NAME + " AND [Deleted] != 1";

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@name", username, command));

				IDataReader reader = command.ExecuteReader();

				User user = null;
				if (reader.Read()) {
					user = new User();
					FillUser(user, reader);
				}

				return user;
			}
		}

		public bool Exists(string username) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_USER_BY_NAME;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@name", username, command));

				IDataReader reader = command.ExecuteReader();

				if (reader.Read())
					return true;
				else
					return false;
			}
		}

		public bool IsEmailAdressInUse(string email, int userId) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_USER_BY_EMAIL;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@email", email, command));

				IDataReader reader = command.ExecuteReader();

				User user = null;
				if (reader.Read()) {
					user = new User();
					FillUser(user, reader);
                    if (user.Id == userId) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
			}
		}
		#endregion

		#region IDao<User> Members
		public User Get(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_USER + " WHERE [Id] = @id AND [Deleted] != 1";

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@id", id, command));

				IDataReader reader = command.ExecuteReader();

				User user = null;
				if (reader.Read()) {
					user = new User();
					FillUser(user, reader);
				}

				return user;
			}
		}

		public IEnumerable<User> GetAll() {
			IList<User> users = null;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_USER + " WHERE [Deleted] != 1";

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (users == null)
						users = new List<User>();

					User user = new User();
					FillUser(user, reader);
					users.Add(user);
				}
			}

			return users;
		}

		public bool Create(User entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = CREATE_USER;
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Update(User entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = UPDATE_USER;
				command.Parameters.Add(DALUtils.CreateParameter("@id", entity.Id, command));
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Delete(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = DELETE_USER;

				IDbDataParameter idParam = command.CreateParameter();
				idParam.ParameterName = "@id";
				idParam.Value = id;
				command.Parameters.Add(idParam);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Reactivate(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = REACTIVATE_USER;

				IDbDataParameter idParam = command.CreateParameter();
				idParam.ParameterName = "@id";
				idParam.Value = id;
				command.Parameters.Add(idParam);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public int CountEntries() {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_COUNT_USER;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;

				IDataReader reader = command.ExecuteReader();

				int result = 0;
				if (reader.Read()) {
					result = reader.GetInt32(0);
				}

				return result;
			}
		}
		#endregion

		#region Help Methods
		private static void FillUser(User user, IDataReader reader) {
			user.Id = reader.GetInt32(0);
			user.Name = reader.GetString(1);
			user.Password = reader.GetString(2);
			user.Email = reader.GetString(3);
			SqlGeography geo = SqlGeography.Deserialize(((SqlDataReader)reader).GetSqlBytes(4));
			user.ResidenceCoordinates = new Coordinate() { Latitude = (double)geo.Lat, Longitude = (double)geo.Long, SpatialReferenceId = (Int32)geo.STSrid };
		}

		private void AddParameters(User user, IDbCommand command) {
			command.Parameters.Add(DALUtils.CreateParameter("@name", user.Name, command));
			command.Parameters.Add(DALUtils.CreateParameter("@password", user.Password, command));
			command.Parameters.Add(DALUtils.CreateParameter("@email", user.Email, command));
			command.Parameters.Add(new SqlParameter("@coordinate", DALUtils.BuildGeographyCoordinates(user.ResidenceCoordinates)) { UdtTypeName = "Geography" });
		}
		#endregion
	}
}
