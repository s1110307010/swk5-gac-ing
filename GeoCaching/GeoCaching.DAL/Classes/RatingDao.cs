﻿namespace GeoCaching.DAL {
	using GeoCaching.Entities;
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class RatingDao : IRatingDao {
		#region Constants
		const string SELECT_RATING = "SELECT [Id], [CacheId], [UserId], [Rate], [CreateDate] FROM [dbo].[Rate]";
		const string DELETE_RATING = "DELETE FROM [dbo].[Rate] WHERE [Id] = @id";
		const string CREATE_RATING = @"INSERT INTO [dbo].[Rate] ([CacheId], [UserId], [Rate], [CreateDate]) 
									  VALUES (@cacheId, @userId, @rate, @creationDate)";
		const string UPDATE_RATING = @"UPDATE [dbo].[Rate] SET [CacheId] = @cacheId, [UserId] = @userId, [Rate] = @rate, [CreateDate] = @creationDate WHERE [Id] = @id";
		const string SELECT_COUNT_RAITING = "SELECT count([Id]) FROM [dbo].[Rate]";
		const string SELECT_RATE_BY_CACHEID = "SELECT [Id], [CacheId], [UserId], [Rate], [CreateDate] FROM [dbo].[Rate] WHERE [CacheId] = @cacheId";
		#endregion

		#region IRatingDao
		public IEnumerable<Rating> GetAllRatingsOfCache(int cacheId) {
			IList<Rating> ratings = null;
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_RATE_BY_CACHEID;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@cacheId", cacheId, command));

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (ratings == null)
						ratings = new List<Rating>();

					Rating r = new Rating();
					r.Id = reader.GetInt32(0);
					r.CacheId = reader.GetInt32(1);
					r.UserId = reader.GetInt32(2);
					r.Rate = reader.GetByte(3);
					r.CreationDate = reader.GetDateTime(4);
					ratings.Add(r);
				}
			}
			return ratings;
		}
		#endregion

		#region IDao<Rating>
		public Rating Get(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_RATING + " WHERE [Id] = @id";

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;
				command.Parameters.Add(DALUtils.CreateParameter("@id", id, command));

				IDataReader reader = command.ExecuteReader();

				Rating rate = null;
				if (reader.Read()) {
					rate = new Rating();
					FillRating(rate, reader);
				}

				return rate;
			}
		}

		public IEnumerable<Rating> GetAll() {
			IList<Rating> rates = null;

			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = SELECT_RATING;

				IDataReader reader = command.ExecuteReader();

				while (reader.Read()) {
					if (rates == null)
						rates = new List<Rating>();

					Rating rate = new Rating();
					FillRating(rate, reader);
					rates.Add(rate);
				}
			}

			return rates;
		}

		public bool Create(Rating entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = CREATE_RATING;
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Update(Rating entity) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = UPDATE_RATING;
				command.Parameters.Add(DALUtils.CreateParameter("@id", entity.Id, command));
				AddParameters(entity, command);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public bool Delete(int id) {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				IDbCommand command = connection.CreateCommand();
				command.CommandText = DELETE_RATING;

				IDbDataParameter idParam = command.CreateParameter();
				idParam.ParameterName = "@id";
				idParam.Value = id;
				command.Parameters.Add(idParam);

				return command.ExecuteNonQuery() == 1;
			}
		}

		public int CountEntries() {
			using (IDbConnection connection = DALFactory.GetConnection()) {
				string select = SELECT_COUNT_RAITING;

				IDbCommand command = connection.CreateCommand();
				command.CommandText = select;

				IDataReader reader = command.ExecuteReader();

				int result = 0;
				if (reader.Read()) {
					result = reader.GetInt32(0);
				}

				return result;
			}
		}
		#endregion

		#region Help Methods
		private static void FillRating(Rating rate, IDataReader reader) {
			rate.Id = reader.GetInt32(0);
			rate.CacheId = reader.GetInt32(1);
			rate.UserId = reader.GetInt32(2);
			rate.Rate = reader.GetByte(3);
			rate.CreationDate = reader.GetDateTime(4);
		}

		private void AddParameters(Rating rate, IDbCommand command) {
			command.Parameters.Add(DALUtils.CreateParameter("@cacheId", rate.CacheId, command));
			command.Parameters.Add(DALUtils.CreateParameter("@userId", rate.UserId, command));
			command.Parameters.Add(DALUtils.CreateParameter("@rate", rate.Rate, command));
			command.Parameters.Add(DALUtils.CreateParameter("@creationDate", rate.CreationDate, command));
		}
		#endregion
	}
}